class Formclear {
    constructor($base) {
        this.$targets = $base.find('input');
        this.$trigger = $base.find('.jsc-formclear-trigger');
        this.bindEvents();
    }
    bindEvents() {
        this.$trigger.on('click', () => {
            this.clearTargets();
        });
    }
    clearTargets() {
        this.$targets.each(function() {
            const $this = $(this);
            switch ($this.attr('type')) {
                case 'radio':
                    $this.attr("checked",false);
            }
        });
    }
}

class Syncform {
    constructor($base) {
        this.$forms = $base.find('input');
        this.bindEvents();
    }
    bindEvents() {
        const self = this;
        this.$forms.each(function() {
            const $this = $(this);
            $this.on('change', function() {
                self.syncParent($this);
            });
        });
    }
    syncParent($target) {
        const thisParentId = $target.data('syncform-parent-id');
        if(! thisParentId) {
            return;
        }

        this.$forms.each(function() {
            const $this = $(this);
            if( thisParentId != $this.val()) {
                return true;
            }

            $this.prop('checked', 'checked')
            .trigger('change');
        });
    }
}

class Fileupload {
    constructor($base) {
        this.$base = $base;
        this.$drag = $base.find('.jsc-fileupload-drag');
        this.maxCount = this.$base.data('fileupload-max');

        this.$thumbnails = $base.find('.jsc-fileupload-thumbnails');
        this.$thumbnailTemplate = $base.find('.jsc-fileupload-thumbnails-template').detach();

        if(this.$thumbnails.children().length > 0) {
            this.$thumbnails.removeClass('hidden');
        }

        this.bindEvents();
    }
    bindEvents() {
        const self = this;

        this.$thumbnails.find('.jsc-fileupload-thumbnail-delete').each(function () {
            const $this = $(this);
            $this.on('click', function () {
                self.deleteThumbnail($this);
            });
        });

        this.$drag.on('drop', e => {
            e.preventDefault();
            const files = e.originalEvent.dataTransfer.files;
            this.uploadFile(files);

            this.$drag.removeClass('filedrag--focus');
        });
        this.$drag.on('dragenter', () => false);
        this.$drag.on('dragover', () => {
            if(this.$drag.hasClass('filedrag--focus')) {
                return false;
            }
            this.$drag.addClass('filedrag--focus');
            return false;
        });
        this.$drag.on('dragleave', () => {
            this.$drag.removeClass('filedrag--focus');
            return false;
        });
    }
    uploadFile(files) {
        const self = this;
        const formData = new FormData();

        for(let i = 0, length = files.length; i < length; i ++){
            if(this.fileUploadMaxCount && i >= this.fileUploadMaxCount) {
                this.showCountOver();
                break;
            }

            const reader = new FileReader();

            reader.onload = (function(theFile) {
                return function(e) {
                    self.setTemplate(e.target.result);
                };
            })(files[i]);
            reader.readAsDataURL(files[i]);
        }
    }
    setTemplate(data) {
        const self = this;

        const $clone = this.$thumbnailTemplate.clone(true);
        $clone.find('.jsc-fileupload-thumbnail-img').attr('src', data);
        $clone.find('.jsc-fileupload-thumbnail-value').val(data);

        this.$thumbnails.append($clone).removeClass('hidden');

        $clone.find('.jsc-fileupload-thumbnail-delete').on('click', function() {
            self.deleteThumbnail($(this));
        });
    }
    deleteThumbnail($target) {
        const $parent = $target.closest('li');
        $parent.remove();
    }
    showCountOver() {
        this.$drag.addClass('dragarea__disabled');
    }
}

class Clone {
    constructor($base) {
        this.$template = $base.find('.jsc-clone-template').children().detach();
        this.$trigger = $base.find('.jsc-clone-trigger');
        this.$cloneTo = $base.find('.jsc-clone-to');

        if(this.$cloneTo.data('clone-init') && this.$cloneTo.children().length < 1) {
            this.addTemplate();
        }
        this.bindEvents();
    }
    bindEvents() {
        this.$trigger.on('click', () => {
            this.addTemplate();
        });
    }
    addTemplate() {
        const childLength = this.$cloneTo.children().length;
        const $clone = this.$template.clone();
        $clone.find('.jsc-clone-change-name').each(function() {
            const $this = $(this);
            $this.attr('name', $this.attr('name').replace('$key', childLength));
        });
        this.$cloneTo.append($clone);
    }
}

$('.jsc-formclear').each(function() {
    new Formclear($(this));
});
$('.jsc-syncform').each(function() {
    new Syncform($(this));
});
$('.jsc-fileupload').each(function() {
    new Fileupload($(this));
});
$('.jsc-clone').each(function() {
    new Clone($(this));
});

$('.fileinput').on('change.bs.fileinput', () => {
    const imgSrc = $('.fileinput').find('img').attr('src');
    $('.jsc-fileupload-value').val(imgSrc);
});

$('.jsc-sortable').sortable({
    update: function(event, ui) {
    }
});


