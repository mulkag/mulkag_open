var MULKAG = MULKAG || {};

MULKAG.CART = {
    init: function () {
        this.setParameters();
        this.setTotalSum();
        this.bindEvents();
    },
    setParameters: function () {
        this.$numbers = $('.jsc-cart-number')
        this.$prices = $('.jsc-cart-price');
        this.$sendPrices = $('.jsc-cart-send-price');
        this.$totalNumber = $('.jsc-cart-total-number');
        this.$totalSend = $('.jsc-cart-total-send-price');
        this.$totalPrice = $('.jsc-cart-total-price');
        this.$deletes = $('.jsc-cart-delete');
    },
    setTotalSum: function () {
        this.numbers = this.getTotalSum(this.$numbers);
        this.prices = this.getTotalSum(this.$prices, true);
        this.sendPrices = this.getTotalSum(this.$sendPrices, true);
        this.$totalNumber.text(this.numbers);
        this.$totalPrice.text((this.prices + this.sendPrices).toLocaleString());
        this.$totalSend.text(this.sendPrices.toLocaleString());
    },
    getTotalSum: function ($obj, isPrice) {
        var self = this;
        var total = 0;

        $obj.each(function (i) {
            var thisVal = parseInt($(this).val());
            if (!thisVal) {
                return true;
            }
            if (isPrice) {
                thisVal = parseInt(self.$numbers.eq(i).val()) * thisVal
            }
            total += thisVal;
        });
        return total;
    },
    bindEvents: function () {
        var self = this;
        this.$numbers.each(function () {
            $(this).on('change', function () {
                self.setTotalSum();
            })
        });

        this.$deletes.each(function () {
            $(this).on('click', function (event) {
                event.preventDefault();
                self.deleteContent($(this));
            });
        });
    },
    deleteContent: function ($trigger) {
        var self = this;
        var id = $trigger.attr('href').replace('#', '');
        $.ajax('/api/cart/', {
            type: 'delete',
            data: {id: id},
        })
            .done(function (result) {
                $trigger.closest('tr').remove();

                setTimeout(function () {
                    self.setTotalSum();
                }, 200);
            });
    }
};

$(function(){
    MULKAG.CART.init();
});
