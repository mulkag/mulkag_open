window.onload = function () {
    Vue.filter('arrayShift', function(array) {
        var firstKey = '';
        for(var key in array) {
            firstKey = key;
        }
        return array[firstKey].photo;
    });
    var search = new Vue({
        el: '.jsc-search',
        data: function() {
            return {
                limit: 50,
                offset: 1,
                items: [],
                number: 0,
                pageNumbers: 0,
                activePage: 1,
                order: 1,
                categories: [],
                blands: [],
                materials: [],
                favorites: [],
                isFavoriteUpdate: false,
            };
        },
        created: function() {
            var that = this;
            $(".jsc-categories").each(function () {
                if($(this).prop("checked")) {
                    that.categories.push($(this).val());
                }
            });
            $(".jsc-blands").each(function () {
                if($(this).prop("checked")) {
                    that.blands.push($(this).val());
                }
            });
            this.buildList();
            this.getFavoriteList();
            this.deferred = new $.Deferred();
        },
        methods: {
            buildList: function(isDeferred){
                var that = this;


                $.ajax({
                    type: 'GET',
                    url: '/api/search.json',
                    data: {
                        'limit': this.limit,
                        'offset': this.offset,
                        'order': this.order,
                        'categories': this.categories,
                        'blands': this.blands,
                        'materials': this.materials,
                    },
                    success: function(result){
                        that.number = result.number;
                        that.items = result.items;
                        that.buildPagination();

                        if(isDeferred) {
                            that.deferred.resolve();
                        }
                    }
                });
            },
            getFavoriteList: function() {
                var that = this;
                $.ajax({
                    type: 'GET',
                    url: '/api/favorite.json',
                    success: function(result){
                        that.favorites = result.favorites;
                    }
                });
            },
            buildPagination: function(){
                this.pageNumbers = Math.ceil(this.number / this.limit);
            },
            showThisPage: function(pageNumber, event) {
                event.preventDefault();
                if(pageNumber === this.activePage) {
                    return;
                }
                this.offset = pageNumber;
                this.activePage = pageNumber;
                this.buildList(true);
                this.deferred.promise().then(function() {
                    $("html,body").animate({scrollTop:0},"300");
                });
            },
            sortList: function() {
                this.buildList();
            },
            narrowList: function() {
                this.buildList();
            },
            changeFavorite: function(itemId) {
                this.isFavoriteUpdate = true;
                var method = '';
                var that = this;

                if(this.favorites.indexOf(itemId) >= 0) {
                    method = 'post';
                }else{
                    method = 'delete';
                }
                $.ajax({
                    type: method,
                    url: '/api/favorite.json',
                    data: {
                        item: itemId
                    },
                    success: function(result){
                        that.isFavoriteUpdate = false;
                    }
                });
        }
     }
    });
};
