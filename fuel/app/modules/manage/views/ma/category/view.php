<h2>Viewing <span class='muted'>#<?php echo $category->id; ?></span></h2>

<p>
	<strong>Name:</strong>
	<?php echo $category->name; ?></p>
<p>
	<strong>Order:</strong>
	<?php echo $category->order; ?></p>
<p>
	<strong>Parent id:</strong>
	<?php echo $category->parent_id; ?></p>
<p>
	<strong>Level:</strong>
	<?php echo $category->level; ?></p>
<p>
	<strong>Is hidden:</strong>
	<?php echo $category->is_hidden; ?></p>

<?php echo \Html::anchor('manage/ma/category/edit/'.$category->id, 'Edit'); ?> |
<?php echo \Html::anchor('manage/ma/category', 'Back'); ?>