<?php echo \Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset class="box-body">
		<div class="form-group">
			<?php echo \Form::label('カテゴリ名', 'name', array('class'=>'control-label')); ?>

				<?php echo \Form::input('name', Input::post('name', isset($category) ? $category->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'カテゴリ名')); ?>

		</div>
		<div class="form-group">
			<?php echo \Form::label('Level', 'level', array('class'=>'control-label')); ?>

				<?php echo \Form::select('level', isset($category) ? $category->level : '', $levels, ['class' => 'form-control']); ?>

		</div>
		<div class="form-group">
			<?php echo \Form::label('表示順', 'order', array('class'=>'control-label')); ?>

				<?php echo \Form::input('order', Input::post('order', isset($category) ? $category->order : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'表示順')); ?>

		</div>

		<?php if($categories): ?>
		<div class="form-group">
			<?php echo \Form::label('親カテゴリ', 'parent_id', array('class'=>'control-label')); ?>
			<div>
				<button type="button" class="btn btn-success " data-toggle="modal" data-target="#modalParentCategory">
				  親カテゴリを選択
				</button>
			</div>
		</div>
		<?php endif; ?>

		<div class="form-group">
			<div class="checkbox">
				<label>
					<?php echo \Form::checkbox('is_hidden', 1, isset($category) ? $category->is_hidden == 1 : false); ?>非表示
				</label>
			</div>
		</div>

		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo \Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>

	<?php if($categories): ?>
	<div class="modal fade jsc-formclear" tabindex="-1" role="dialog" id="modalParentCategory">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">親カテゴリ</h4>
				</div>
				<div class="modal-body">
					<dl>
						<?php foreach ($categories as $level => $category_rows): ?>
							<dt>レベル<?php echo $level; ?></dt>
							<dd>
								<?php foreach($category_rows as $category_row): ?>
									<label class="checkbox-inline">
										<?php echo \Form::radio('parent_id', $category_row['id'], isset($category) ? $category->parent_id == $category_row['id'] : false, ['data-parent-id' => $category_row['parent_id']]); ?>
										<?php echo $category_row['name']; ?>
									</label>
								<?php endforeach; ?>
							</dd>
						<?php endforeach; ?>
					</dl>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger jsc-formclear-trigger">選択を解除</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<?php endif; ?>
<?php echo \Form::close(); ?>