<h2>Editing <span class='muted'>Category</span></h2>
<br>

<?php echo render('ma/category/_form'); ?>
<p>
	<?php echo \Html::anchor('manage/ma/category/view/'.$category->id, 'View'); ?> |
	<?php echo \Html::anchor('manage/ma/category', 'Back'); ?></p>
