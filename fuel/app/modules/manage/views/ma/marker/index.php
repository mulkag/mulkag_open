<h2>Listing <span class='muted'>Ma_markers</span></h2>
<br>
<?php if ($ma_markers): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Zip</th>
			<th>Pref</th>
			<th>Address</th>
			<th>Tel</th>
			<th>Fax</th>
			<th>Mail</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($ma_markers as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->zip; ?></td>
			<td><?php echo $item->pref; ?></td>
			<td><?php echo $item->address; ?></td>
			<td><?php echo $item->tel; ?></td>
			<td><?php echo $item->fax; ?></td>
			<td><?php echo $item->mail; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo \Html::anchor('manage/ma/marker/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/ma/marker/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/ma/marker/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Ma_markers.</p>

<?php endif; ?><p>
	<?php echo \Html::anchor('manage/ma/marker/create', 'Add new Ma marker', array('class' => 'btn btn-success')); ?>

</p>
