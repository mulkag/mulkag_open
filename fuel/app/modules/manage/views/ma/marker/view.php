<h2>Viewing <span class='muted'>#<?php echo $ma_marker->id; ?></span></h2>

<p>
	<strong>Name:</strong>
	<?php echo $ma_marker->name; ?></p>
<p>
	<strong>Zip:</strong>
	<?php echo $ma_marker->zip; ?></p>
<p>
	<strong>Pref:</strong>
	<?php echo $ma_marker->pref; ?></p>
<p>
	<strong>Address:</strong>
	<?php echo $ma_marker->address; ?></p>
<p>
	<strong>Tel:</strong>
	<?php echo $ma_marker->tel; ?></p>
<p>
	<strong>Fax:</strong>
	<?php echo $ma_marker->fax; ?></p>
<p>
	<strong>Mail:</strong>
	<?php echo $ma_marker->mail; ?></p>

<?php echo \Html::anchor('manage/ma/marker/edit/'.$ma_marker->id, 'Edit'); ?> |
<?php echo \Html::anchor('manage/ma/marker', 'Back'); ?>