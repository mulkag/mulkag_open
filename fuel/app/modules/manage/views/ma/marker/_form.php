<div class="jsc-clone">
	<?php echo \Form::open(array("class"=>"form-horizontal")); ?>
	<div class="box">
        <fieldset class="box-body">
			<div class="form-group">
				<?php echo \Form::label('メーカー名', 'name', array('class'=>'control-label')); ?>

					<?php echo \Form::input('name', Input::post('name', isset($ma_marker) ? $ma_marker->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'メーカー名')); ?>

			</div>
			<div class="form-group">
				<?php echo \Form::label('郵便番号', 'zip', array('class'=>'control-label')); ?>

					<?php echo \Form::input('zip', Input::post('zip', isset($ma_marker) ? $ma_marker->zip : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'郵便番号')); ?>

			</div>
			<div class="form-group">
				<?php echo \Form::label('都道府県', 'pref', array('class'=>'control-label')); ?>

					<?php echo \Form::input('pref', Input::post('pref', isset($ma_marker) ? $ma_marker->pref : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'都道府県')); ?>

			</div>
			<div class="form-group">
				<?php echo \Form::label('住所', 'address', array('class'=>'control-label')); ?>

					<?php echo \Form::input('address', Input::post('address', isset($ma_marker) ? $ma_marker->address : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'住所')); ?>

			</div>
			<div class="form-group">
				<?php echo \Form::label('Tel', 'tel', array('class'=>'control-label')); ?>

					<?php echo \Form::input('tel', Input::post('tel', isset($ma_marker) ? $ma_marker->tel : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Tel')); ?>

			</div>
			<div class="form-group">
				<?php echo \Form::label('Fax', 'fax', array('class'=>'control-label')); ?>

					<?php echo \Form::input('fax', Input::post('fax', isset($ma_marker) ? $ma_marker->fax : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Fax')); ?>

			</div>
			<div class="form-group">
				<?php echo \Form::label('Mail', 'mail', array('class'=>'control-label')); ?>

					<?php echo \Form::input('mail', Input::post('mail', isset($ma_marker) ? $ma_marker->mail : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Mail')); ?>

			</div>
		</fieldset>
	</div>

	<!-- 配送方法 -->
	<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">配送方法</h3>
        </div>
        <fieldset class="box-body">
            <div class="jsc-clone-to" data-clone-init="1">
                <?php if(isset($sends)): ?>

                    <?php $i = 0; foreach($sends as $key => $send):?>
                    <div>
                        <div class="form-group">
                            <?php echo \Form::label('配送会社', "sends[{$i}][ma_carrier_id]", array('class'=>'control-label')); ?>
                            <?php echo \Form::select("sends[{$i}][ma_carrier_id]", $send['ma_carrier_id'], $carriers, ['class' => 'form-control jsc-clone-change-name']); ?>
                        </div>

                        <div class="form-group">
                            <?php echo \Form::label('配送方法', "sends[{$i}][ma_carrier_method_id]", array('class'=>'control-label')); ?>
                            <?php echo \Form::select("sends[{$i}][ma_carrier_method_id]", $send['ma_carrier_method_id'], $carrier_methods, ['class' => 'form-control jsc-clone-change-name']); ?>
                        </div>
                    </div>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label class='control-label'>&nbsp;</label>
                <button type="button" class="btn btn-success jsc-clone-trigger">配送方法を追加</button>
            </div>

            <div class="form-group">
                <label class='control-label'>&nbsp;</label>
                <?php echo \Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>
            </div>
        </fieldset>
    </div>
	<?php echo \Form::close(); ?>

<!-- 配送方法テンプレート -->
    <div class="hidden jsc-clone-template">
        <div>
            <div class="form-group">
                <?php echo \Form::label('配送会社', 'sends[$key][ma_carrier_id]', array('class'=>'control-label')); ?>
                <?php echo \Form::select('sends[$key][ma_carrier_id]', '', $carriers, ['class' => 'form-control jsc-clone-change-name']); ?>
            </div>

            <div class="form-group">
                <?php echo \Form::label('配送方法', 'sends[$key][ma_carrier_method_id]', array('class'=>'control-label')); ?>
                <?php echo \Form::select('sends[$key][ma_carrier_method_id]', '', $carrier_methods, ['class' => 'form-control jsc-clone-change-name']); ?>
            </div>
        </div>
    </div>
</div>