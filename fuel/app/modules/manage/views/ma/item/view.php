<h2>Viewing <span class='muted'>#<?php echo $item->id; ?></span></h2>

<p>
	<strong>Name:</strong>
	<?php echo $item->name; ?></p>
<p>
	<strong>Ma bland id:</strong>
	<?php echo $item->ma_bland_id; ?></p>
<p>
	<strong>Explain:</strong>
	<?php echo $item->explain; ?></p>
<p>
	<strong>Cost price:</strong>
	<?php echo $item->cost_price; ?></p>
<p>
	<strong>Jan code:</strong>
	<?php echo $item->jan_code; ?></p>
<p>
	<strong>Is hidden:</strong>
	<?php echo $item->is_hidden; ?></p>
<p>
	<strong>Is members:</strong>
	<?php echo $item->is_members; ?></p>
<p>
	<strong>Color code:</strong>
	<?php echo $item->ma_color_id; ?></p>
<p>
	<strong>Multina price:</strong>
	<?php echo $item->multina_price; ?></p>
<p>
	<strong>Packing size:</strong>
	<?php echo $item->packing_size; ?></p>

<?php echo \Html::anchor('manage/ma/item/edit/'.$item->id, 'Edit'); ?> |
<?php echo \Html::anchor('manage/ma/item', 'Back'); ?>