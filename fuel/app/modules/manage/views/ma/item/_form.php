<div class="jsc-clone">
    <?php echo \Form::open(array("class"=>"form-horizontal")); ?>
    <div class="box">
        <fieldset class="box-body">
            <div class="form-group">
                <?php echo \Form::label('商品名', 'name', array('class'=>'control-label')); ?>
                <?php echo \Form::input('name', Input::post('name', isset($item) ? $item->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'商品名')); ?>
            </div>

            <?php if($categories): ?>
                <div class="form-group">
                    <?php echo \Form::label('カテゴリ', 'category_id', array('class'=>'control-label')); ?>
                    <div>
                        <button type="button" class="btn btn-success " data-toggle="modal" data-target="#modalCategory">カテゴリを選択</button>
                    </div>
              </div>
            <?php endif; ?>

            <div class="form-group">
                <?php echo \Form::label('ブランド', 'ma_bland_id', array('class'=>'control-label')); ?>
                <?php echo \Form::select('ma_bland_id', Input::post('ma_bland_id', isset($item) ? $item->ma_bland_id : ''), $blands, ['class' => 'form-control']); ?>
            </div>

            <div class="form-group">
                <?php echo \Form::label('説明', 'explain', array('class'=>'control-label')); ?>
                <?php echo \Form::textarea('explain', Input::post('explain', isset($item) ? $item->explain : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'説明')); ?>
            </div>

            <div class="form-group jsc-fileupload" data-fileupload-max="5">
                <?php echo \Form::label('商品画像', 'photos', array('class'=>'control-label')); ?>
                <div class="filedrag jsc-fileupload-drag">
                    <p>画像をドラッグしてください</p>
                    <button type="button" class="btn">画像を選択</button>
                </div>
                <ul class="thumbnails hidden jsc-fileupload-thumbnails jsc-sortable">
                    <?php if($items_photos): ?>
                        <?php foreach ($items_photos as $key => $item_photo): ?>
                            <li>
                                <img class="thumbnails__img jsc-fileupload-thumbnail-img" src="<?php echo $item_photo; ?>">
                                <button type="button" class="thumbnails__delete btn btn-block btn-danger btn-xs jsc-fileupload-thumbnail-delete">削除</button>
                                <input type="hidden" name="photo[<?php echo $key; ?>]" value="<?php echo $item_photo; ?>" class="jsc-fileupload-thumbnail-value">
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
                <ul class="hidden">
                    <li class="jsc-fileupload-thumbnails-template">
                        <img class="thumbnails__img jsc-fileupload-thumbnail-img">
                        <button type="button" class="thumbnails__delete btn btn-block btn-danger btn-xs jsc-fileupload-thumbnail-delete">削除</button>
                        <input type="hidden" name="photo[]" value="" class="jsc-fileupload-thumbnail-value">
                    </li>
                </ul>
            </div>

            <div class="form-group">
                <?php echo \Form::label('Jan code', 'jan_code', array('class'=>'control-label')); ?>
                <?php echo \Form::input('jan_code', Input::post('jan_code', isset($item) ? $item->jan_code : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Jan code')); ?>
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <?php echo \Form::checkbox('is_hidden', 1, isset($category) ? $category->is_hidden == 1 : false); ?>非表示
                    </label>
                </div>
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <?php echo \Form::checkbox('is_members', 1, isset($category) ? $category->is_members == 1 : false); ?>会員限定
                    </label>
                </div>
            </div>

            <div class="form-group">
                <?php echo \Form::label('仕入れ値', 'cost_price', array('class'=>'control-label')); ?>
                <?php echo \Form::input('cost_price', Input::post('cost_price', isset($item) ? $item->cost_price : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'仕入れ値')); ?>
            </div>

            <div class="form-group">
                <?php echo \Form::label('販売価格', 'multina_price', array('class'=>'control-label')); ?>
                <?php echo \Form::input('multina_price', Input::post('multina_price', isset($item) ? $item->multina_price : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'販売価格')); ?>
            </div>

            <div class="form-group">
                <?php echo \Form::label('梱包サイズ', 'packing_size', array('class'=>'control-label')); ?>
                <?php echo \Form::input('packing_size', Input::post('packing_size', isset($item) ? $item->packing_size : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'梱包サイズ')); ?>
            </div>

            <div class="form-group">
                <?php echo \Form::label('色コード', 'ma_color_id', array('class'=>'control-label')); ?>
                <?php echo \Form::select('ma_color_id', Input::post('ma_color_id', isset($item) ? $item->ma_color_id : ''), $colors, ['class' => 'form-control']); ?>
            </div>
        </fieldset>
    </div>

    <!-- 商品詳細 -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">商品詳細</h3>
        </div>
        <fieldset class="box-body">
            <div class="jsc-clone-to" data-clone-init="1">
                <?php if(isset($item_details)): ?>
                    <?php foreach($item_details as $key => $detail): ?>
                    <div>
                        <div class="form-group">
                            <?php echo \Form::label('素材', "item_details[{$key}][ma_material_id]", array('class'=>'control-label')); ?>
                            <?php echo \Form::select("item_details[{$key}][ma_material_id]", $detail['ma_material_id'], $materials, ['class' => 'form-control', 'required' => 'required']); ?>
                        </div>

                        <div class="form-group">
                            <?php echo \Form::label('仕入れ値', "item_details[{$key}][cost_price]", array('class'=>'control-label')); ?>
                            <?php echo \Form::input("item_details[{$key}][cost_price]", $detail['cost_price'], array('class' => 'col-md-4 form-control', 'placeholder'=>'仕入れ値')); ?>
                        </div>

                        <div class="form-group">
                            <?php echo \Form::label('販売価格', "item_details[{$key}][multina_price]", array('class'=>'control-label')); ?>
                            <?php echo \Form::input("item_details[{$key}][multina_price]", $detail['multina_price'], array('class' => 'col-md-4 form-control', 'placeholder'=>'販売価格')); ?>
                        </div>

                        <div class="form-group">
                            <?php echo \Form::label('梱包サイズ', "item_details[{$key}][packing_size]", array('class'=>'control-label')); ?>
                            <?php echo \Form::input("item_details[{$key}][packing_size]", $detail['packing_size'], array('class' => 'col-md-4 form-control', 'placeholder'=>'梱包サイズ')); ?>
                        </div>

                        <div class="form-group">
                            <?php echo \Form::label('色コード', "item_details[{$key}][ma_color_id]", array('class'=>'control-label')); ?>
                            <?php echo \Form::select('item_details[{$key}][ma_color_id]', $detail['ma_color_id'], $colors, ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <div class="form-group">
                <label class='control-label'>&nbsp;</label>
                <button type="button" class="btn btn-success jsc-clone-trigger">素材パターンを追加</button>
            </div>

            <div class="form-group">
                <label class='control-label'>&nbsp;</label>
                <?php echo \Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>
            </div>

            <?php if($categories): ?>
                <div class="modal fade jsc-formclear" tabindex="-1" role="dialog" id="modalCategory">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">カテゴリ</h4>
                            </div>
                            <div class="modal-body">
                                <dl>
                                    <?php foreach ($categories as $level => $category_rows): ?>
                                        <dt>レベル<?php echo $level; ?></dt>
                                        <dd>
                                            <?php foreach($category_rows as $key => $category_row): ?>
                                                <label class="checkbox-inline">
                                                    <?php echo \Form::checkbox('ma_category_id[]', $category_row['id'], isset($item) ? $item->categories == $category_row['id'] : false); ?>
                                                    <?php echo $category_row['name']; ?>
                                                </label>
                                            <?php endforeach; ?>
                                        </dd>
                                    <?php endforeach; ?>
                                </dl>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger jsc-formclear-trigger">選択を解除</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </fieldset>
    </div>
    <?php echo \Form::close(); ?>

<!-- 商品詳細テンプレート -->
    <div class="hidden jsc-clone-template">
        <div>
            <div class="form-group">
                <?php echo \Form::label('素材', 'item_details[$key][ma_material_id]', array('class'=>'control-label')); ?>
                <?php echo \Form::select('item_details[$key][ma_material_id]', '', $materials, ['class' => 'form-control jsc-clone-change-name']); ?>
            </div>

            <div class="form-group">
                <?php echo \Form::label('販売価格', 'item_details[$key][multina_price]', array('class'=>'control-label')); ?>
                <?php echo \Form::input('item_details[$key][multina_price]', '', array('class' => 'col-md-4 form-control jsc-clone-change-name', 'placeholder'=>'未入力の場合は親情報を引継ぐ')); ?>
            </div>

            <div class="form-group">
                <?php echo \Form::label('梱包サイズ', 'item_details[$key][packing_size]', array('class'=>'control-label')); ?>
                <?php echo \Form::input('item_details[$key][packing_size]', '', array('class' => 'col-md-4 form-control jsc-clone-change-name', 'placeholder'=>'未入力の場合は親情報を引継ぐ')); ?>
            </div>

            <div class="form-group">
                <?php echo \Form::label('色コード', 'item_details[$key][ma_color_id]', array('class'=>'control-label')); ?>
                <?php echo \Form::select('item_details[$key][ma_color_id]', '', $colors, ['class' => 'form-control']); ?>
            </div>
        </div>
    </div>
</div>
