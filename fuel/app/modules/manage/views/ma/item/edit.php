<h2>Editing <span class='muted'>Item</span></h2>
<br>

<?php echo render('ma/item/_form'); ?>
<p>
	<?php echo \Html::anchor('manage/ma/item/view/'.$item->id, 'View'); ?> |
	<?php echo \Html::anchor('manage/ma/item', 'Back'); ?></p>
