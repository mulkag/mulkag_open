<h2>Listing <span class='muted'>Items</span></h2>
<br>

<?php echo \Form::open(array('method' => 'get')); ?>
    <div class="form-group">
    <?php foreach($categories as $key => $category): ?>
        <div class="col-sm-3">
            <?php echo \Form::select('category' . $key, '', $category, ['class' => 'form-control']); ?>
        </div>
    <?php endforeach; ?>
        <div class="col-sm-3">
            <?php echo \Form::submit('submit', '絞込む', array('class' => 'btn btn-primary')); ?>
        </div>
    </div>
</form>

<?php if ($items): ?>
<?php echo $pagination; ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>商品名</th>
			<th>カテゴリ</th>
			<th>ブランド</th>
			<th>仕入れ値</th>
			<th>売値</th>
			<th>会員限定</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($items as $item): ?>		<tr>
			<td><?php echo $item->name; ?></td>
			<td>
				<ul class="list-horizontal list-unstyled">
				<?php foreach($item->categories as $category): ?>
					<?php if($category->level > 1) continue; ?>
					<li><?php echo $category->name; ?></li>
				<?php endforeach; ?>
				</ul>
			</td>
			<td><?php echo isset($blands[$item->ma_bland_id]) ? $blands[$item->ma_bland_id] : '-'; ?></td>
			<td><?php echo $item->cost_price; ?></td>
			<td><?php echo $item->multina_price; ?></td>
			<td><?php echo $item->is_members; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo \Html::anchor('manage/ma/item/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/ma/item/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/ma/item/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Items.</p>

<?php endif; ?><p>
	<?php echo \Html::anchor('manage/ma/item/create', 'Add new Item', array('class' => 'btn btn-success')); ?>

</p>
