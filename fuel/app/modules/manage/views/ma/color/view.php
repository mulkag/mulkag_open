<h2>Viewing <span class='muted'>#<?php echo $ma_color->id; ?></span></h2>

<p>
	<strong>Name:</strong>
	<?php echo $ma_color->name; ?></p>
<p>
	<strong>Color code:</strong>
	<?php echo $ma_color->color_code; ?></p>

<?php echo \Html::anchor('manage/ma/color/edit/'.$ma_color->id, 'Edit'); ?> |
<?php echo \Html::anchor('manage/ma/color', 'Back'); ?>