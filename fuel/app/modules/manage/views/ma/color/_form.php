<?php echo \Form::open(array("class"=>"form-horizontal")); ?>
	<fieldset class="box-body">
		<div class="form-group">
			<?php echo \Form::label('Name', 'name', array('class'=>'control-label')); ?>

				<?php echo \Form::input('name', Input::post('name', isset($ma_color) ? $ma_color->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Name')); ?>

		</div>
		<div class="form-group">
			<?php echo \Form::label('Color code', 'color_code', array('class'=>'control-label')); ?>

				<?php echo \Form::input('color_code', Input::post('color_code', isset($ma_color) ? $ma_color->color_code : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Color code')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo \Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo \Form::close(); ?>