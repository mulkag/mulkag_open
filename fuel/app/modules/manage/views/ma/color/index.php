<h2>Listing <span class='muted'>Ma_colors</span></h2>
<br>
<?php if ($ma_colors): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Color code</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($ma_colors as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->color_code; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo \Html::anchor('manage/ma/color/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/ma/color/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/ma/color/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Ma_colors.</p>

<?php endif; ?><p>
	<?php echo \Html::anchor('manage/ma/color/create', 'Add new Ma color', array('class' => 'btn btn-success')); ?>

</p>
