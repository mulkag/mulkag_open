<?php if ($ma_blands): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>ブランド名</th>
			<th>メーカー</th>
			<th>説明</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($ma_blands as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $markers[$item->ma_marker_id]; ?></td>
			<td><?php echo nl2br($item->explain); ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo \Html::anchor('manage/ma/bland/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/ma/bland/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/ma/bland/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>ブランドはありません</p>

<?php endif; ?><p>
	<?php echo \Html::anchor('manage/ma/bland/create', 'ブランド登録', array('class' => 'btn btn-success')); ?>

</p>
