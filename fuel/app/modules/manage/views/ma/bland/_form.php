<?php echo \Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset class="box-body">
		<div class="form-group">
			<?php echo \Form::label('ブランド名', 'name', array('class'=>'control-label')); ?>

				<?php echo \Form::input('name', Input::post('name', isset($ma_bland) ? $ma_bland->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'ブランド名')); ?>

		</div>
		<div class="form-group">
			<?php echo \Form::label('メーカー', 'ma_marker_id', array('class'=>'control-label')); ?>
				<?php echo \Form::select('ma_marker_id', isset($ma_bland) ? $ma_bland->ma_marker_id : '', $markers, ['class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<?php echo \Form::label('説明', 'explain', array('class'=>'control-label')); ?>
				<?php echo \Form::textarea('explain', Input::post('explain', isset($ma_bland) ? $ma_bland->explain : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'説明')); ?>

		</div>
        <div class="form-group">
            <?php echo \Form::label('ロゴ', 'logo', array('class'=>'control-label', 'style' => 'display:block; text-align: left;')); ?>

    		<div class="fileinput fileinput-new" data-provides="fileinput">
    			<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                    <?php if(isset($logo) && $logo): ?>
                        <img src="<?php echo $logo; ?>">
                    <?php endif; ?>
                </div>
    			<div>
                    <span class="btn btn-default btn-file">
                        <span class="fileinput-new">Select image</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="logo">
                        <input type="hidden" value="<?php if(isset($logo) && $logo) echo $logo; ?>" name="logo_data" class="jsc-fileupload-value">
                    </span>
    				<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
    			</div>
    		</div>
        </div>
		<div>
			<label class='control-label'>&nbsp;</label>
			<?php echo \Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo \Form::close(); ?>
