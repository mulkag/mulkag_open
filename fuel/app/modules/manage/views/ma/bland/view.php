<h2>Viewing <span class='muted'>#<?php echo $ma_bland->id; ?></span></h2>

<p>
	<strong>ブランド名:</strong>
	<?php echo $ma_bland->name; ?></p>
<p>
	<strong>メーカー:</strong>
	<?php echo $markers[$ma_bland->ma_marker_id]; ?></p>
<p>
	<strong>説明:</strong>
	<?php echo nl2br($ma_bland->explain); ?></p>

<?php if($ma_bland->logo): ?>
<p>
    <strong>ロゴ:</strong>
    <p><?php echo Asset::img('blands/' . $ma_bland->logo, ['class' => 'view__img']); ?></p>
<?php endif; ?>

<?php echo \Html::anchor('manage/ma/bland/edit/'.$ma_bland->id, 'Edit'); ?> |
<?php echo \Html::anchor('manage/ma/bland', 'Back'); ?>