<h2>Editing <span class='muted'>ManageUser</span></h2>
<br>

<?php echo render('manageuser/_form'); ?>
<p>
	<?php echo \Html::anchor('manage/manageuser/view/'.$manageUser->id, 'View'); ?> |
	<?php echo \Html::anchor('manage/manageuser', 'Back'); ?></p>
