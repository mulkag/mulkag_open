<?php echo \Form::open(array("class"=>"form-horizontal")); ?>
	<fieldset class="box-body">
		<div class="form-group">
			<?php echo \Form::label('Name', 'name', array('class'=>'control-label')); ?>

				<?php echo \Form::input('name', Input::post('name', isset($manageUser) ? $manageUser->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Name')); ?>

		</div>
		<div class="form-group">
			<?php echo \Form::label('Mail', 'mail', array('class'=>'control-label')); ?>

				<?php echo \Form::input('mail', Input::post('mail', isset($manageUser) ? $manageUser->mail : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Mail')); ?>

		</div>
		<div class="form-group">
			<?php echo \Form::label('パスワード', 'password', array('class'=>'control-label')); ?>

				<?php echo \Form::input('password', '', array('type' => 'password', 'class' => 'col-md-4 form-control', 'placeholder'=>'パスワード')); ?>

		</div>
		<div class="form-group">
			<div class="checkbox">
				<label>
					<?php echo \Form::checkbox('is_admin', 1, isset($manageUser) ? $manageUser->is_admin == 1 : false); ?>管理者
				</label>
		</div>

		<div class="form-group">
            <?php echo \Form::label('アイコン', 'photo', array('class'=>'control-label', 'style' => 'display:block; text-align: left;')); ?>

    		<div class="fileinput fileinput-new" data-provides="fileinput">
    			<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                    <?php if(isset($photo) && $photo): ?>
                        <img src="<?php echo $photo; ?>">
                    <?php endif; ?>
                </div>
    			<div>
                    <span class="btn btn-default btn-file">
                        <span class="fileinput-new">Select image</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="photo">
                        <input type="hidden" value="<?php if(isset($photo) && $photo) echo $photo; ?>" name="photo_data" class="jsc-fileupload-value">
                    </span>
    				<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
    			</div>
    		</div>
        </div>

		<div class="form-group">
			<?php echo \Form::label('メーカー', 'ma_marker_id', array('class'=>'control-label')); ?>
				<?php echo \Form::select('ma_marker_id', isset($manageUser) ? $manageUser->ma_marker_id : '', $markers, ['class' => 'form-control']); ?>
		</div>

		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo \Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo \Form::close(); ?>