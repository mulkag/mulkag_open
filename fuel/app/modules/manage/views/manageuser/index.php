<h2>Listing <span class='muted'>ManageUsers</span></h2>
<br>
<?php if ($manageUsers): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>名前</th>
			<th>Mail</th>
			<th>管理者</th>
			<th>メーカー</th>
			<th>最終ログイン日時</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($manageUsers as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->mail; ?></td>
			<td><?php echo $item->is_admin; ?></td>
			<td><?php echo $item->ma_marker_id; ?></td>
			<td><?php echo $item->last_login ? date('Y-m-d H:i:s', $item->last_login) : '-'; ?></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo \Html::anchor('manage/manageuser/view/'.$item->id, '<i class="icon-eye-open"></i> View', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/manageuser/edit/'.$item->id, '<i class="icon-wrench"></i> Edit', array('class' => 'btn btn-default btn-sm')); ?>						<?php echo \Html::anchor('manage/manageuser/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No ManageUsers.</p>

<?php endif; ?><p>
	<?php echo \Html::anchor('manage/manageuser/create', 'Add new ManageUser', array('class' => 'btn btn-success')); ?>

</p>
