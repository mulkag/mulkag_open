<?php
namespace manage;

class Controller_Login extends Controller
{

	public function action_index()
	{
        $data = [];

        if (\Input::method() == 'POST')
        {
            $data['result'] = $this->login();
        }

        return \Response::forge(\View::forge('login/index', $data));
	}


	public function action_logout()
	{
        $manage_user = \Model_ManageUser::query()->where('id', \Session::get('manageuser.id'))->get_one();
        if(! $manage_user) {
            exit;
        }
        $manage_user->login_hush = null;
        $manage_user->save();

        \Model_Manageuser_History::add_data($manage_user->id, 'logout');

        \Session::delete('manageuser');
        \Cookie::delete('manageuser');

        $data['logout'] = true;

        return \Response::forge(\View::forge('login/index', $data));
	}


    protected function login()
    {
        if(! \Input::post('mail') || ! \Input::post('password') ) {
            return false;
        }

        $user = \Model_ManageUser::query()->where('mail', \Input::post('mail'))->where('password', \Crypt::encode(\Input::post('password')))->get_one();
        if(! $user) {
            return false;
        }

        \DB::start_transaction();

        $user->last_login = time();
        $user->login_hush = \Crypt::encode(\Session::key('session_id') . time());

        \Model_Manageuser_History::add_data($user->id, 'login');

        $user->save();
        \DB::commit_transaction();

        \Session::set('manageuser', $user);

        if(\Input::post('keep')) {
            \Cookie::set('manageuser', $user, 60 * 60 * 24);
        }
        return true;
    }

}
