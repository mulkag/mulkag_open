<?php
namespace manage;

class Controller_Ma_Material extends Controller_Manage
{

	public function action_index()
	{
		$data['ma_materials'] = \Model_Ma_Material::find('all');
		$this->template->title = "Ma_materials";
		$this->template->content = \View::forge('ma/material/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and \Response::redirect('ma/material');

		if ( ! $data['ma_material'] = \Model_Ma_Material::find($id))
		{
			\Session::set_flash('error', 'Could not find ma_material #'.$id);
			\Response::redirect('ma/material');
		}

		$this->template->title = "Ma_material";
		$this->template->content = \View::forge('ma/material/view', $data);

	}

	public function action_create()
	{
		if (\Input::method() == 'POST')
		{
			$val = \Model_Ma_Material::validate('create');

			if ($val->run())
			{
				$ma_material = \Model_Ma_Material::forge(array(
					'name' => \Input::post('name'),
				));

				if ($ma_material and $ma_material->save())
				{
					\Session::set_flash('success', 'Added ma_material #'.$ma_material->id.'.');

					\Response::redirect('ma/material');
				}

				else
				{
					\Session::set_flash('error', 'Could not save ma_material.');
				}
			}
			else
			{
				\Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Ma_Materials";
		$this->template->content = \View::forge('ma/material/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and \Response::redirect('ma/material');

		if ( ! $ma_material = \Model_Ma_Material::find($id))
		{
			\Session::set_flash('error', 'Could not find ma_material #'.$id);
			\Response::redirect('ma/material');
		}

		$val = \Model_Ma_Material::validate('edit');

		if ($val->run())
		{
			$ma_material->name = \Input::post('name');

			if ($ma_material->save())
			{
				\Session::set_flash('success', 'Updated ma_material #' . $id);

				\Response::redirect('ma/material');
			}

			else
			{
				\Session::set_flash('error', 'Could not update ma_material #' . $id);
			}
		}

		else
		{
			if (\Input::method() == 'POST')
			{
				$ma_material->name = $val->validated('name');

				\Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_material', $ma_material, false);
		}

		$this->template->title = "Ma_materials";
		$this->template->content = \View::forge('ma/material/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and \Response::redirect('ma/material');

		if ($ma_material = \Model_Ma_Material::find($id))
		{
			$ma_material->delete();

			\Session::set_flash('success', 'Deleted ma_material #'.$id);
		}

		else
		{
			\Session::set_flash('error', 'Could not delete ma_material #'.$id);
		}

		\Response::redirect('ma/material');

	}

}
