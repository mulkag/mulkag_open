<?php
namespace manage;

class Controller_Ma_Color extends Controller_Manage
{

	public function action_index()
	{
		$data['ma_colors'] = \Model_Ma_Color::find('all');
		$this->template->title = "Ma_colors";
		$this->template->content = \View::forge('ma/color/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and \Response::redirect('ma/color');

		if ( ! $data['ma_color'] = \Model_Ma_Color::find($id))
		{
			\Session::set_flash('error', 'Could not find ma_color #'.$id);
			\Response::redirect('ma/color');
		}

		$this->template->title = "Ma_color";
		$this->template->content = \View::forge('ma/color/view', $data);

	}

	public function action_create()
	{
		if (\Input::method() == 'POST')
		{
			$val = \Model_Ma_Color::validate('create');

			if ($val->run())
			{
				$ma_color = \Model_Ma_Color::forge(array(
					'name' => \Input::post('name'),
					'color_code' => \Input::post('color_code'),
				));

				if ($ma_color and $ma_color->save())
				{
					\Session::set_flash('success', 'Added ma_color #'.$ma_color->id.'.');

					\Response::redirect('ma/color');
				}

				else
				{
					\Session::set_flash('error', 'Could not save ma_color.');
				}
			}
			else
			{
				\Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Ma_Colors";
		$this->template->content = \View::forge('ma/color/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and \Response::redirect('ma/color');

		if ( ! $ma_color = \Model_Ma_Color::find($id))
		{
			\Session::set_flash('error', 'Could not find ma_color #'.$id);
			\Response::redirect('ma/color');
		}

		$val = \Model_Ma_Color::validate('edit');

		if ($val->run())
		{
			$ma_color->name = \Input::post('name');
			$ma_color->color_code = \Input::post('color_code');

			if ($ma_color->save())
			{
				\Session::set_flash('success', 'Updated ma_color #' . $id);

				\Response::redirect('ma/color');
			}

			else
			{
				\Session::set_flash('error', 'Could not update ma_color #' . $id);
			}
		}

		else
		{
			if (\Input::method() == 'POST')
			{
				$ma_color->name = $val->validated('name');
				$ma_color->color_code = $val->validated('color_code');

				\Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_color', $ma_color, false);
		}

		$this->template->title = "Ma_colors";
		$this->template->content = \View::forge('ma/color/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and \Response::redirect('ma/color');

		if ($ma_color = \Model_Ma_Color::find($id))
		{
			$ma_color->delete();

			\Session::set_flash('success', 'Deleted ma_color #'.$id);
		}

		else
		{
			\Session::set_flash('error', 'Could not delete ma_color #'.$id);
		}

		\Response::redirect('ma/color');

	}

}
