<?php
namespace manage;

class Controller_Ma_User_History extends Controller_Manage
{

	public function action_index()
	{
		$data['ma_user_histories'] = \Model_Ma_User_History::find('all');
		$this->template->title = "Ma_user_histories";
		$this->template->content = \View::forge('ma/user/history/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and \Response::redirect('ma/user/history');

		if ( ! $data['ma_user_history'] = \Model_Ma_User_History::find($id))
		{
			\Session::set_flash('error', 'Could not find ma_user_history #'.$id);
			\Response::redirect('ma/user/history');
		}

		$this->template->title = "Ma_user_history";
		$this->template->content = \View::forge('ma/user/history/view', $data);

	}

	public function action_create()
	{
		if (\Input::method() == 'POST')
		{
			$val = \Model_Ma_User_History::validate('create');

			if ($val->run())
			{
				$ma_user_history = \Model_Ma_User_History::forge(array(
					'name' => \Input::post('name'),
				));

				if ($ma_user_history and $ma_user_history->save())
				{
					\Session::set_flash('success', 'Added ma_user_history #'.$ma_user_history->id.'.');

					\Response::redirect('ma/user/history');
				}

				else
				{
					\Session::set_flash('error', 'Could not save ma_user_history.');
				}
			}
			else
			{
				\Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Ma_User_Histories";
		$this->template->content = \View::forge('ma/user/history/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and \Response::redirect('ma/user/history');

		if ( ! $ma_user_history = \Model_Ma_User_History::find($id))
		{
			\Session::set_flash('error', 'Could not find ma_user_history #'.$id);
			\Response::redirect('ma/user/history');
		}

		$val = \Model_Ma_User_History::validate('edit');

		if ($val->run())
		{
			$ma_user_history->name = \Input::post('name');

			if ($ma_user_history->save())
			{
				\Session::set_flash('success', 'Updated ma_user_history #' . $id);

				\Response::redirect('ma/user/history');
			}

			else
			{
				\Session::set_flash('error', 'Could not update ma_user_history #' . $id);
			}
		}

		else
		{
			if (\Input::method() == 'POST')
			{
				$ma_user_history->name = $val->validated('name');

				\Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_user_history', $ma_user_history, false);
		}

		$this->template->title = "Ma_user_histories";
		$this->template->content = \View::forge('ma/user/history/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and \Response::redirect('ma/user/history');

		if ($ma_user_history = \Model_Ma_User_History::find($id))
		{
			$ma_user_history->delete();

			\Session::set_flash('success', 'Deleted ma_user_history #'.$id);
		}

		else
		{
			\Session::set_flash('error', 'Could not delete ma_user_history #'.$id);
		}

		\Response::redirect('ma/user/history');

	}

}
