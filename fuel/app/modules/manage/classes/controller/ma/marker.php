<?php
namespace manage;

class Controller_Ma_Marker extends Controller_Manage
{

	public function action_index()
	{
		$data['ma_markers'] = \Model_Ma_Marker::find('all');
		$this->template->title = "メーカー";
		$this->template->content = \View::forge('ma/marker/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and \Response::redirect('ma/marker');

		if ( ! $data['ma_marker'] = \Model_Ma_Marker::find($id))
		{
			\Session::set_flash('error', 'Could not find ma_marker #'.$id);
			\Response::redirect('ma/marker');
		}

		$this->template->title = "メーカー";
		$this->template->content = \View::forge('ma/marker/view', $data);

	}

	public function action_create()
	{
		if (\Input::method() == 'POST')
		{
			$val = \Model_Ma_Marker::validate('create');

			if ($val->run())
			{
				$ma_marker = \Model_Ma_Marker::forge(array(
					'name' => \Input::post('name'),
					'zip' => \Input::post('zip'),
					'pref' => \Input::post('pref'),
					'address' => \Input::post('address'),
					'tel' => \Input::post('tel'),
					'fax' => \Input::post('fax'),
					'mail' => \Input::post('mail'),
				));
				foreach((array) \Input::post('sends') as $key => $send) {
					$ma_marker->sends[$key] = new \Model_Ma_Marker_Send();
					$ma_marker->sends[$key]['ma_carrier_id'] = $send['ma_carrier_id'];
					$ma_marker->sends[$key]['ma_carrier_method_id'] = $send['ma_carrier_method_id'];
				}

				if ($ma_marker and $ma_marker->save())
				{
					\Session::set_flash('success', 'Added ma_marker #'.$ma_marker->id.'.');

					\Response::redirect('ma/marker');
				}
				else
				{
					\Session::set_flash('error', 'Could not save ma_marker.');
				}
			}
			else
			{
				\Session::set_flash('error', $val->error());
			}
		}

		$this->build_form_parts();

        if(\Input::post('sends')) {
            \View::set_global('sends', \Input::post('sends'), false);
        }

		$this->template->title = "メーカー";
		$this->template->content = \View::forge('ma/marker/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and \Response::redirect('ma/marker');

		if ( ! $ma_marker = \Model_Ma_Marker::find($id, ['related' => ['sends']]))
		{
			\Session::set_flash('error', 'Could not find ma_marker #'.$id);
			\Response::redirect('ma/marker');
		}

		$val = \Model_Ma_Marker::validate('edit');

		foreach((array) \Input::post('sends') as $key => $send) {
			$val->add("sends." . $key . ".ma_carrier_id", '運送会社')
				->add_rule('valid_string', ['numeric']);
			$val->add("sends." . $key . ".ma_carrier_method_id", '配送方法')
				->add_rule('valid_string', ['numeric']);
		}

		if ($val->run())
		{
			$ma_marker->name = \Input::post('name');
			$ma_marker->zip = \Input::post('zip');
			$ma_marker->pref = \Input::post('pref');
			$ma_marker->address = \Input::post('address');
			$ma_marker->tel = \Input::post('tel');
			$ma_marker->fax = \Input::post('fax');
			$ma_marker->mail = \Input::post('mail');

			foreach((array) \Input::post('sends') as $key => $send) {
				$ma_marker->sends[$key] = new \Model_Ma_Marker_Send();
				$ma_marker->sends[$key]['ma_carrier_id'] = $send['ma_carrier_id'];
				$ma_marker->sends[$key]['ma_carrier_method_id'] = $send['ma_carrier_method_id'];
			}

            \Model_Ma_Marker_Send::query()->where('ma_marker_id', $ma_marker->id)->delete();

			if ($ma_marker->save())
			{
				\Session::set_flash('success', 'Updated ma_marker #' . $id);

				\Response::redirect('ma/marker');
			}

			else
			{
				\Session::set_flash('error', 'Could not update ma_marker #' . $id);
			}
		}

		else
		{
			if (\Input::method() == 'POST')
			{
				$ma_marker->name = $val->validated('name');
				$ma_marker->zip = $val->validated('zip');
				$ma_marker->pref = $val->validated('pref');
				$ma_marker->address = $val->validated('address');
				$ma_marker->tel = $val->validated('tel');
				$ma_marker->fax = $val->validated('fax');
				$ma_marker->mail = $val->validated('mail');

				foreach((array) \Input::post('sends') as $key => $send) {
					$ma_marker->sends[$key]['ma_carrier_id'] = $send['ma_carrier_id'];
					$ma_marker->sends[$key]['ma_carrier_method_id'] = $send['ma_carrier_method_id'];
				}
				\Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_marker', $ma_marker, false);
		}

		$this->build_form_parts();

        $this->template->set_global('sends', $ma_marker->sends, false);

		$this->template->title = "メーカー";
		$this->template->content = \View::forge('ma/marker/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and \Response::redirect('ma/marker');

		if ($ma_marker = \Model_Ma_Marker::find($id))
		{
			$ma_marker->delete();

			\Session::set_flash('success', 'Deleted ma_marker #'.$id);
		}

		else
		{
			\Session::set_flash('error', 'Could not delete ma_marker #'.$id);
		}

		\Response::redirect('ma/marker');

	}


	/**
	 * @param null $item
	 */
	protected function build_form_parts($item = null)
	{
		$carriers[''] = '配送会社を選択';
		$carriers += \Model_Ma_Carrier::idKeyNameValue();
		\View::set_global('carriers', $carriers, false);

		$carrier_methods[''] = '配送方法を選択';
		$carrier_methods += \Model_Ma_Carrier_Method::idKeyNameValue();
		\View::set_global('carrier_methods', $carrier_methods, false);
	}

}
