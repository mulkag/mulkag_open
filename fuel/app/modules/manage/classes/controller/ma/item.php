<?php
namespace manage;

class Controller_Ma_Item extends Controller_Manage
{

	public function action_index($page = 1)
	{
        $par_page = 30;
        $query = \Model_Ma_item::query();

        $config = [
            'pagination_uri' => Uri::base(false),
            'total_items' => $query->count(),
            'per_page' => $par_page,
            'name' => 'bootstrap',
            'wrapper' => '<ul class="pagination">{pagination}</ul>',
            ];
        $pagination = Pagination::forge('mypagination', $config);
        $data['pagination'] = $pagination;


		$data['blands'] = \Model_Ma_Bland::idKeyNameValue();


        foreach(\Model_Ma_Category::buildLevelList() as $key => $category) {
            $categories[$key][] = 'レベル' . $key . 'カテゴリを選択';
            $categories[$key] += Arr::assoc_to_keyval($category, 'id', 'name');
        }
		$data['categories'] = $categories;


		$data['items'] = \Model_Ma_Item::find('all', ['related' => [
			'categories', 'photos', 'details'
		]]);


		$this->template->title = "Items";
		$this->template->content = \View::forge('ma/item/index', $data);
	}

	public function action_view($id = null)
	{
		is_null($id) and \Response::redirect('ma/item');

		if ( ! $data['item'] = \Model_Ma_Item::find($id))
		{
			\Session::set_flash('error', 'Could not find item #'.$id);
			\Response::redirect('ma/item');
		}

		$this->template->title = "Item";
		$this->template->content = \View::forge('ma/item/view', $data);

	}

	public function action_create()
	{
		if (\Input::method() == 'POST')
		{
			$val = \Model_Ma_Item::validate('create');

			if ($val->run())
			{
                \DB::start_transaction();

				$item = $this->build_save_object(\Model_Ma_Item::forge());

				if ($item and $item->save())
				{
                    \DB::commit_transaction();

					\Session::set_flash('success', 'Added item #'.$item->id.'.');

					\Response::redirect('ma/item');
				}else{
                    \DB::rollback_transaction();
					\Session::set_flash('error', 'Could not save item.');
				}
			}else{
				\Session::set_flash('error', $val->error());
			}
		}

		$this->build_form_parts();

        if(\Input::post('item_details')) {
            $item_details = \Input::post('item_details');
            \View::set_global('item_details', $item_details, false);
        }

		$this->template->title = "Items";
		$this->template->content = \View::forge('ma/item/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and \Response::redirect('ma/item');

        if ( ! $item = \Model_Ma_Item::find($id, ['related' => [
            'categories', 'photos', 'details'
        ]]))
		{
			\Session::set_flash('error', 'Could not find item #'.$id);
			\Response::redirect('ma/item');
		}

		$val = \Model_Ma_Item::validate('edit');

		if ($val->run())
		{
            \DB::start_transaction();

			$item = $this->build_save_object($item);

			if ($item->save())
			{
                \DB::commit_transaction();

				\Session::set_flash('success', 'Updated item #' . $id);

				\Response::redirect('ma/item');
			}
			else
			{
                \DB::rollback_transaction();
				\Session::set_flash('error', 'Could not update item #' . $id);
			}
		}

		else
		{
			if (\Input::method() == 'POST')
			{
				$item->name = $val->validated('name');
				$item->ma_bland_id = $val->validated('ma_bland_id');
				$item->explain = $val->validated('explain');
				$item->cost_price = $val->validated('cost_price');
				$item->jan_code = $val->validated('jan_code');
				$item->is_hidden = $val->validated('is_hidden');
				$item->is_members = $val->validated('is_members');
				$item->ma_color_id = $val->validated('ma_color_id');
				$item->multina_price = $val->validated('multina_price');
				$item->packing_size = $val->validated('packing_size');

				\Session::set_flash('error', $val->error());
			}

			$this->template->set_global('item', $item, false);
		}

		$this->build_form_parts($item);

        $this->template->set_global('item_details', $item->details, false);

		$this->template->title = "Items";
		$this->template->content = \View::forge('ma/item/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and \Response::redirect('ma/item');

		if ($item = \Model_Ma_Item::find($id))
		{
			$item->delete();

			\Session::set_flash('success', 'Deleted item #'.$id);
		}

		else
		{
			\Session::set_flash('error', 'Could not delete item #'.$id);
		}

		\Response::redirect('ma/item');

	}


	protected function build_save_object($item)
	{
        if($item->id) {
            \Model_Ma_Item_Photo::query()->where('ma_item_id', $item->id)->delete();
            \Model_Ma_Item_Detail::query()->where('ma_item_id', $item->id)->delete();
            \DB::delete('item_category')->where('ma_item_id', $item->id)->execute();
        }

		$item = array(
			'name' => \Input::post('name'),
			'ma_bland_id' => \Input::post('ma_bland_id'),
			'explain' => \Input::post('explain'),
			'cost_price' => \Input::post('cost_price'),
			'jan_code' => \Input::post('jan_code'),
			'is_hidden' => \Input::post('is_hidden', 0),
			'is_members' => \Input::post('is_members', 0),
			'ma_color_id' => \Input::post('ma_color_id'),
			'multina_price' => \Input::post('multina_price'),
			'packing_size' => \Input::post('packing_size'),
        );


        if(\Input::post('photo')) {
            foreach(\Input::post('photo') as $key => $photo) {
                $item->photos[$key] = new \Model_Ma_Item_Photo();
                $item->photos[$key]['photo'] = $this->save_image($photo);
                $item->photos[$key]['order'] = $key + 1;
                $item->photos[$key]['is_primary'] = 0;
                $item->photos[$key]['is_hidden'] = 0;
            }
        }

		// カテゴリ登録
        if(\Input::post("ma_category_id")) {
            foreach(\Input::post("ma_category_id") as $key => $category_id) {
                $item->categories[$key] = \Model_Ma_Category::find($category_id);
            }
        }

		// 商品詳細
		foreach(\Input::post("item_details") as $key => $item_detail) {
			$item->details[$key] = new \Model_Ma_Item_Detail();
			$item->details[$key]['ma_material_id'] = $item_detail['ma_material_id'];
			foreach($item_detail as $name => $detail) {
				if($name == 'ma_material_id') {
					continue;
				}
				$item->details[$key][$name] = $detail ? $detail : \Input::post($name);
			}
		}

		return $item;
	}


    protected function save_image($base64)
    {
        preg_match("/data:image\/([\w]+);/i", $base64, $match);
        $saved_name = \Str::random('unique') . '.' . mb_strtolower($match[1]);
        $saved_fullpath = DOCROOT . 'assets/img/items/' . $saved_name;

        $img = preg_replace("/data:([^,]+),/i", "", $base64);

        if(touch($saved_fullpath)) {
            $fp = fopen($saved_fullpath, 'wb');
            fwrite($fp, base64_decode($img));
            fclose($fp);
        }

        return $saved_name;
    }


	/**
	 * @param null $item
	 */
	protected function build_form_parts($item = null)
	{
		$blands[''] = 'ブランドを選択';
		$blands += \Model_Ma_Bland::idKeyNameValue();
		\View::set_global('blands', $blands, false);

		$materials[''] = '素材を選択';
		$materials += \Model_Ma_Material::idKeyNameValue();
		\View::set_global('materials', $materials, false);

        $colors[''] = '色を選択';
        $colors += \Model_Ma_Color::idKeyNameValue();
        \View::set_global('colors', $colors, false);

        \View::set_global('categories', \Model_Ma_Category::buildLevelList(), false);


        $photos = null;
		if(\Input::post('photo')) {
			$photos = \Input::post('photo');
		}elseif(isset($item->photos)) {
			foreach($item->photos as $photo) {
				$photos[] = \Model_MYModel::convImgBase64(DOCROOT . 'assets/img/items/' . $photo->photo);
			}
		}
		\View::set_global('items_photos', $photos, false);
	}
}
