<?php
namespace manage;

class Controller_ManageUser extends Controller_Manage
{
	const DIRMANAGEUSERPHOTO = DOCROOT . 'assets/img/manageusers/';

	public function action_index()
	{
		$data['manageUsers'] = \Model_ManageUser::find('all');
		$this->template->title = "ManageUsers";
		$this->template->content = \View::forge('manageuser/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and \Response::redirect('manageuser');

		if ( ! $data['manageUser'] = \Model_ManageUser::find($id))
		{
			\Session::set_flash('error', 'Could not find manageUser #'.$id);
			\Response::redirect('manageuser');
		}

		$this->template->title = "ManageUser";
		$this->template->content = \View::forge('manageuser/view', $data);

	}

	public function action_create()
	{
		if (\Input::method() == 'POST')
		{
			$val = \Model_ManageUser::validate('create');

			if ($val->run())
			{
				$photo = null;
				if(\Input::post('photo_data')) {
					$photo = \Model_MYModel::save_image(\Input::post('logo_data'), self::DIRMANAGEUSERPHOTO);
				}
				var_dump($photo);
				exit();

				$manageUser = \Model_ManageUser::forge(array(
					'name' => \Input::post('name'),
					'mail' => \Input::post('mail'),
					'password' =>  \Crypt::encode(\Input::post('password')),
					'is_admin' => \Input::post('is_admin', 0),
					'ma_marker_id' => \Model_MyModel::convToNull(\Input::post('ma_marker_id')),
				));

				if ($manageUser and $manageUser->save())
				{
					\Session::set_flash('success', 'Added manageUser #'.$manageUser->id.'.');

					\Response::redirect('manageuser');
				}

				else
				{
					\Session::set_flash('error', 'Could not save manageUser.');
				}
			}
			else
			{
				\Session::set_flash('error', $val->error());
			}
		}

		$markers[''] = 'メーカーを選択';
		$markers += \Model_Ma_Marker::idKeyNameValue();

		\View::set_global('markers', $markers, false);

		$this->template->title = "Manageusers";
		$this->template->content = \View::forge('manageuser/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and \Response::redirect('manageuser');

		if ( ! $manageUser = \Model_ManageUser::find($id))
		{
			\Session::set_flash('error', 'Could not find manageUser #'.$id);
			\Response::redirect('manageuser');
		}

		$val = \Model_ManageUser::validate('edit');

		if ($val->run())
		{
			$photo = null;
			if(\Input::post('photo_data')) {
				$photo = \Model_MYModel::save_image(\Input::post('photo_data'), self::DIRMANAGEUSERPHOTO);
			}

			$manageUser->name = \Input::post('name');
			$manageUser->mail = \Input::post('mail');
			$manageUser->password = \Crypt::encode(\Input::post('password'));
			$manageUser->is_admin = \Input::post('is_admin', 0);
			$manageUser->ma_marker_id = \Model_MyModel::convToNull(\Input::post('ma_marker_id'));
			$manageUser->photo = $photo;

			if ($manageUser->save())
			{
				\Session::set_flash('success', 'Updated manageUser #' . $id);

				\Response::redirect('manageuser');
			}

			else
			{
				\Session::set_flash('error', 'Could not update manageUser #' . $id);
			}
		}

		else
		{
			if (\Input::method() == 'POST')
			{
				$manageUser->name = $val->validated('name');
				$manageUser->mail = $val->validated('mail');
				$manageUser->password = $val->validated('password');
				$manageUser->is_admin = $val->validated('is_admin');
				$manageUser->ma_marker_id = $val->validated('ma_marker_id');

				\Session::set_flash('error', $val->error());
			}

			$this->template->set_global('manageUser', $manageUser, false);
		}

		$markers[''] = 'メーカーを選択';
		$markers += \Model_Ma_Marker::idKeyNameValue();
		\View::set_global('markers', $markers, false);

		$saved_photo = '';
		if($manageUser->photo) {
			$saved_photo = \Model_MYModel::convImgBase64(self::DIRMANAGEUSERPHOTO . $manageUser->photo);
		}
		\View::set_global('photo', $saved_photo, false);

		$this->template->title = "ManageUsers";
		$this->template->content = \View::forge('manageuser/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and \Response::redirect('manageuser');

		if ($manageUser = \Model_ManageUser::find($id))
		{
			$manageUser->delete();

			\Session::set_flash('success', 'Deleted manageUser #'.$id);
		}

		else
		{
			\Session::set_flash('error', 'Could not delete manageUser #'.$id);
		}

		\Response::redirect('manageuser');

	}

}
