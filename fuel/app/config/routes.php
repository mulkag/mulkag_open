<?php
return array(
	'_root_'  => 'top',
	'_404_'   => 'error',

    'search(/:alpha)(/:num)' => 'search/index/$1/$2',
    'detail(/:num)' => 'detail/index/$1',
    'category(/:any)' => 'search/index/$1',
    'brand(/:any)' => 'search/index/$1',
    'contact(/:num)' => 'contact/index/$1',
);
