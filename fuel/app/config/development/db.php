<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=mulkag',
			'username'   => 'root',
			'password'   => 'u6xTpT7Y',
		),
        'profiling' => true,
	),
);
