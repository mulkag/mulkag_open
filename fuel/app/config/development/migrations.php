<?php
return array(
  'version' => 
  array(
    'app' => 
    array(
      'default' => 
      array(
        0 => '001_create_ma_markers',
        1 => '002_create_ma_blands',
        2 => '003_create_categories',
        3 => '004_create_items',
        4 => '005_create_ma_item_photos',
        5 => '006_create_ma_item_details',
        6 => '007_create_ma_materials',
        7 => '008_create_ma_colors',
        8 => '009_create_manageusers',
        9 => '010_create_ma_carrier_methods',
        10 => '010_create_ma_carriers',
        11 => '010_create_ma_marker_sends',
        12 => '011_create_ma_marker_carriers',
        13 => '012_create_manageuser_histories',
        14 => '013_create_ma_user_histories',
        15 => '014_create_news',
        16 => '014_create_users',
        17 => '015_create_ma_newscategories',
        18 => '016_create_ma_regist_methods',
        19 => '017_create_ma_productions',
        20 => '018_create_ma_item_sends',
        21 => '019_delete_city_from_users',
        22 => '020_create_user_favorites',
        23 => '021_create_contacts',
        24 => '022_add_tracking_number_to_contacts',
        25 => '023_add_notes_to_contacts',
        26 => '024_create_item_purchases',
        27 => '024_create_orders',
        28 => '025_add_ma_color_id_to_item_purchases',
        29 => '025_create_ma_order_statuses',
        30 => '026_create_order_items',
        31 => '027_add_ma_payment_id_to_orders',
        32 => '028_create_ma_payments',
        33 => '029_add_price_to_order_items',
        34 => '030_add_send_to_order_items',
        35 => '031_add_name_en_to_ma_categories',
        36 => '032_add_name_en_to_ma_blands',
        37 => '033_add_original_name_to_ma_items',
        38 => '034_add_selling_price_to_ma_items',
        39 => '035_add_material_detail_to_ma_items',
        40 => '036_add_note_to_ma_items',
        41 => '037_add_location_to_ma_blands',
      ),
    ),
    'module' => 
    array(
    ),
    'package' => 
    array(
    ),
  ),
  'folder' => 'migrations/',
  'table' => 'migration',
);
