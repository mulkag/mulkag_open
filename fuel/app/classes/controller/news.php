<?php
class Controller_News extends Controller_Template
{

	public function action_index()
	{
		$data['news'] = Model_News::find('all');
		$this->template->title = "News";
		$this->template->content = View::forge('news/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('news');

		if ( ! $data['news'] = Model_News::find($id))
		{
			Session::set_flash('error', 'Could not find news #'.$id);
			Response::redirect('news');
		}

		$this->template->title = "News";
		$this->template->content = View::forge('news/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_News::validate('create');

			if ($val->run())
			{
				$news = Model_News::forge(array(
					'title' => Input::post('title'),
					'url' => Input::post('url'),
					'content' => Input::post('content'),
					'ma_newscategory_id' => Input::post('ma_newscategory_id'),
				));

				if ($news and $news->save())
				{
					Session::set_flash('success', 'Added news #'.$news->id.'.');

					Response::redirect('news');
				}

				else
				{
					Session::set_flash('error', 'Could not save news.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "News";
		$this->template->content = View::forge('news/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('news');

		if ( ! $news = Model_News::find($id))
		{
			Session::set_flash('error', 'Could not find news #'.$id);
			Response::redirect('news');
		}

		$val = Model_News::validate('edit');

		if ($val->run())
		{
			$news->title = Input::post('title');
			$news->url = Input::post('url');
			$news->content = Input::post('content');
			$news->ma_newscategory_id = Input::post('ma_newscategory_id');

			if ($news->save())
			{
				Session::set_flash('success', 'Updated news #' . $id);

				Response::redirect('news');
			}

			else
			{
				Session::set_flash('error', 'Could not update news #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$news->title = $val->validated('title');
				$news->url = $val->validated('url');
				$news->content = $val->validated('content');
				$news->ma_newscategory_id = $val->validated('ma_newscategory_id');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('news', $news, false);
		}

		$this->template->title = "News";
		$this->template->content = View::forge('news/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('news');

		if ($news = Model_News::find($id))
		{
			$news->delete();

			Session::set_flash('success', 'Deleted news #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete news #'.$id);
		}

		Response::redirect('news');

	}

}
