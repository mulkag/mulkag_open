<?php
class Controller_Contact extends Controller_OpenTemplate
{
    public function before()
    {
        parent::before();
        Asset::css(['pc_contact.min.css'],[], 'add_css', false);
        Asset::js(['pc_contact.min.js'],[], 'add_js', false);
    }


    public function action_index()
    {
          $this->template->breadcrumbs = [
              [
                  "name" => "お問い合せ",
                  "link" => "/contact"
              ],
          ];
        if(! Session::get('contact.is_confirm')) {
            Session::delete('contact');
        }else{
            Session::delete('contact.is_confirm');
        }

        $val = Model_Contact::validate('contact');

        if(! $val->run()) {
            $data['errors'] = $val->error();
            $this->template->content = View_Twig::forge('contact/index', $data);
            return;
        }
        // if($_FILES['upfile']['name']) {
        //     $config = [
        //         'path' => DOCROOT.'tmp',
        //         'randomize' => true,
        //         'ext_whitelist' => ['jpg','jpeg', 'png', 'gif'],
        //     ];
        //     Upload::process($config);
        //     if(Upload::is_valid()) {
        //         Upload::save();
        //     }else{
        //         $data['errors'] = Upload::get_errors();
        //         $this->template->content = View_Twig::forge('contact/index', $data);
        //         return;
        //     }
        //     Session::set('contact.file', Upload::get_files()[0]['saved_as']);
        // }
        foreach(Input::post() as $key => $post) {
            if(! $post) {
                continue;
            }
            Session::set('contact.'.$key, $post);
        }

        Response::redirect('contact/confirm', 'refresh');
    }


    public function action_confirm()
    {
          $this->template->breadcrumbs = [
              [
                  "name" => "お問い合せ",
                  "link" => "/contact"
              ],
              [
                  "name" => "お問い合せ内容の確認",
                  "link" => "/contact/confirm"
              ],
          ];
        Session::set('contact.is_confirm', true);
        $val = Model_Contact::validate('contact');

        if(! $val->run()) {
            $this->template->content = View_Twig::forge('contact/confirm');
            return;
        }

        $data = [
            'category' => Input::post('category'),
            'name' => Input::post('name'),
            'mail' => Input::post('mail'),
            'content' => Input::post('content'),
            'tracking_number' => Input::post('tracking_number'),
            'order_number' => Input::post('order_number'),
            'photo' => Input::post('file'),
            'notes' => Input::post('note'),
        ];
        $contact = Model_Contact::forge($data);
        $contact->save();

        $mail_body = Model_Contact::buildMailBody($data);

        $email = Email::forge();
        $email->from('info@mulkag.com', 'マルカグ');
        $email->to($data["mail"]);
        $email->bcc(array(
            'yanagisawa@ultrasevenstar.com',
            'info@mulkag.com',
        ));
        $email->subject("【MULKAG】お問合せを受け付けました");
        $email->body($mail_body);

        try{
            $email->send();
        }
        catch(\EmailSendingFailedException $e)
        {
            echo "dame1";
            // ドライバがメールを送信できなかった。
        }
        catch(\EmailValidationFailedException $e)
        {
            echo "dame2";

            // 1 つ以上のメールアドレスがバリデーションで失敗した。
        }

        if(Input::post('file')) {
            File::renme(DOCROOT.'public/tmp/'.Input::post('file'), DOCROOT.'public/assets/img/contact/'.Input::post('file'));
        }

        Response::redirect('contact/thanks', 'refresh');
    }


    public function action_thanks()
    {
          $this->template->breadcrumbs = [
              [
                  "name" => "お問い合せ",
                  "link" => "/contact"
              ],
              [
                  "name" => "お問い合せ内容の確認",
                  "link" => "/contact/confirm"
              ],
              [
                  "name" => "お問い合せ完了",
                  "link" => "/contact/thanks"
              ],
          ];
        Session::delete('contact');
        $this->template->content = View_Twig::forge('contact/thanks');
    }
}
