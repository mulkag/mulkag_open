<?php
class Controller_Search extends Controller_OpenTemplate
{

    public function action_index()
    {
        Asset::css(['pc_search.min.css'],[], 'add_css', false);
        Asset::js(['pc_search.min.js', 'plugins/vue.js', 'plugins/search.js'],[], 'add_js', false);

        $this->template->content = Response::forge(Presenter::forge('search/index'));

        $this->template->breadcrumbs = [
            [
                "name" => "取り扱い家具一覧",
                "link" => "/search"
            ],
        ];
    }



}
