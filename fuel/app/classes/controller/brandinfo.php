<?php
class Controller_BrandInfo extends Controller_OpenTemplate
{
    public function action_index()
    {
      Asset::css(['pc_brand.min.css'],[], 'add_css', false);
      Asset::js(['pc_brand.min.js'],[], 'add_js', false);
      $this->template->content = View_Twig::forge('brandInfo/index');

      $this->template->breadcrumbs = [
          [
              "name" => "取り扱いブランド一覧",
              "link" => "/brandInfo"
          ],
      ];
    }
    public function action_bina()
    {
      Asset::css(['pc_brand_detail.min.css'],[], 'add_css', false);
      Asset::js(['pc_brand_detail.min.js'],[], 'add_js', false);
      $this->template->content = View_Twig::forge('brandInfo/bina');

      $this->template->breadcrumbs = [
          [
              "name" => "取り扱いブランド一覧",
              "link" => "/brandInfo/"
          ],
          [
              "name" => "bina(ビーナ)",
              "link" => "/brandInfo/bina"
          ],
      ];
    }
    public function action_dbodhi()
    {
      Asset::css(['pc_brand_detail.min.css'],[], 'add_css', false);
      Asset::js(['pc_brand_detail.min.js'],[], 'add_js', false);
      $this->template->content = View_Twig::forge('brandInfo/dbodhi');

      $this->template->breadcrumbs = [
          [
              "name" => "取り扱いブランド一覧",
              "link" => "/brandInfo"
          ],
          [
              "name" => "dbodhi(ディーボディ)",
              "link" => "/brandInfo/dbodhi"
          ],
      ];
    }
    public function action_halo()
    {
      Asset::css(['pc_brand_detail.min.css'],[], 'add_css', false);
      Asset::js(['pc_brand_detail.min.js'],[], 'add_js', false);
      $this->template->content = View_Twig::forge('brandInfo/halo');

      $this->template->breadcrumbs = [
          [
              "name" => "取り扱いブランド一覧",
              "link" => "/brandInfo"
          ],
          [
              "name" => "halo(ハロ)",
              "link" => "/brandInfo/halo"
          ],
      ];
    }
    public function action_metalsistem()
    {
      Asset::css(['pc_brand_detail.min.css'],[], 'add_css', false);
      Asset::js(['pc_brand_detail.min.js'],[], 'add_js', false);
      $this->template->content = View_Twig::forge('brandInfo/metalsistem');

      $this->template->breadcrumbs = [
          [
              "name" => "取り扱いブランド一覧",
              "link" => "/brandInfo"
          ],
          [
              "name" => "metalsistem(メタルシステム)",
              "link" => "/brandInfo/metalsistem"
          ],
      ];
    }


}
