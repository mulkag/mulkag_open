<?php

class Controller_OpenTemplate extends Controller_Template
{
    public $template = 'template_pc.twig';

    public function before()
    {
        if(Agent::is_mobiledevice()) {
            $this->template = 'template_pc.twig';
        }

        parent::before();
//        $this->getFavoriteList();
        $this->template->carts = count(Cookie::get('cart'));
        $this->setTdk();

        $this->template->symbols = View::forge('symbols');

        $this->template->tax = 1.08;
    }


    public function getFavoriteList()
    {
        if(! Session::get('user.id')) {
            return;
        }
        $favorites = DB::select('name', 'photo', 'ma_items.id')
            ->from('user_favorites')
            ->where('user_id', Session::get('user.id'))
            ->join('ma_items')
            ->on('user_favorites.ma_item_id', '=', 'ma_items.id')
            ->join('ma_item_photos')
            ->on('ma_item_photos.ma_item_id', '=', 'ma_items.id')
            ->where('is_primary', '=', 1)
            ->execute();
        if($favorites) {
            $this->template->favorites = $favorites;
        }

    }


    protected function setLoginInfo($user_id)
    {
		$user = Model_User::query()->where('id', $user_id)->get_one();
		if(! $user) {
			return false;
		}

		$user->last_login = time();
		$user->hush = \Crypt::encode(Session::key('session_id') + time());
		$user->save();

        Session::delete('user.id');
        Session::delete('user.hush');

        Session::set('user.id', $user->id);
        Session::set('user.hush', $user->hush);
		return $user;
    }


    protected function setTdk()
    {
        $controller = Uri::segment(1);
        $method = Uri::segment(2);
        $tdk = Config::get("tdk");

        if ($method && isset($tdk[$controller][$method])) {
            $this->template->title = $tdk[$controller][$method]["title"];
            $this->template->description = $tdk[$controller][$method]["description"];
            $this->template->keywords = $tdk[$controller][$method]["keywords"];
        }else if(isset($tdk[$controller])) {
            $this->template->title = $tdk[$controller]["title"];
            $this->template->description = $tdk[$controller]["description"];
            $this->template->keywords =  $tdk[$controller]["keywords"];
        }else{
            $this->template->title = "家具・インテリアの総合通販サイト|MULKAG【マルカグ】";
            $this->template->description = "家具・収納・インテリアの通販サイトならマルカグ。有名ブランドのソファー・テーブル・椅子・収納を豊富に取り揃え。家具・インテリアにこだわる人のための選び抜かれた家具通販サイト。";
            $this->template->keywords = "家具,インテリア,通販,ブランド,ソファ,テーブル,椅子,チェア,インテリア,収納,MULKAG,マルカグ";
        }
    }
}
