<?php

class Controller_Regist extends Controller_OpenTemplate
{

	public function action_index()
	{
        if(! Session::get_flash('is_confirm')) {
            Session::delete('form');
        }

		$val = \Model_User::validate('create');
		if (\Input::method() == 'POST' && $val->run()) {
			Session::set('form', Input::post());
			Response::redirect('regist/conrim');
			return;
		}

        $data['errors'] = $val->error();
        Asset::css(['pc_regist.min.css'],[], 'add_css', false);
        Asset::js(['pc_regist_input.min.js'],[], 'add_js', false);
		$this->template->content = \View_Twig::forge('regist/index', $data);
	}


	public function action_conrim()
	{
        Session::set_flash('is_confirm', true);

		$val = \Model_User::validate('create');
		if (\Input::method() == 'POST' && $val->run()) {
			Session::set('form', Input::post());
			Response::redirect('regist/thanks');
			return;
		}

        Asset::css(['pc_regist.min.css'],[], 'add_css', false);
		$this->template->content = \View_Twig::forge('regist/confirm');
	}


	public function action_thanks()
	{
        Asset::css(['pc_regist.min.css'],[], 'add_css', false);

		$user = \Model_User::forge(array(
			'mail' => \Session::get('form.mail'),
			'password' =>  hash('sha256', \Session::get('form.password')),
			'ma_regist_method_id' => 1,
            'zip' => \Session::get('form.zip'),
            'pref' => \Session::get('form.pref'),
            'address' => \Session::get('form.address'),
            'tel' => \Session::get('form.tel'),
            'name' => \Session::get('form.name'),
            'kana' => \Session::get('form.kana'),
		));


		if ($user->save()) {
            $this->setLoginInfo($user->id);
			Session::delete('form');
			Session::delete('is_confirm');
			$this->template->content = \View_Twig::forge('regist/thanks');
		}
	}
}
