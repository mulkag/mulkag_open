<?php
class Controller_Detail extends Controller_OpenTemplate
{

    public function action_index()
    {
        Asset::css(['pc_search_detail.min.css'], [], 'add_css', false);
        Asset::js(['pc_search_detail.min.js'], [], 'add_js', false);
        $item_id = (int) Uri::segment(2);

        $item = Model_Ma_Item::find($item_id, [
            'related' => [
                'photos',
                'colors',
                'production',
                'materials',
                'categories',
                'sends' => [
                    'related' => [
                        'carrier',
                        'carrier_method'
                    ]
                ],
                'bland' => [
                    'related' => 'marker'
                ],
            ]
        ]);
        $data["item"] = $item;
        // var_dump($item);
        // exit;
        $this->template->content = \View_Twig::forge('detail/index', $data);
        $this->template->item = $item;
        $this->template->title = "{$item['name']}({$item['bland']['name']})｜家具通販サイトの【MULKAG】";
        $this->template->description = "{$item['name']}({$item['bland']['name']})に関する詳細ページです。商品に関するお問合せもこちらのページから。家具・インテリアにこだわる人のための選び抜かれた家具通販サイト。";
        $this->template->keywords = "{$item['name']},{$item['bland']['name']},家具,インテリア,通販,MULKAG,マルカグ";
        $this->template->breadcrumbs = [
            [
                "name" => "取り扱い家具一覧",
                "link" => "/search"
            ],
            [
                "name" => "{$item['bland']['name']} | {$item['name']}",
                "link" => "/search/detail/{$item['id']}"
            ],
        ];
    }


    public function action_cart()
    {
        $id = (int) Input::post('id');

        foreach(Input::post() as $key => $input) {
            if($key == 'id') {
                continue;
            }
            Cookie::set("cart[{$id}][{$key}]", $input);
        }
        Response::redirect('cart');
    }


    public function action_confirm()
    {
      var_dump('AA');
      exit;
        $data = [
            'ma_item_id' => \Input::post('id'),
            'mail' => \Input::post('mail', 'hoge@hoge.com'),
            'itma_item_purchase_status' => 1,
            'ma_color_id' => \Input::post('color'),
            'number' => \Input::post('number'),
        ];
        $item_purchase = \Model_Item_Purchase::forge($data);
        $item_purchase->save();

        $email = \Email::forge();
        $email->from('yanagisawa@ultrasevenstar.com', 'My Name');
        $email->to('yanagisawa@ultrasevenstar.com', 'Johny Squid');
        $email->subject('This is the subject');

        // 本文を指定する。
        $email->body('This is my message');

        try
        {
            $email->send();
        }
        catch(\EmailValidationFailedException $e)
        {
            var_dump('a');
            exit();
            // バリデーションが失敗したとき
        }
        catch(\EmailSendingFailedException $e)
        {
            var_dump('b');
            exit();

            // ドライバがメールを送信できなかったとき
        }

    }
}
