<?php

class Controller_Api extends Controller_Rest
{
    protected $format = 'json';

    public function before()
    {
        parent::before();

        if(! Input::is_ajax()){
            // 例外処理をかく
            // https://groups.google.com/forum/#!topic/fuelphp_jp/uoHjIbfWCGs
        }
    }


    public function get_search()
    {
        $orders = [
            1 => ['column' => 'created_at', 'sort_order' => 'DESC'],
            3 => ['column' => 'multina_price', 'sort_order' => 'ASC'],
            4 => ['column' => 'multina_price', 'sort_order' => 'DESC'],
        ];

        $offset = (int) Input::get('offset', 1)-1;
        $limit = (int) Input::get('limit');
        $order = (int) Input::get('order', 1);
        $categories = Input::get('categories');
        $blands = Input::get('blands');
        $materials = Input::get('materials');

        if(! $limit) {
            return $this->response([
                'is_success' => false
            ]);
        }
        $offset = $limit * $offset;

        $query = Model_Ma_Item::query()
            ->order_by($orders[$order]['column'], $orders[$order]['sort_order'])
            ->related([
                'photos' =>
                    ['where' => [['is_primary','=', 1]]]
            ]);
        // カテゴリ絞り込み
        if(count($categories) > 0) {
            $query = $query->related(['categories' => ['where' => [['id', 'in', $categories]]]]);
        }
        // ブランド絞り込み
        if(count($blands) > 0) {
            $query = $query->where('ma_bland_id', 'in', $blands);
        }
        // 素材絞り込み
        if(count($materials) > 0) {
            $query = $query->related(['materials' => ['where' => [['id', 'in', $materials]]]]);
        }
        $number_of_items = $query->count();
        $ma_items = $query->rows_limit($limit)->rows_offset($offset)->get();

        // JS側で並び順が変わるため配列に変換
        foreach($ma_items as $key => $item) {
            $items[] = $ma_items[$key];
        }

        return $this->response([
            'is_success' => true,
            'items' => $items,
            'number' => $number_of_items,
        ]);
    }


    public function post_favorite()
    {
        if(! Session::get('user.id')) {
            return $this->response([
                'is_success' => false
            ]);
        }
        if(! Input::post('item')) {
            return $this->response([
                'is_success' => false
            ]);
        }

        $data = [
            'ma_item_id' => Input::post('item'),
            'user_id' => Session::get('user.id'),
        ];
        $user_favorite = Model_User_Favorite::forge($data);
        $user_favorite->save();

        return $this->response([
            'is_success' => true
        ]);
    }


    public function delete_favorite()
    {
        if(! Session::get('user.id')) {
                return $this->response([
                'message' => 'ログインしてください',
                'is_success' => false
            ]);
        }
        if(! Input::delete('item')) {
            return $this->response([
                'message' => '不正なパラメータです',
                'is_success' => false
            ]);
        }

        $user_favorite = Model_User_Favorite::query()->where('user_id', Session::get('user.id'))->where('ma_item_id', Input::delete('item'))->get_one();
        if(! $user_favorite) {
            return $this->response([
                'message' => 'アイテムが未登録です',
                'is_success' => false
            ]);
        }
        $user_favorite->delete();

        return $this->response([
            'is_success' => true
        ]);
    }


    public function get_favorite()
    {
        if(! Session::get('user.id')) {
                return $this->response([
                'message' => 'ログインしてください',
                'is_success' => false
            ]);
        }
        $user_favorites = Model_User_Favorite::query()->select('ma_item_id')->where('user_id', Session::get('user.id'))->get();

        if(! $user_favorites) {
            return $this->response([
                'is_success' => true,
                'favorites' => []
            ]);
        }
        foreach($user_favorites as $user_favorite) {
            $favorites[] = $user_favorite->ma_item_id;
        }

        return $this->response([
            'is_success' => true,
            'favorites' => $favorites
        ]);
    }


    public function delete_cart()
    {
        $id = (int) Input::delete('id');
        foreach(Cookie::get('cart')[$id] as $key => $cart) {
            \Cookie::delete("cart[{$id}][{$key}]");
        }

        return $this->response([
            'is_success' => true
        ]);
    }
}
