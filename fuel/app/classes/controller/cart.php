<?php
class Controller_Cart extends Controller_OpenTemplate
{
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        Asset::css(['pc_cart.min.css'], [], 'add_css', false);
        Asset::js(['pc_cart.min.js', 'plugins/cart.js'], [], 'add_js', false);
        $this->template->content = Response::forge(Presenter::forge('cart/index'));


        $this->template->breadcrumbs = [
            [
                "name" => "カート",
                "link" => "/cart/"
            ],
        ];
    }


    public function action_Thanks()
    {
        $val = Model_Order::validate('order');

        if(! $val->run()) {
            Session::set_flash('order', Input::post());
            Response::redirect('cart');
        }
        list($order_id, $total_price, $total_send_price) = Model_Order::addOrder();
        $this->sendMail($order_id, $total_price, $total_send_price);

        if(count(\Cookie::get('cart'))) {
            foreach(\Cookie::get('cart') as $id => $cart) {
                foreach($cart as $key => $data) {
                    Cookie::delete("cart[{$id}][{$key}]");
                }
            }
        }

        Asset::css(['pc_cart_thanks.min.css'], [], 'add_css', false);
        Asset::js(['pc_cart.min.js', 'plugins/cart.js'], [], 'add_js', false);

        $data["order_id"] = $order_id;
        $this->template->content = \View_Twig::forge('cart/thanks', $data);
    }


    public function action_confirm()
    {
        $val = Model_Order::validate('order');

        if(! $val->run() || Input::post('is_return')) {
            Session::set_flash('order', Input::post());
            Response::redirect('cart');
        }

        Asset::css(['pc_cart.min.css'], [], 'add_css', false);


        $this->template->breadcrumbs = [
            [
                "name" => "カート",
                "link" => "/cart/"
            ],
            [
                "name" => "ご注文内容の確認",
                "link" => "/cart/confirm/"
            ]
        ];

        $this->template->content = Response::forge(Presenter::forge('cart/confirm'));
        return;
    }


    public function action_settle()
    {
        // var_dump(Input::post());
        // exit;
        $url = 'https://beta.epsilon.jp/cgi-bin/order/receive_order3.cgi';
        $param = [
            'contract_code'  => '64081800',
            "xml"           => 1,
            'user_id'       => 0,
            'user_name'     => \Input::post('name'),
            'user_mail_add' => \Input::post('mail'),
            'item_code'     => implode(',', \Input::post('id')),
            'item_name'     => mb_strimwidth(implode(',', \Input::post('item_name')), 0, 32, "..."),
            'order_number'  => rand(0, 99999999),
            // 'st_code'       => $_POST['st_code'],
            // 'mission_code'  => $_POST['mission_code'],
            // 'item_price'    => \Input::post('price')[0],
            'item_price'    => 1,
            // 'process_code'  => $_POST['process_code'],
            'process_code' => 1,
            'mission_code' => 1
        ];
        $options = array('http' => array(
            'method' => 'POST',
            'content' => http_build_query($param),
            'header' => 'Content-Type: application/x-www-form-urlencoded',
        ));
        $contents = file_get_contents($url, false, stream_context_create($options));
        $result = simplexml_load_string($contents);


        Response::redirect(urldecode($result->result[1]['redirect']), 'refresh');
        exit;

    }


    public function sendMail($order_id, $total_price, $total_send_price)
    {
        $total_price = number_format($total_price);
        $total_send_price = number_format($total_send_price);

        $input = Input::post();
        $ma_item = Model_Ma_Item::idKeyNameValue();

        $item_send = Model_Item_Send::find('all', ['related' => ["carrier_method"]]);

        $item_detail = "";
        foreach(Input::post("item_id") as $key => $item_id) {
            $item_detail .= "商品名：{$ma_item[$item_id]}\n";
            $item_detail .= "個数：{$input['number'][$key]}個\n";
            $item_detail .= "価格：{$input['price'][$key]}円\n";
            $item_detail .= "配送方法：{$item_send[$input['send_id'][$key]]["carrier_method"]["name"]}\n\n\n";
        }

        $payment = Model_Ma_Payment::find($input["payment"])->name;


        $subject = "【MULKAG】ご注文を受け付けました";

        $mail_body = <<<EOF
{$input["name"]}様

この度はMULKAGをご利用頂き、誠にありがとうございます。
短い間ではございますが、お取引完了まで宜しくお願いいたします。

本日、以下のご注文を承りました。
在庫確認後、改めてご連絡差し上げます。

恐れ入りますが、
今しばらくお待ちいただけますよう、よろしくお願い申し上げます。

----------------------------------
    注文内容
----------------------------------
注文番号：{$order_id}
{$item_detail}
送料：{$total_send_price}円
合計：{$total_price}円

----------------------------------
    お届け先
----------------------------------
お名前：{$input["name"]}
ふりがな：{$input["kana"]}
郵便番号：{$input["zip"]}
ご住所：{$input["pref"]}{$input["city"]}{$input["address-etc"]}
お電話番号：{$input["tel"]}
メールアドレス：{$input["mail"]}

【支払い方法】
{$payment}
【配達希望日時】
最短でのお届けを希望する
EOF;

        $email = Email::forge();
        $email->from('info@mulkag.com', 'マルカグ');
        $email->to($input["mail"]);
        $email->bcc(array(
            'info@mulkag.com',
            'yanagisawa@ultrasevenstar.com',
        ));
        $email->subject($subject);
        $email->body($mail_body);

        try{
            $email->send();
        }
        catch(\EmailSendingFailedException $e)
        {
            echo "dame1";
            // ドライバがメールを送信できなかった。
        }
        catch(\EmailValidationFailedException $e)
        {
            echo "dame2";

            // 1 つ以上のメールアドレスがバリデーションで失敗した。
        }
    }

}
