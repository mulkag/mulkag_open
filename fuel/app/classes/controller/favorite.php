<?php
class Controller_Favorite extends Controller_OpenTemplate
{
    public function action_index()
    {
        Asset::css(['pc_favorite.min.css'],[], 'add_css', false);
        Asset::js(['pc_favorite.min.js'],[], 'add_js', false);
        $this->template->content = View_Twig::forge('favorite/index');
    }
}
