<?php

class Controller_Guide extends Controller_OpenTemplate
{
    public function action_index() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/index');
        $this->template->breadcrumbs = [
            [
                "name" => "ご利用ガイド",
                "link" => "/guide"
            ],
        ];
    }


    public function action_Cancel() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/cancel');
        $this->template->breadcrumbs = [
            [
                "name" => "ご利用ガイド",
                "link" => "/guide"
            ],
            [
                "name" => "返品・交換について",
                "link" => "/guide/cancel"
            ],
        ];
    }


    public function action_Controll() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/controll');
    }


    public function action_Item() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/item');
        $this->template->breadcrumbs = [
            [
                "name" => "ご利用ガイド",
                "link" => "/guide"
            ],
            [
                "name" => "商品について",
                "link" => "/guide/item"
            ],
        ];
    }


    public function action_Member() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/member');
        $this->template->breadcrumbs = [
            [
                "name" => "ご利用ガイド",
                "link" => "/guide"
            ],
            [
                "name" => "MULKAG会員について",
                "link" => "/guide/member"
            ],
        ];
    }


    public function action_Order() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/order');
        $this->template->breadcrumbs = [
            [
                "name" => "ご利用ガイド",
                "link" => "/guide"
            ],
            [
                "name" => "ご注文について",
                "link" => "/guide/order"
            ],
        ];
    }


    public function action_Other() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/other');
        $this->template->breadcrumbs = [
            [
                "name" => "ご利用ガイド",
                "link" => "/guide"
            ],
            [
                "name" => "その他",
                "link" => "/guide/other"
            ],
        ];
    }


    public function action_Pay() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/pay');
        $this->template->breadcrumbs = [
            [
                "name" => "ご利用ガイド",
                "link" => "/guide"
            ],
            [
                "name" => "お支払いについて",
                "link" => "/guide/pay"
            ],
        ];
    }


    public function action_Postage() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/postage');
        $this->template->breadcrumbs = [
            [
                "name" => "ご利用ガイド",
                "link" => "/guide"
            ],
            [
                "name" => "お届けについて",
                "link" => "/guide/postage"
            ],
        ];
    }


    public function action_Site() {
        Asset::css(['pc_guide.min.css'],[], 'add_css', false);
        Asset::js(['pc_guide.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/guide/site');
        $this->template->breadcrumbs = [
            [
                "name" => "ご利用ガイド",
                "link" => "/guide"
            ],
            [
                "name" => "サイトについて",
                "link" => "/guide/site"
            ],
        ];
    }
}
