<?php
class Controller_Top extends Controller_OpenTemplate
{
    /**
     * The basic welcome message
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        Asset::css(['pc_top.min.css'],[], 'add_css', false);
        Asset::js(['pc_top.min.js'],[], 'add_js', false);

        $this->template->content = Response::forge(Presenter::forge('top/index'));
    }



}
