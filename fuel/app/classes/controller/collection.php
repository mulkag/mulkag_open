<?php
class Controller_Collection extends Controller_OpenTemplate
{
    public function action_index()
    {
        Asset::css(['pc_collection.min.css'],[], 'add_css', false);
        Asset::js(['pc_collection.min.js'],[], 'add_js', false);
        $this->template->content = View_Twig::forge('collection/index');
    }
    public function action_detail($page)
    {
        return View::forge('collection/detail/'.$page);
    }
}
