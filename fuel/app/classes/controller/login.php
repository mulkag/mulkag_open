<?php

class Controller_Login extends Controller_OpenTemplate
{

	public function action_index()
	{
        Asset::css(['pc_login.min.css'], [], 'add_css', false);
        Asset::js(['pc_login.min.js'],[], 'add_js', false);

        $password = Input::post('password');
        $mail = Input::post('mail');
        $self = $this;

		$val = Validation::forge('login');
		$val->add('password', 'パスワード')->add_rule('required');

        $val->add('mail', 'メール')
            ->add_rule('required')
            ->add_rule('valid_email')
            ->add_rule('trim')
            ->add_rule(
                function($mail) use ($self, $password) {
                    if(! $mail || ! $password) {
                        return true;
                    }
                    $user = Model_User::query()
                        ->where('mail', Input::post('mail'))
                        ->where('password', hash('sha256', Input::post('password')))
                        ->get_one();

                    if($user) {
                        $self->setLoginInfo($user->id);
                        return true;
                    } else {
                        Validation::active()->set_message('closure', 'ログインに失敗しました');
                        return false;
                    }
                });

        $data['errors'] = $val->error();

		if (! $val->run()) {
        	$this->template->content = Response::forge(Presenter::forge('login/index')->set('data', ['errors' => $val->error()]));
        	return;
		}

        Response::redirect('top', 'refresh');

	}

}
