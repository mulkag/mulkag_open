<?php
class Controller_Convert extends Controller
{
	public function action_index()
	{
		$csv = Format::forge(File::read('csv/item/20170314.csv', true), 'csv')->to_array();

		foreach ($csv as $key => $rows) {
			// var_dump($rows);
			// exit;
/*
			$material = Model_Ma_Material::query()->where("name", $rows["素材1"])->get_one();

			if(! $material && $rows["素材1"]) {
				$material = Model_Ma_Material::forge();
				$material->name = $rows["素材1"];
				$material->save();
			}

			$category = Model_Ma_Category::query()->where("name", $rows["商品カテゴリ"])->get_one();
			$category_parent_id = $category->id;

			$category_parent_id = $this->addCategory($rows["タイプ"], 2, $category_parent_id);
			$category_parent_id = $this->addCategory($rows["タイプ（子）"], 3, $category_parent_id);
			$category_parent_id = $this->addCategory($rows["タイプ（孫）"], 4, $category_parent_id);
*/


			// exit;
			$brand = Model_Ma_Bland::query()->where("name", $rows["ブランド名"])->get_one();
			if(!$brand->location) {
				$brand->location = $rows["ブランド所在地"];
				$brand->save();
			}

			$production = Model_Ma_Production::query()->where("name", $rows["生産形態"])->get_one();


			$item = Model_Ma_Item::forge();

			$item->categories[] = Model_Ma_Category::query()->where("name", $rows["商品カテゴリ"])->get_one();
			if($rows["タイプ"]) {
				$item->categories[] = Model_Ma_Category::query()->where("name", $rows["タイプ"])->get_one();
			}
			if($rows["タイプ（子）"]) {
				$item->categories[] = Model_Ma_Category::query()->where("name", $rows["タイプ（子）"])->get_one();
			}
			if($rows["タイプ（孫）"]) {
				$item->categories[] = Model_Ma_Category::query()->where("name", $rows["タイプ（孫）"])->get_one();
			}

			$colors = explode(",", $rows["色"]);
			foreach($colors as $color) {
				$item->colors[] = Model_Ma_Color::query()->where("name", $color)->get_one();
			}

			if($rows["素材1"]) {
				$item->materials[] = Model_Ma_Material::query()->where("name", $rows["素材1"])->get_one();
			}

			$item->name = $rows["サイト表示名"];
			$item->original_name = $rows["商品名"];
			$item->ma_bland_id = $brand->id;
			$item->cost_price = $rows["下代（税別）"];
			$item->selling_price = $rows["上代（税別）"];
			$item->jan_code = $rows["JANコード"];
			$item->multina_price = $rows["販売価格（税別）"];
			$item->ma_production_id = $production->id;
			$item->is_members = 0;
			// $item->weight = $rows[""];

			if($rows["W：横幅"] != "-") {
				$item->size_w = $rows["W：横幅"];
			}
			if($rows["D：奥行き"] != "-") {
				$item->size_d = $rows["D：奥行き"];
			}
			if($rows["H：高さ"] != "-") {
				$item->size_h = $rows["H：高さ"];
			}
			if($rows["SH：床から座までの高さ"] != "-") {
				$item->size_sh = $rows["SH：床から座までの高さ"];
			}

			if($rows["素材2"]) {
				$item->material_detail = $rows["素材2"];
			}
			if($rows["備考"]) {
				$item->note = $rows["備考"];
			}

			$item->save();

			$this->getSend(1, $rows, $item->id);
			$this->getSend(2, $rows, $item->id);
			$this->getSend(3, $rows, $item->id);

			$this->movePhoto($item->id);

		}

	}

	protected function addCategory($name, $level, $parent_id)
	{
		if(! $name) {
			return;
		}
		$category = Model_Ma_Category::query()->where("name", $name)->where("level", $level)->get_one();

		if($category) {
			return;
		}

		$category = Model_Ma_Category::forge();
		$category->name = $name;
		$category->parent_id = $parent_id;
		$category->level = $level;
		$category->save();
		return $category->id;
	}


	protected function getSend($number, $rows, $item_id)
	{
		if($rows["備考{$number}"] == "不可") {
			return;
		}

		$send_name = $rows["配送会社{$number}"];
		if($send_name == "西濃") {
			$send_name = "西濃運輸";
		}

		$carrier = Model_Ma_Carrier::query()->where("name", $send_name)->get_one();
		$method = Model_Ma_Carrier_Method::query()->where("name", $rows["配送方法{$number}"])->get_one();
		$price = $rows["配送料{$number}"];

		if(! $carrier) {
			var_dump($carrier);
		}
		if(! $method) {
			var_dump($rows["配送方法{$number}"]);
		}

		$item_send = Model_Item_Send::forge();
		$item_send->ma_item_id = $item_id;
		$item_send->ma_carrier_id = $carrier->id;
		$item_send->ma_carrier_method_id = $method->id;
		$item_send->price = $price;
		$item_send->save();
	}


	protected function movePhoto($item_id)
	{
		$photos = File::read_dir(DOCROOT."/csv/item/20170314/{$item_id}");

		if(! $photos) {
			return;
		}
		if(! file_exists(DOCROOT . "/assets/img/items/{$item_id}")) {
			File::create_dir(DOCROOT . "/assets/img/items/", $item_id, 0755);
		}

		$i = 1;
		foreach($photos as $photo) {
			$item_photo = Model_Ma_Item_Photo::forge();

			$extension = pathinfo($photo, PATHINFO_EXTENSION);
			$file_name = Str::random('unique') . "." . $extension;

			File::rename(DOCROOT."/csv/item/20170314/{$item_id}/{$photo}", DOCROOT . "/assets/img/items/{$item_id}/{$file_name}");

			$item_photo->ma_item_id = $item_id;
			$item_photo->photo = $file_name;
			$item_photo->order = $i;
			if($i < 2) {
				$item_photo->is_primary = 1;
			}else{
				$item_photo->is_primary = 0;
			}
			$item_photo->is_hidden = 0;

			$item_photo->save();
			$i++;
		}
	}
}
