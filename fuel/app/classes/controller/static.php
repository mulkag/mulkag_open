<?php

class Controller_Static extends Controller_OpenTemplate
{
    public function action_about() {
        Asset::css(['pc_about.min.css'],[], 'add_css', false);
        Asset::js(['pc_about.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/about');
        $this->template->breadcrumbs = [
            [
                "name" => "MULKAGについて",
                "link" => "/static/about"
            ],
        ];
    }


    public function action_sct() {
        Asset::css(['pc_sct.min.css'],[], 'add_css', false);
        Asset::js(['pc_sct.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/sct');
        $this->template->breadcrumbs = [
            [
                "name" => "特定商取引法に基づく表示",
                "link" => "/static/sct"
            ],
        ];
    }


    public function action_rule() {
        Asset::css(['pc_rule.min.css'],[], 'add_css', false);
        Asset::js(['pc_rule.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/rule');
        $this->template->breadcrumbs = [
            [
                "name" => "利用規約",
                "link" => "/static/rule"
            ],
        ];
    }


    public function action_privacy() {
        Asset::css(['pc_privacy.min.css'],[], 'add_css', false);
        Asset::js(['pc_privacy.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/privacy');
        $this->template->breadcrumbs = [
            [
                "name" => "プライバシーポリシー",
                "link" => "/static/privacy"
            ],
        ];
    }


    public function action_sitemap() {
        Asset::css(['pc_sitemap.min.css'],[], 'add_css', false);
        Asset::js(['pc_sitemap.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('static/sitemap');
        $this->template->breadcrumbs = [
            [
                "name" => "サイトマップ",
                "link" => "/static/sitemap"
            ],
        ];
    }


}
