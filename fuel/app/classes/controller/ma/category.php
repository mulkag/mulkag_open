<?php
class Controller_Ma_Category extends Controller_Template
{

	public function action_index()
	{
		$data['categories'] = Model_Ma_Category::find('all');
		$this->template->title = "Categories";
		$this->template->content = View::forge('ma/category/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('ma/category');

		if ( ! $data['category'] = Model_Ma_Category::find($id))
		{
			Session::set_flash('error', 'Could not find category #'.$id);
			Response::redirect('ma/category');
		}

		$this->template->title = "Category";
		$this->template->content = View::forge('ma/category/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Ma_Category::validate('create');

			if ($val->run())
			{
				$category = Model_Ma_Category::forge(array(
					'name' => Input::post('name'),
					'order' => Input::post('order'),
					'parent_id' => Input::post('parent_id'),
					'level' => Input::post('level'),
					'is_hidden' => Input::post('is_hidden'),
				));

				if ($category and $category->save())
				{
					Session::set_flash('success', 'Added category #'.$category->id.'.');

					Response::redirect('ma/category');
				}

				else
				{
					Session::set_flash('error', 'Could not save category.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		View::set_global('levels', Model_Ma_Category::buildLevels(), false);
		View::set_global('categories', Model_Ma_Category::buildLevelList(), false);

		$this->template->title = "Categories";
		$this->template->content = View::forge('ma/category/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('ma/category');

		if ( ! $category = Model_Ma_Category::find($id))
		{
			Session::set_flash('error', 'Could not find category #'.$id);
			Response::redirect('ma/category');
		}

		$val = Model_Ma_Category::validate('edit');

		if ($val->run())
		{
			$category->name = Input::post('name');
			$category->order = Input::post('order');
			$category->parent_id = Input::post('parent_id');
			$category->level = Input::post('level');
			$category->is_hidden = Input::post('is_hidden');

			if ($category->save())
			{
				Session::set_flash('success', 'Updated category #' . $id);

				Response::redirect('ma/category');
			}

			else
			{
				Session::set_flash('error', 'Could not update category #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$category->name = $val->validated('name');
				$category->order = $val->validated('order');
				$category->parent_id = $val->validated('parent_id');
				$category->level = $val->validated('level');
				$category->is_hidden = $val->validated('is_hidden');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('category', $category, false);
		}

		View::set_global('levels', Model_Ma_Category::buildLevels(), false);
		View::set_global('categories', Model_Ma_Category::buildLevelList(), false);

		$this->template->title = "Categories";
		$this->template->content = View::forge('ma/category/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('ma/category');

		if ($category = Model_Ma_Category::find($id))
		{
			$category->delete();

			Session::set_flash('success', 'Deleted category #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete category #'.$id);
		}

		Response::redirect('ma/category');

	}

}
