<?php
class Controller_Ma_Carrier_Method extends Controller_Template
{

	public function action_index()
	{
		$data['ma_carrier_methods'] = Model_Ma_Carrier_Method::find('all');
		$this->template->title = "Ma_carrier_methods";
		$this->template->content = View::forge('ma/carrier/method/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('ma/carrier/method');

		if ( ! $data['ma_carrier_method'] = Model_Ma_Carrier_Method::find($id))
		{
			Session::set_flash('error', 'Could not find ma_carrier_method #'.$id);
			Response::redirect('ma/carrier/method');
		}

		$this->template->title = "Ma_carrier_method";
		$this->template->content = View::forge('ma/carrier/method/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Ma_Carrier_Method::validate('create');

			if ($val->run())
			{
				$ma_carrier_method = Model_Ma_Carrier_Method::forge(array(
					'name' => Input::post('name'),
				));

				if ($ma_carrier_method and $ma_carrier_method->save())
				{
					Session::set_flash('success', 'Added ma_carrier_method #'.$ma_carrier_method->id.'.');

					Response::redirect('ma/carrier/method');
				}

				else
				{
					Session::set_flash('error', 'Could not save ma_carrier_method.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Ma_Carrier_Methods";
		$this->template->content = View::forge('ma/carrier/method/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('ma/carrier/method');

		if ( ! $ma_carrier_method = Model_Ma_Carrier_Method::find($id))
		{
			Session::set_flash('error', 'Could not find ma_carrier_method #'.$id);
			Response::redirect('ma/carrier/method');
		}

		$val = Model_Ma_Carrier_Method::validate('edit');

		if ($val->run())
		{
			$ma_carrier_method->name = Input::post('name');

			if ($ma_carrier_method->save())
			{
				Session::set_flash('success', 'Updated ma_carrier_method #' . $id);

				Response::redirect('ma/carrier/method');
			}

			else
			{
				Session::set_flash('error', 'Could not update ma_carrier_method #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$ma_carrier_method->name = $val->validated('name');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_carrier_method', $ma_carrier_method, false);
		}

		$this->template->title = "Ma_carrier_methods";
		$this->template->content = View::forge('ma/carrier/method/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('ma/carrier/method');

		if ($ma_carrier_method = Model_Ma_Carrier_Method::find($id))
		{
			$ma_carrier_method->delete();

			Session::set_flash('success', 'Deleted ma_carrier_method #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete ma_carrier_method #'.$id);
		}

		Response::redirect('ma/carrier/method');

	}

}
