<?php
class Controller_Ma_Production extends Controller_Template
{

	public function action_index()
	{
		$data['ma_productions'] = Model_Ma_Production::find('all');
		$this->template->title = "Ma_productions";
		$this->template->content = View::forge('ma/production/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('ma/production');

		if ( ! $data['ma_production'] = Model_Ma_Production::find($id))
		{
			Session::set_flash('error', 'Could not find ma_production #'.$id);
			Response::redirect('ma/production');
		}

		$this->template->title = "Ma_production";
		$this->template->content = View::forge('ma/production/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Ma_Production::validate('create');

			if ($val->run())
			{
				$ma_production = Model_Ma_Production::forge(array(
					'name' => Input::post('name'),
				));

				if ($ma_production and $ma_production->save())
				{
					Session::set_flash('success', 'Added ma_production #'.$ma_production->id.'.');

					Response::redirect('ma/production');
				}

				else
				{
					Session::set_flash('error', 'Could not save ma_production.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Ma_Productions";
		$this->template->content = View::forge('ma/production/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('ma/production');

		if ( ! $ma_production = Model_Ma_Production::find($id))
		{
			Session::set_flash('error', 'Could not find ma_production #'.$id);
			Response::redirect('ma/production');
		}

		$val = Model_Ma_Production::validate('edit');

		if ($val->run())
		{
			$ma_production->name = Input::post('name');

			if ($ma_production->save())
			{
				Session::set_flash('success', 'Updated ma_production #' . $id);

				Response::redirect('ma/production');
			}

			else
			{
				Session::set_flash('error', 'Could not update ma_production #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$ma_production->name = $val->validated('name');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_production', $ma_production, false);
		}

		$this->template->title = "Ma_productions";
		$this->template->content = View::forge('ma/production/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('ma/production');

		if ($ma_production = Model_Ma_Production::find($id))
		{
			$ma_production->delete();

			Session::set_flash('success', 'Deleted ma_production #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete ma_production #'.$id);
		}

		Response::redirect('ma/production');

	}

}
