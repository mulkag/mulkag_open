<?php
class Controller_ma_newscategory extends Controller_Template
{

	public function action_index()
	{
		$data['ma_newscategories'] = Model_ma_newscategory::find('all');
		$this->template->title = "ma_newscategories";
		$this->template->content = View::forge('ma_newscategory/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('ma_newscategory');

		if ( ! $data['ma_newscategory'] = Model_ma_newscategory::find($id))
		{
			Session::set_flash('error', 'Could not find ma_newscategory #'.$id);
			Response::redirect('ma_newscategory');
		}

		$this->template->title = "ma_newscategory";
		$this->template->content = View::forge('ma_newscategory/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_ma_newscategory::validate('create');

			if ($val->run())
			{
				$ma_newscategory = Model_ma_newscategory::forge(array(
					'name' => Input::post('name'),
				));

				if ($ma_newscategory and $ma_newscategory->save())
				{
					Session::set_flash('success', 'Added ma_newscategory #'.$ma_newscategory->id.'.');

					Response::redirect('ma_newscategory');
				}

				else
				{
					Session::set_flash('error', 'Could not save ma_newscategory.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "ma_newscategories";
		$this->template->content = View::forge('ma_newscategory/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('ma_newscategory');

		if ( ! $ma_newscategory = Model_ma_newscategory::find($id))
		{
			Session::set_flash('error', 'Could not find ma_newscategory #'.$id);
			Response::redirect('ma_newscategory');
		}

		$val = Model_ma_newscategory::validate('edit');

		if ($val->run())
		{
			$ma_newscategory->name = Input::post('name');

			if ($ma_newscategory->save())
			{
				Session::set_flash('success', 'Updated ma_newscategory #' . $id);

				Response::redirect('ma_newscategory');
			}

			else
			{
				Session::set_flash('error', 'Could not update ma_newscategory #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$ma_newscategory->name = $val->validated('name');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_newscategory', $ma_newscategory, false);
		}

		$this->template->title = "ma_newscategories";
		$this->template->content = View::forge('ma_newscategory/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('ma_newscategory');

		if ($ma_newscategory = Model_ma_newscategory::find($id))
		{
			$ma_newscategory->delete();

			Session::set_flash('success', 'Deleted ma_newscategory #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete ma_newscategory #'.$id);
		}

		Response::redirect('ma_newscategory');

	}

}
