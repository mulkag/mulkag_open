<?php
class Controller_Ma_Carrier extends Controller_Template
{

	public function action_index()
	{
		$data['ma_carriers'] = Model_Ma_Carrier::find('all');
		$this->template->title = "Ma_carriers";
		$this->template->content = View::forge('ma/carrier/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('ma/carrier');

		if ( ! $data['ma_carrier'] = Model_Ma_Carrier::find($id))
		{
			Session::set_flash('error', 'Could not find ma_carrier #'.$id);
			Response::redirect('ma/carrier');
		}

		$this->template->title = "Ma_carrier";
		$this->template->content = View::forge('ma/carrier/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Ma_Carrier::validate('create');

			if ($val->run())
			{
				$ma_carrier = Model_Ma_Carrier::forge(array(
					'name' => Input::post('name'),
				));

				if ($ma_carrier and $ma_carrier->save())
				{
					Session::set_flash('success', 'Added ma_carrier #'.$ma_carrier->id.'.');

					Response::redirect('ma/carrier');
				}

				else
				{
					Session::set_flash('error', 'Could not save ma_carrier.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Ma_Carriers";
		$this->template->content = View::forge('ma/carrier/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('ma/carrier');

		if ( ! $ma_carrier = Model_Ma_Carrier::find($id))
		{
			Session::set_flash('error', 'Could not find ma_carrier #'.$id);
			Response::redirect('ma/carrier');
		}

		$val = Model_Ma_Carrier::validate('edit');

		if ($val->run())
		{
			$ma_carrier->name = Input::post('name');

			if ($ma_carrier->save())
			{
				Session::set_flash('success', 'Updated ma_carrier #' . $id);

				Response::redirect('ma/carrier');
			}

			else
			{
				Session::set_flash('error', 'Could not update ma_carrier #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$ma_carrier->name = $val->validated('name');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_carrier', $ma_carrier, false);
		}

		$this->template->title = "Ma_carriers";
		$this->template->content = View::forge('ma/carrier/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('ma/carrier');

		if ($ma_carrier = Model_Ma_Carrier::find($id))
		{
			$ma_carrier->delete();

			Session::set_flash('success', 'Deleted ma_carrier #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete ma_carrier #'.$id);
		}

		Response::redirect('ma/carrier');

	}

}
