<?php
class Controller_Ma_Bland extends Controller_Template
{
	const DIRBLANDLOGO = DOCROOT . 'assets/img/blands/';

	public function action_index()
	{
		$data['markers'] = Model_Ma_Marker::idKeyNameValue();

		$data['ma_blands'] = Model_Ma_Bland::find('all');

		$this->template->title = "ブランド";
		$this->template->content = View::forge('ma/bland/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('ma/bland');

		if ( ! $data['ma_bland'] = Model_Ma_Bland::find($id))
		{
			Session::set_flash('error', 'Could not find ma_bland #'.$id);
			Response::redirect('ma/bland');
		}

		$data['markers'] = Model_Ma_Marker::idKeyNameValue();
		$data['img_dir'] = self::DIRBLANDLOGO;

		$this->template->title = "ブランド";
		$this->template->content = View::forge('ma/bland/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Ma_Bland::validate('create');

			if ($val->run())
			{
				$logo = null;
				if(Input::post('logo_data')) {
					$logo = Model_MYModel::save_image(Input::post('logo_data'), self::DIRBLANDLOGO);
				}

				$ma_bland = Model_Ma_Bland::forge(array(
					'name' => Input::post('name'),
					'ma_marker_id' => Input::post('ma_marker_id'),
					'explain' => Input::post('explain'),
					'logo' => $logo
				));

				if ($ma_bland and $ma_bland->save())
				{
					Session::set_flash('success', 'Added ma_bland #'.$ma_bland->id.'.');

					Response::redirect('ma/bland');
				}

				else
				{
					Session::set_flash('error', 'Could not save ma_bland.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}
		$markers[''] = 'メーカーを選択';
		$markers += Model_Ma_Marker::idKeyNameValue();

		View::set_global('markers', $markers, false);
		$this->template->title = "ブランド";
		$this->template->content = View::forge('ma/bland/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('ma/bland');

		if ( ! $ma_bland = Model_Ma_Bland::find($id))
		{
			Session::set_flash('error', 'Could not find ma_bland #'.$id);
			Response::redirect('ma/bland');
		}

		$val = Model_Ma_Bland::validate('edit');

		if ($val->run())
		{
			$logo = null;
			if(Input::post('logo_data')) {
				$logo = Model_MYModel::save_image(Input::post('logo_data'), self::DIRBLANDLOGO);
			}

			$ma_bland->name = Input::post('name');
			$ma_bland->ma_marker_id = Input::post('ma_marker_id');
			$ma_bland->explain = Input::post('explain');
			$ma_bland->logo = $logo;

			if ($ma_bland->save())
			{
				Session::set_flash('success', 'Updated ma_bland #' . $id);

				Response::redirect('ma/bland');
			}

			else
			{
				Session::set_flash('error', 'Could not update ma_bland #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$ma_bland->name = $val->validated('name');
				$ma_bland->ma_marker_id = $val->validated('ma_marker_id');
				$ma_bland->explain = $val->validated('explain');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_bland', $ma_bland, false);
		}

		$saved_logo = '';
		if($ma_bland->logo) {
			$saved_logo = Model_MYModel::convImgBase64(self::DIRBLANDLOGO . $ma_bland->logo);
		}
		View::set_global('logo', $saved_logo, false);

		$markers[''] = 'メーカーを選択';
		$markers += Model_Ma_Marker::idKeyNameValue();
		View::set_global('markers', $markers, false);

		$this->template->title = "ブランド";
		$this->template->content = View::forge('ma/bland/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('ma/bland');

		if ($ma_bland = Model_Ma_Bland::find($id))
		{
			File::delete(self::DIRBLANDLOGO.$ma_bland->logo);
			$ma_bland->delete();

			Session::set_flash('success', 'Deleted ma_bland #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete ma_bland #'.$id);
		}

		Response::redirect('ma/bland');

	}

}
