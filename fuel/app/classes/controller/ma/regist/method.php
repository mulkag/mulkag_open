<?php
class Controller_Ma_Regist_Method extends Controller_Template
{

	public function action_index()
	{
		$data['ma_regist_methods'] = Model_Ma_Regist_Method::find('all');
		$this->template->title = "Ma_regist_methods";
		$this->template->content = View::forge('ma/regist/method/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('ma/regist/method');

		if ( ! $data['ma_regist_method'] = Model_Ma_Regist_Method::find($id))
		{
			Session::set_flash('error', 'Could not find ma_regist_method #'.$id);
			Response::redirect('ma/regist/method');
		}

		$this->template->title = "Ma_regist_method";
		$this->template->content = View::forge('ma/regist/method/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Ma_Regist_Method::validate('create');

			if ($val->run())
			{
				$ma_regist_method = Model_Ma_Regist_Method::forge(array(
					'name' => Input::post('name'),
				));

				if ($ma_regist_method and $ma_regist_method->save())
				{
					Session::set_flash('success', 'Added ma_regist_method #'.$ma_regist_method->id.'.');

					Response::redirect('ma/regist/method');
				}

				else
				{
					Session::set_flash('error', 'Could not save ma_regist_method.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Ma_Regist_Methods";
		$this->template->content = View::forge('ma/regist/method/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('ma/regist/method');

		if ( ! $ma_regist_method = Model_Ma_Regist_Method::find($id))
		{
			Session::set_flash('error', 'Could not find ma_regist_method #'.$id);
			Response::redirect('ma/regist/method');
		}

		$val = Model_Ma_Regist_Method::validate('edit');

		if ($val->run())
		{
			$ma_regist_method->name = Input::post('name');

			if ($ma_regist_method->save())
			{
				Session::set_flash('success', 'Updated ma_regist_method #' . $id);

				Response::redirect('ma/regist/method');
			}

			else
			{
				Session::set_flash('error', 'Could not update ma_regist_method #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$ma_regist_method->name = $val->validated('name');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('ma_regist_method', $ma_regist_method, false);
		}

		$this->template->title = "Ma_regist_methods";
		$this->template->content = View::forge('ma/regist/method/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('ma/regist/method');

		if ($ma_regist_method = Model_Ma_Regist_Method::find($id))
		{
			$ma_regist_method->delete();

			Session::set_flash('success', 'Deleted ma_regist_method #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete ma_regist_method #'.$id);
		}

		Response::redirect('ma/regist/method');

	}

}
