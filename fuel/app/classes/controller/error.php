<?php

class Controller_Error extends Controller_OpenTemplate
{
    public function action_index() {
        Asset::css(['pc_error.min.css'],[], 'add_css', false);
        Asset::js(['pc_error.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('error/index');
        $this->template->breadcrumbs = [
            [
                "name" => "404",
                "link" => "/404"
            ],
        ];
    }

}
