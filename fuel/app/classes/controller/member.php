<?php

class Controller_Member extends Controller_OpenTemplate
{
    public function action_index() {
        Asset::css(['pc_member.min.css'],[], 'add_css', false);
        Asset::js(['pc_member.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('member/index');
    }


    public function action_Chat() {
        Asset::css(['pc_member.min.css'],[], 'add_css', false);
        Asset::js(['pc_member.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('member/chat');
    }


    public function action_History() {
        Asset::css(['pc_member.min.css'],[], 'add_css', false);
        Asset::js(['pc_member.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('member/history');
    }


    public function action_Info() {
        Asset::css(['pc_member.min.css'],[], 'add_css', false);
        Asset::js(['pc_member.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('member/info');
    }


    public function action_Status() {
        Asset::css(['pc_member.min.css'],[], 'add_css', false);
        Asset::js(['pc_member.min.js'],[], 'add_js', false);

        $this->template->content = \View_Twig::forge('member/status');
    }

}
