<?php
class MyValidation
{
    public static function _validation_unique($val, $options)
    {
        list($table, $field) = explode('.', $options);
        $id = Validation::active()->input('id');

        $result = DB::select(DB::expr("LOWER (\"$field\")"))
            ->where($field, '=', Str::lower($val))
            ->where('id', '!=', $id)
            ->from($table)->execute();

        Validation::active()->set_message('unique', '「:value」は既に登録されています。');

        return ! ($result->count() > 0);
    }
}
