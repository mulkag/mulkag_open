<?php

/**
 * The welcome 404 presenter.
 *
 * @package  app
 * @extends  Presenter
 */
class Presenter_Top_Index extends Presenter
{
	protected $_view = 'top/index.twig';

	public function view()
	{
		$this->newses = Model_News::query()->order_by('created_at', 'desc')->limit(5)->get();
		$this->blands = Model_Ma_Bland::query()->order_by('order', 'ASC')->get();

        $items = [];
        for($i = 1; $i <= 6; $i++){
            $items += Model_Ma_Category::query()
                ->where('id', $i)
                ->related(['items' => [
                    'order_by' => ['created_at' => 'DESC'],
//                    'related' => 'photos'
                    ]
                ])
                ->rows_limit(7)
                ->get();
        }
        $this->items = $items;
	}
}
