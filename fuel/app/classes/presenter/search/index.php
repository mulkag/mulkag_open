<?php

/**
 * The welcome 404 presenter.
 *
 * @package  app
 * @extends  Presenter
 */
class Presenter_Search_Index extends Presenter
{
    public function view()
    {
        $this->selectedCategory = null;
        $this->selectedBland = null;

        if (Uri::segment(1) == "category") {
            $this->selectedCategory = Uri::segment(2);
        }
        if (Uri::segment(1) == "bland") {
            $this->selectedBland = Uri::segment(2);
        }
//        $this->categories = Arr::assoc_to_keyval(Model_Ma_Category::query()->where('level', 1)->order_by('order')->get(), 'id', 'name');
        // $this->categories = Model_Ma_Category::query()->where('level', 1)->order_by('order')->get();
        $this->categories = DB::select('id', 'name', 'name_en')
            ->from('ma_categories')
            ->where('level', 1)
            ->order_by('order')
            ->join('item_category')
            ->on('ma_categories.id', '=', 'ma_category_id')
            ->group_by('name')
            ->execute();

//        $this->blands = Arr::assoc_to_keyval(Model_Ma_Bland::query()->order_by('order')->get(), 'id', 'name');
        $this->blands = Model_Ma_Bland::query()->order_by('order')->get();

        $this->materials = Arr::assoc_to_keyval(Model_Ma_Material::query()->get(), 'id', 'name');

        $this->colors = Arr::assoc_to_keyval(Model_Ma_Color::query()->get(), 'id', 'color_code');
    }
}
