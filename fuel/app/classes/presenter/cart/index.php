<?php

class Presenter_Cart_Index extends Presenter
{
    protected $_view = 'cart/index.twig';

    public function view()
    {
        if(! Cookie::get('cart')) {
            $this->items = '';
            return;
        }

        foreach(Cookie::get('cart') as $id => $item_infos) {
            $item[$id]['detaile'] = Model_Ma_Item::find($id, ['related' => ['bland']]);
            $item[$id]["photo"] = Model_Ma_Item_Photo::query()->select("photo")->where("ma_item_id", $item[$id]['detaile']->id)->get_one()->photo;

            $item[$id]['color'] = Model_Ma_color::find($item_infos['color']);
            $item[$id]['number'] = $item_infos['number'];
            $item[$id]['send'] = Model_Item_Send::query()->related('carrier_method')->where('id', $item_infos['send'])->get_one();

            $this->items = $item;
        }

    }
}
