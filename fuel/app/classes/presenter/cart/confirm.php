<?php

class Presenter_Cart_Confirm extends Presenter
{
    protected $_view = 'cart/confirm.twig';

    public function view()
    {
        $posts = Input::post();

        $total_number = 0;
        $total_send_price = 0;
        $total_price = 0;

        $this->orderer = [
            "name" => $posts["name"],
            'kana' => $posts['kana'],
            'zip' => $posts['zip'],
            'pref' => $posts['pref'],
            'city' => $posts['city'],
            'address-etc' => $posts['address-etc'],
            'tel' => $posts['tel'],
            'mail' => $posts['mail'],
            'payment' => $posts['payment'],
        ];

        foreach($posts["item_id"] as $key => $item_id) {
            $item[$item_id]['detaile'] = Model_Ma_Item::find($item_id, ['related' => ['bland']]);
            $item[$item_id]["photo"] = Model_Ma_Item_Photo::query()->select("photo")->where("ma_item_id", $item_id)->get_one()->photo;

            $item[$item_id]["price"] = $posts["price"][$key];
            $item[$item_id]["send_price"] = $posts["send_price"][$key];

            $item[$item_id]['color'] = Model_Ma_color::find($posts["color"][$key]);
            $item[$item_id]['number'] = $posts['number'][$key];
            $item[$item_id]['send'] = Model_Item_Send::query()->related('carrier_method')->where('id', $posts["send_id"][$key])->get_one();

            $total_number += $posts['number'][$key];
            $total_send_price += $posts['send_price'][$key];
            $total_price += $posts['price'][$key];
        }

        $this->items = $item;
        $this->total_number = $total_number;
        $this->total_send_price = $total_send_price;
        $this->total_price = $total_price;
        $this->posts = $posts;
    }
}
