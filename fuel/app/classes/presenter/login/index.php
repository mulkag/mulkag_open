<?php

class Presenter_Login_Index extends Presenter
{
    protected $_view = 'login/index.twig';

    public function view()
    {
        $this->errors = $this->data['errors'];
    }
}
