<?php

class Presenter_Detail_Index extends Presenter
{
    protected $_view = 'detail/index.twig';
    protected $self_view = null;

    public function view()
    {
        $this->item = Model_Ma_Item::find($this->data['item_id'], [
            'related' => [
                'photos',
                'colors',
                'production',
                'materials',
                'sends' => [
                    'related' => [
                        'carrier',
                        'carrier_method'
                    ]
                ],
                'bland' => [
                    'related' => 'marker'
                ],
            ]
        ]);

        $this->self_view = $this->get_view();
        $this->self_view->set_global([
            'title' => 'aaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'self_url' => \Uri::main()
        ]);
        $this->title = "aaaaaaaaaaaaaaaaaaa";
    }


}
