<?php

class Model_Item_Send extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'ma_item_id',
		'ma_carrier_id',
		'ma_carrier_method_id',
		'price',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
		'carrier' => array(
			'key_from' => 'ma_carrier_id',
			'model_to' => 'Model_Ma_Carrier',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		),
		'carrier_method' => array(
			'key_from' => 'ma_carrier_method_id',
			'model_to' => 'Model_Ma_Carrier_Method',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => false,
		),
	);

	protected static $_table_name = 'item_sends';

}
