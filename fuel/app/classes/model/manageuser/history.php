<?php

class Model_Manageuser_History extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'manageuser_id',
		'ipaddress',
		'user_agent',
		'ma_user_history_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'manageuser_histories';


	public static function add_data($user_id, $method)
	{
		$history = Model_Ma_User_History::query()->where('name', $method)->get_one();

        $manageuser_history = self::forge();
        $manageuser_history->manageuser_id = $user_id;
        $manageuser_history->ipaddress = ip2long(Input::ip());
        $manageuser_history->user_agent = Input::user_agent();
        $manageuser_history->ma_user_history_id = $history->id;
        $manageuser_history->save();
	}
}
