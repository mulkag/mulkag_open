<?php
use Orm\Model;

class Model_ManageUser extends Model
{
	protected static $_properties = array(
		'id',
		'name',
		'mail',
		'password',
		'is_admin' => ['default' => 0],
		'ma_marker_id',
		'last_login',
		'login_hush',
		'photo',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[32]|trim');
		$val->add_field('mail', 'Mail', 'required|max_length[255]|valid_email|trim');
		$val->add_field('password', 'Password', 'required|max_length[128]');
		$val->add_field('is_admin', 'Is Admin', 'valid_string[numeric]');
		$val->add_field('ma_marker_id', 'Ma Marker Id', 'valid_string[numeric]');
		$val->add_field('photo', 'Photo', 'max_length[64]');

		return $val;
	}

	protected static $_table_name = 'manageusers';

}
