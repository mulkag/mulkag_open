<?php

class Model_Order extends \Orm\Model
{

    protected static $_properties = array(
        'id',
        'name',
        'kana',
        'zip',
        'pref',
        'city',
        'address-etc',
        'tel',
        'mail',
        'delivery-date',
        'method-mayment',
        'ma_order_status_id',
        "settlementurl",
        'created_at',
        'updated_at',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'orders';


    protected static $_has_many = array(
        'order_items' => array(
            'key_from' => 'id',
            'model_to' => 'Model_Order_Item',
            'key_to' => 'order_id',
            'cascade_save' => true,
            'cascade_delete' => true,
        )
    );

    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        $val->add_field('name', 'お名前', 'required|max_length[50]');
        $val->add_field('kana', 'ふりがな', 'required|max_length[50]');
        $val->add_field('zip', '郵便番号', 'required');
        $val->add_field('pref', '都道府県', 'required');
        $val->add_field('city', '市区町村', 'required');
        $val->add_field('address-etc', '市区町村以下', 'required');
        $val->add_field('mail', 'メールアドレス', 'required|valid_email');
        $val->add_field('payment', 'お支払い方法', 'required');

        return $val;
    }


    public static function addOrder()
    {
        $query = DB::insert('orders');
        $query->set([
            'name' => Input::post("name"),
            'kana' => Input::post("kana"),
            'mail' => Input::post("mail"),
            'pref' => Input::post('pref'),
            'city' => Input::post('city'),
            'address-etc' => Input::post('address-etc'),
            'tel' => Input::post('tel'),
            'mail' => Input::post('mail'),
            'ma_payment_id' => Input::post('payment'),
            "ma_order_status_id" => 1,
            'created_at' => time()
        ]);
        $result = $query->execute();
        $order_id = $result[0];

        $order_items = \DB::insert(
            'order_items'
        )->columns(array(
            'order_id',
            'ma_item_id',
            'number',
            'ma_send_id',
            'ma_color_id',
            'price',
            'send',
            'created_at',
        ));

        $total_send_price = 0;
        $total_price = 0;

        foreach (Input::post('item_id') as $key => $item_id) {
            $total_send_price += Input::post('send_price')[$key];
            $total_price += Input::post('price')[$key];

            $values[] = $order_id;
            $values[] = $item_id;
            $values[] = Input::post('number')[$key];
            $values[] = Input::post('send_id')[$key];
            $values[] = Input::post('color')[$key];
            $values[] = Input::post('price')[$key];
            $values[] = Input::post('send_price')[$key];
            $values[] = time();

            $order_items->values($values);

            $values = [];
        }
        $order_items->execute();

        return [$order_id, $total_price, $total_send_price];
    }
}
