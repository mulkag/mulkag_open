<?php

class Model_Order_Item extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'order_id',
		'ma_item_id',
		'number',
		'ma_send_id',
		'ma_color_id',
        'price',
        'send',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'order_items';

}
