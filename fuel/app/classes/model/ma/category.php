<?php
use Orm\Model;

class Model_Ma_Category extends Model
{
	protected static $_properties = array(
		'id',
		'name',
        'name_en',
        'order',
		'parent_id',
		'level',
		'is_hidden' => ['default' => 0],
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_many_many = array(
		'items' => array(
			'key_from' => 'id',
			'key_through_from' => 'ma_category_id',
			'table_through' => 'item_category',
			'key_through_to' => 'ma_item_id',
			'model_to' => 'Model_Ma_Item',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => true,
		)
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[50]');
		$val->add_field('order', 'Order', 'valid_string[numeric]');
		$val->add_field('parent_id', 'Parent Id', 'valid_string[numeric]');
		$val->add_field('level', 'Level', 'required|valid_string[numeric]');
		// $val->add_field('is_hidden', 'Is Hidden', 'required');

		return $val;
	}


	/**
	 * @return mixed
	 */
	public static function buildLevels()
	{
		$levels = [];

		$max_level = self::query()->max('level');
		// if(! $max_level) {
		// 	$max_level = 1;
		// }

		for($i = 1; $i <= $max_level + 1; $i++) {
			$levels[$i] = $i;
		}

		return $levels;
	}


	/**
	 * @return array|null
	 */
	public static function buildLevelList()
	{
		$rows = self::query()->get();

		if (!$rows) {
			return null;
		}

		$data = [];
		foreach ($rows as $row) {
			$data[$row->level][] = $row;
		}
		return $data;
	}
}
