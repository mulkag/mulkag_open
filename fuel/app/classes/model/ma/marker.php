<?php
use Orm\Model;

class Model_Ma_Marker extends Model_MYModel
{
	protected static $_properties = array(
		'id',
		'name',
		'zip',
		'pref',
		'address',
		'tel',
		'fax',
		'mail',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_has_many = array(
		'sends' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Ma_Marker_Send',
			'key_to' => 'ma_marker_id',
			'cascade_save' => true,
			'cascade_delete' => true,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[100]');
		$val->add_field('zip', 'Zip', 'max_length[8]');
		$val->add_field('pref', 'Pref', 'max_length[4]');
		$val->add_field('address', 'Address', 'max_length[50]');
		$val->add_field('tel', 'Tel', 'max_length[20]');
		$val->add_field('fax', 'Fax', 'max_length[20]');
		$val->add_field('mail', 'Mail', 'max_length[255]');

		return $val;
	}

}
