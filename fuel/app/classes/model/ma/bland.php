<?php
use Orm\Model;

class Model_Ma_Bland extends Model_MYModel
{
	protected static $_properties = array(
		'id',
		'name',
        'name_en',
		'ma_marker_id',
		'explain',
        'logo',
        'order',
        'location',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
		'marker' => array(
			'key_from' => 'ma_marker_id',
			'model_to' => 'Model_Ma_Marker',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[50]');
		$val->add_field('ma_marker_id', 'Ma Marker Id', 'required|valid_string[numeric]');
		$val->add_field('order', 'Order', 'valid_string[numeric]');
		$val->add_field('explain', 'Explain', 'required');

		return $val;
	}

}
