<?php
use Orm\Model;

class Model_Ma_Item extends Model_MYModel
{
	protected static $_properties = array(
		'id',
		'name',
		'original_name',
		'ma_bland_id',
		'explain',
		'cost_price',
		'selling_price',
		'jan_code',
		'is_hidden' => ['default' => 0],
		'is_members',
		// 'ma_color_id',
		'multina_price',
		// 'packing_size',
		'ma_production_id',
		'weight',
		'size_w',
		'size_d',
		'size_h',
		'size_sh',
		'material_detail',
		'note',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
		'bland' => array(
			'key_from' => 'ma_bland_id',
			'model_to' => 'Model_Ma_Bland',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
		),
		'production' => array(
			'key_from' => 'ma_production_id',
			'model_to' => 'Model_Ma_Production',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
		),
	);

	protected static $_has_many = array(
		'photos' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Ma_Item_Photo',
			'key_to' => 'ma_item_id',
			'cascade_save' => true,
			'cascade_delete' => true,
		),
		'details' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Ma_Item_Detail',
			'key_to' => 'ma_item_id',
			'cascade_save' => true,
			'cascade_delete' => true,
		),
		'sends' => array(
			'key_from' => 'id',
			'model_to' => 'Model_Item_Send',
			'key_to' => 'ma_item_id',
			'cascade_save' => true,
			'cascade_delete' => true,
		),
	);

	protected static $_many_many = array(
		'categories' => array(
			'key_from' => 'id',
			'key_through_from' => 'ma_item_id',
			'table_through' => 'item_category',
			'key_through_to' => 'ma_category_id',
			'model_to' => 'Model_Ma_Category',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => true,
		),
		'colors' => array(
			'key_from' => 'id',
			'key_through_from' => 'ma_item_id',
			'table_through' => 'item_color',
			'key_through_to' => 'ma_color_id',
			'model_to' => 'Model_Ma_Color',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => true,
		),
		'materials' => array(
			'key_from' => 'id',
			'key_through_from' => 'ma_item_id',
			'table_through' => 'item_material',
			'key_through_to' => 'ma_material_id',
			'model_to' => 'Model_Ma_Material',
			'key_to' => 'id',
			'cascade_save' => true,
			'cascade_delete' => true,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[100]');
		$val->add_field('ma_bland_id', 'Ma Bland Id', 'valid_string[numeric]');
		$val->add_field('explain', 'Explain', 'required');
		$val->add_field('cost_price', 'Cost Price', 'valid_string[numeric]');
		$val->add_field('jan_code', 'Jan Code', 'max_length[64]');
		$val->add_field('ma_color_id', 'Color Code', 'max_length[16]');
		$val->add_field('multina_price', 'Multina Price', 'valid_string[numeric]');
		$val->add_field('packing_size', 'Packing Size', 'valid_string[numeric]');

		return $val;
	}

}
