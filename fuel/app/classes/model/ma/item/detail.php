<?php

class Model_Ma_Item_Detail extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'ma_item_id',
		'ma_material_id',
		'cost_price',
		'multina_price',
		'packing_size',
		'is_hidden' => ['default' => 0],
		'is_members' => ['default' => 0],
		'ma_color_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'ma_item_details';

}
