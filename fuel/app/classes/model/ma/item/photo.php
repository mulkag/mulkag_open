<?php

class Model_Ma_Item_Photo extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'ma_item_id',
		'photo',
		'order',
		'is_primary',
		'is_hidden',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'ma_item_photos';

}
