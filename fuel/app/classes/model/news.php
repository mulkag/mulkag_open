<?php
use Orm\Model;

class Model_News extends Model
{
	protected static $_properties = array(
		'id',
		'title',
		'url',
		'content',
		'ma_newscategory_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'Title', 'required|max_length[100]');
		$val->add_field('url', 'Url', 'max_length[255]');
		$val->add_field('content', 'Content', 'required');
		$val->add_field('ma_newscategory_id', 'Ma Newscategory Id', 'required|valid_string[numeric]');

		return $val;
	}

}
