<?php

class Model_Contact extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'category',
		'name',
		'mail',
		'content',
        'tracking_number',
        'order_number',
        'photo',
        'notes',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'contacts';


	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'お名前', 'required|max_length[50]');
		// $val->add_field('category', '問合せ種別', 'required|valid_string[numeric]');
		$val->add_field('mail', 'メールアドレス', 'required|valid_email');
		$val->add_field('tracking_number', 'お問い合わせ番号', 'valid_string[numeric]');
		//$val->add_field('order_number', 'ご注文番号', 'valid_string[numeric]');

		return $val;
	}



	public static function buildMailBody($data)
	{
		$body = <<<EOF
この度は、お問合せいただきありがとうございます。
下記内容でお問合せを受け付けました。

また、ご返信にはお時間がかかる場合がございます。
あらかじめご了承くださいませ。

【お問い合わせ内容】
名前：{$data["name"]}様
メールアドレス：{$data["mail"]}
お問い合わせ番号：{$data["tracking_number"]}
お問い合わせ内容：{$data["content"]}

≪注意事項≫
※このメールに心当たりのない場合や、ご不明な点がございましたら、info@mulkag.comまでご連絡ください。
※入金確認や発注など、業務に関わる処理は営業時間内での対応となりますので、あらかじめご了承ください。

──────────────────────────────────

MULKAG
こだわりを厳選した家具・インテリア通販サイト
http://mulkag.com

──────────────────────────────────
EOF;

		return $body;
	}
}
