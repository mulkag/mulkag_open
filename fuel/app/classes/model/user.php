<?php
use Orm\Model;

class Model_User extends Model
{
    protected static $_properties = array(
        'id',
        'name',
        'mail',
        'password',
        'sex',
        'zip',
        'pref',
        'address',
        'tel',
        'ma_regist_method_id',
        'photo',
        'hush',
        'last_login',
        'kana',
        'created_at',
        'updated_at',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_save'),
            'mysql_timestamp' => false,
        ),
    );

    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        $val->add_callable('MyValidation');
        $val->add_field('name', 'お名前', 'max_length[32]|trim');
        $val->add_field('mail', 'メールアドレス', 'required|max_length[255]|valid_email|trim|unique[users.mail]');
        $val->add_field('password', 'パスワード', 'required|max_length[128]');
        $val->add_field('sex', '性別', 'valid_string[numeric]');
        $val->add_field('zip', '郵便番号', 'valid_string[numeric]');
        $val->add_field('pref', '都道府県', 'max_length[4]');
        $val->add_field('kana', 'ふりがな', 'max_length[32]|trim');

        return $val;
    }

    protected static $_table_name = 'users';

}
