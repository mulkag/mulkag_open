<?php
use Orm\Model;

class Model_MYModel extends Model
{
    public static function idKeyNameValue()
    {
        $rows = self::find('all');
        return Arr::assoc_to_keyval($rows, 'id', 'name');
    }


    /**
     * @param $fullPath
     * @return string
     */
    public static function convImgBase64($fullPath)
    {
        $extension = substr($fullPath, strpos($fullPath, '.') + 1);
        return "data:image/{$extension};base64," . base64_encode(file_get_contents($fullPath));
    }


    public static function save_image($base64, $path)
    {
        preg_match("/data:image\/([\w]+);/i", $base64, $match);
        $saved_name = \Str::random('unique') . '.' . mb_strtolower($match[1]);
        $saved_fullpath = $path . $saved_name;

        $img = preg_replace("/data:([^,]+),/i", "", $base64);

        if(touch($saved_fullpath)) {
            $fp = fopen($saved_fullpath, 'wb');
            fwrite($fp, base64_decode($img));
            fclose($fp);
        }

        return $saved_name;
    }


    public static function convToNull($value)
    {
        if(!$value) {
            return null;
        }
        return $value;
    }

}
