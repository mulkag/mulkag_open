<?php

namespace Fuel\Migrations;

class Create_manageuser_histories
{
	public function up()
	{
		\DBUtil::create_table('manageuser_histories', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'manageuser_id' => array('constraint' => 11, 'type' => 'int'),
			'ipaddress' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('manageuser_histories');
	}
}