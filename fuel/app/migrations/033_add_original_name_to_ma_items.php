<?php

namespace Fuel\Migrations;

class Add_original_name_to_ma_items
{
	public function up()
	{
		\DBUtil::add_fields('ma_items', array(
			'original_name' => array('constraint' => 64, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('ma_items', array(
			'original_name'

		));
	}
}