<?php

namespace Fuel\Migrations;

class Create_ma_markers
{
	public function up()
	{
		\DBUtil::create_table('ma_markers', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 100, 'type' => 'varchar'),
			'zip' => array('constraint' => 8, 'type' => 'varchar', 'null' => true),
			'pref' => array('constraint' => 4, 'type' => 'varchar', 'null' => true),
			'address' => array('constraint' => 50, 'type' => 'varchar', 'null' => true),
			'tel' => array('constraint' => 20, 'type' => 'varchar', 'null' => true),
			'fax' => array('constraint' => 20, 'type' => 'varchar', 'null' => true),
			'mail' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('ma_markers');
	}
}