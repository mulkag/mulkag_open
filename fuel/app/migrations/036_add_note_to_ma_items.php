<?php

namespace Fuel\Migrations;

class Add_note_to_ma_items
{
	public function up()
	{
		\DBUtil::add_fields('ma_items', array(
			'note' => array('type' => 'text'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('ma_items', array(
			'note'

		));
	}
}