<?php

namespace Fuel\Migrations;

class Add_name_en_to_ma_blands
{
	public function up()
	{
		\DBUtil::add_fields('ma_blands', array(
			'name_en' => array('constraint' => 16, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('ma_blands', array(
			'name_en'

		));
	}
}