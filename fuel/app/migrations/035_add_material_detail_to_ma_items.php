<?php

namespace Fuel\Migrations;

class Add_material_detail_to_ma_items
{
	public function up()
	{
		\DBUtil::add_fields('ma_items', array(
			'material_detail' => array('type' => 'text'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('ma_items', array(
			'material_detail'

		));
	}
}