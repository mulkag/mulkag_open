<?php

namespace Fuel\Migrations;

class Add_tracking_number_to_contacts
{
	public function up()
	{
		\DBUtil::add_fields('contacts', array(
			'tracking_number' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'order_number' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'photo' => array('constraint' => 64, 'type' => 'varchar', 'null' => true),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('contacts', array(
			'tracking_number'
,			'order_number'
,			'photo'

		));
	}
}