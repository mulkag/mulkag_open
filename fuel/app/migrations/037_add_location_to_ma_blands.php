<?php

namespace Fuel\Migrations;

class Add_location_to_ma_blands
{
	public function up()
	{
		\DBUtil::add_fields('ma_blands', array(
			'location' => array('constraint' => 16, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('ma_blands', array(
			'location'

		));
	}
}