<?php

namespace Fuel\Migrations;

class Add_notes_to_contacts
{
	public function up()
	{
		\DBUtil::add_fields('contacts', array(
			'notes' => array('type' => 'text', 'null' => true),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('contacts', array(
			'notes'

		));
	}
}