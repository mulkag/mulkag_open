<?php

namespace Fuel\Migrations;

class Create_ma_item_details
{
	public function up()
	{
		\DBUtil::create_table('ma_item_details', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'ma_item_id' => array('constraint' => 11, 'type' => 'int'),
			'ma_material_id' => array('constraint' => 11, 'type' => 'int'),
			'cost_price' => array('constraint' => 11, 'type' => 'int'),
			'multina_price' => array('constraint' => 11, 'type' => 'int'),
			'packing_size' => array('constraint' => 11, 'type' => 'int'),
			'is_hidden' => array('type' => 'boolean'),
			'is_members' => array('type' => 'boolean'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('ma_item_details');
	}
}