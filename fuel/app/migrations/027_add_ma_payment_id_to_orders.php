<?php

namespace Fuel\Migrations;

class Add_ma_payment_id_to_orders
{
	public function up()
	{
		\DBUtil::add_fields('orders', array(
			'ma_payment_id' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('orders', array(
			'ma_payment_id'

		));
	}
}