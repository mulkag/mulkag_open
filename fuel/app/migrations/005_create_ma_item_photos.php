<?php

namespace Fuel\Migrations;

class Create_ma_item_photos
{
	public function up()
	{
		\DBUtil::create_table('ma_item_photos', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'ma_item_id' => array('constraint' => 11, 'type' => 'int'),
			'photo' => array('constraint' => 64, 'type' => 'varchar'),
			'order' => array('constraint' => 11, 'type' => 'int'),
			'is_primary' => array('type' => 'boolean', 'default' => '0'),
			'is_hidden' => array('type' => 'boolean', 'default' => '0'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('ma_item_photos');
	}
}