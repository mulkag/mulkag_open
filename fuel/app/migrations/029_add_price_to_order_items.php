<?php

namespace Fuel\Migrations;

class Add_price_to_order_items
{
	public function up()
	{
		\DBUtil::add_fields('order_items', array(
			'price' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('order_items', array(
			'price'

		));
	}
}