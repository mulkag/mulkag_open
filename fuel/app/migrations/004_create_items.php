<?php

namespace Fuel\Migrations;

class Create_items
{
	public function up()
	{
		\DBUtil::create_table('items', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 100, 'type' => 'varchar'),
			'ma_bland_id' => array('constraint' => 11, 'type' => 'int'),
			'explain' => array('type' => 'text', 'null' => true),
			'cost_price' => array('constraint' => 11, 'type' => 'int'),
			'jan_code' => array('constraint' => 64, 'type' => 'varchar', 'null' => true),
			'is_hidden' => array('type' => 'boolean', 'default' => '0'),
			'is_members' => array('type' => 'boolean', 'default' => '0'),
			'ma_color_id' => array('constraint' => 16, 'type' => 'varchar', 'null' => true),
			'multina_price' => array('constraint' => 11, 'type' => 'int'),
			'packing_size' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('items');
	}
}