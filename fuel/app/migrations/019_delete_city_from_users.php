<?php

namespace Fuel\Migrations;

class Delete_city_from_users
{
	public function up()
	{
		\DBUtil::drop_fields('users', array(
			'city'

		));
	}

	public function down()
	{
		\DBUtil::add_fields('users', array(
			'city' => array('constraint' => 32, 'type' => 'varchar'),

		));
	}
}