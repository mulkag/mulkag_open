<?php

namespace Fuel\Migrations;

class Create_categories
{
	public function up()
	{
		\DBUtil::create_table('categories', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 50, 'type' => 'varchar'),
			'order' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'parent_id' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'level' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'is_hidden' => array('type' => 'boolean', 'default' => '0'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('categories');
	}
}