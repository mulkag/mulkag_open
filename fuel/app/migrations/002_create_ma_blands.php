<?php

namespace Fuel\Migrations;

class Create_ma_blands
{
	public function up()
	{
		\DBUtil::create_table('ma_blands', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 50, 'type' => 'varchar'),
			'ma_marker_id' => array('constraint' => 11, 'type' => 'int'),
			'explain' => array('type' => 'text', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('ma_blands');
	}
}