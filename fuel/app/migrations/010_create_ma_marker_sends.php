<?php

namespace Fuel\Migrations;

class Create_ma_marker_sends
{
	public function up()
	{
		\DBUtil::create_table('ma_marker_sends', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'ma_marker_id' => array('constraint' => 11, 'type' => 'int'),
			'ma_carrier_id' => array('constraint' => 11, 'type' => 'int'),
			'ma_carrier_method_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('ma_marker_sends');
	}
}