<?php

namespace Fuel\Migrations;

class Create_order_items
{
	public function up()
	{
		\DBUtil::create_table('order_items', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'order_id' => array('constraint' => 11, 'type' => 'int'),
			'ma_item_id' => array('constraint' => 11, 'type' => 'int'),
			'number' => array('constraint' => 11, 'type' => 'int'),
			'ma_send_id' => array('constraint' => 11, 'type' => 'int'),
			'ma_color_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('order_items');
	}
}