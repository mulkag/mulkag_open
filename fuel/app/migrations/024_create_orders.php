<?php

namespace Fuel\Migrations;

class Create_orders
{
	public function up()
	{
		\DBUtil::create_table('orders', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 64, 'type' => 'varchar'),
			'kana' => array('constraint' => 64, 'type' => 'varchar'),
			'zip' => array('constraint' => 7, 'type' => 'varchar'),
			'pref' => array('constraint' => 5, 'type' => 'varchar'),
			'city' => array('constraint' => 16, 'type' => 'varchar'),
			'address-etc' => array('constraint' => 64, 'type' => 'varchar'),
			'tel' => array('constraint' => 20, 'type' => 'varchar'),
			'mail' => array('constraint' => 255, 'type' => 'varchar'),
			'delivery-date' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'method-mayment' => array('constraint' => 16, 'type' => 'varchar'),
			'ma_order_status_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('orders');
	}
}