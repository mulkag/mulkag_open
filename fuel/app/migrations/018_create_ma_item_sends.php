<?php

namespace Fuel\Migrations;

class Create_ma_item_sends
{
	public function up()
	{
		\DBUtil::create_table('ma_item_sends', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'ma_item_id' => array('constraint' => 11, 'type' => 'int'),
			'ma_marker_send' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('ma_item_sends');
	}
}