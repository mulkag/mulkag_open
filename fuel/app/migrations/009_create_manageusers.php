<?php

namespace Fuel\Migrations;

class Create_manageusers
{
	public function up()
	{
		\DBUtil::create_table('manageusers', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 32, 'type' => 'varchar'),
			'mail' => array('constraint' => 255, 'type' => 'varchar'),
			'password' => array('constraint' => 64, 'type' => 'varchar'),
			'is_admin' => array('type' => 'boolean', 'default' => '0'),
			'ma_marker_id' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'last_login' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'login_hush' => array('constraint' => 32, 'type' => 'varchar', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('manageusers');
	}
}