<?php

namespace Fuel\Migrations;

class Add_selling_price_to_ma_items
{
	public function up()
	{
		\DBUtil::add_fields('ma_items', array(
			'selling_price' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('ma_items', array(
			'selling_price'

		));
	}
}