<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow">
    <title>MULKAG マルカグ | ○○○</title>
    <meta name="description" content="120文字">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="MULKAG | マルカグ">
    <meta property="og:title" content="○○○">
    <meta property="og:description" content="記事の概要">
    <meta property="og:url" content="https://mulkag.com">
    <meta property="og:image" content="">
    <link rel="canonical" href="https://mulkag.com/collection/detail/160901/">
    <link rel="icon" href="/assets/img/common/favicon.ico">
    <link rel="apple-touch-icon" href="/mulkag/icon/icon.png">
<?php echo Asset::css('collection/160901/style.css'); ?>
</head>
<body>
    <main>
        <article>
            <section class="contentsWrapper">
                <div class="collectionInfo">
                    <h1 class="collectionInfo__title"><img src="/assets/img/pg_collection/160901/collectionInfo_title.svg" alt="HALO"></h1>
                    <p class="collectionInfo__description">1976年に誕生したHALOは、伝統と革新をベースにDaring-大胆、Authentic-本物、オリジナルを忠実に再現する事を追求した独自のもの作りと世界観を持っています。オールハンドメイドで伝統的な職人の技を忠実に再現、素材や仕上げにこだわり、アンティークの持つユーズド感を徹底的に追及し、レザーソファ、鋲うちトランク、木製の手彫りのテーブル等を丁寧に生産しています。</p>
                </div>
                <div class="collectionItem">
                    <ul class="collectionItem__list">
                        <li>
                            <a href="">
                                <dl>
                                    <dt>KENSINGTON 2.5P SOFA</dt>
                                    <dd><img src="/assets/img/pg_collection/160901/item05.jpg" alt=""></dd>
                                </dl>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <dl>
                                    <dt>BENSINGTON 2P SOFA</dt>
                                    <dd><img src="/assets/img/pg_collection/160901/item04.jpg" alt=""></dd>
                                </dl>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <dl>
                                    <dt>NEWCOUNTHENRY　TC 2PSOFA</dt>
                                    <dd><img src="/assets/img/pg_collection/160901/item02.jpg" alt=""></dd>
                                </dl>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <dl>
                                    <dt>FROSTER 2.5PSOFA</dt>
                                    <dd><img src="/assets/img/pg_collection/160901/item03.jpg" alt=""></dd>
                                </dl>
                            </a>
                        </li>
                    </ul>
                    <p class="ctaMessage">他にも魅力的な家具をご紹介しております。<br>
                    下記ボタンよりwebサイトにお立ち寄りください。</p>
                    <a href="/" class="ctaBtn">HALOの2人掛けソファを探す</a>
                </div>
            </section>
        </article>
    </main>
</body>
</html>
