<section class="resultHeader">
    <div class="resultHeaderInner cf is_center">
        <div class="controller--resultHeader cf">
            <ul class="itemController">
                <li><a href="#" class="jsc_searchItemModal_openTrigger">商品を絞り込む</a></li>
                <li><a href="#" class="jsc_sortItemController_openTrigger">並び替える</a></li>
            </ul>
            <ul class="view jsc_changeview is_pc">
                <li>
                    <input id="viewTypeA" name="view" value="" type="radio" checked="checked">
                    <label for="viewTypeA" class="viewLabel label--viewTypeA"></label>
                </li>
                <li>
                    <input id="viewTypeB" name="view" value="" type="radio">
                    <label for="viewTypeB" class="viewLabel label--viewTypeB"></label>
                </li>
            </ul>
            <ul class="sort">
                <li class="sort--new">
                    <input id="sortNew" name="sort" value="1" type="radio" checked="checked" v-model="order" @change="sortList">
                    <label for="sortNew" class="sortLabel label--sortNew"></label>
                </li>
                <li class="sort--famous">
                    <input id="sortFamous" name="sort" value="order-famous" type="radio">
                    <label for="sortFamous" class="sortLabel label--sortFamous"></label>
                </li>
                <li class="sort--cheap">
                    <input id="sortCheap" name="sort" value="3" type="radio" v-model="order" @change="sortList">

                    <label for="sortCheap" class="sortLabel label--sortCheap"></label>
                </li>
                <li class="sort--expensive">
                    <input id="sortExpensive" name="sort" value="4" type="radio" v-model="order" @change="sortList">
                    <label for="sortExpensive" class="sortLabel label--sortExpensive"></label>
                </li>
            </ul>
        </div>
        <div class="searchResult searchResult--resultHeader">
            <h1 class="sec_ttl">検索結果</h1>
            <p class="searchResult__count"><span>{{ number }}</span>点</p>
        </div>
    </div>
</section>
<main class="main is_center">
    <section class="col_right right--search sp_searchModal">
        <section class="searchPanel searchPanel--category">
            <h2 class="ttl--searchPanel icn_category">カテゴリー</h2>
            <ul class="searchPanel__items">
                <?php foreach($categories as $key => $category): ?>
                <li>
                    <label class="checkbox" for="search-category-<?php echo $category["id"]; ?>"><?php echo $category["name"]; ?></label>
                    <input class="jsc-categories" <?php $category["name_en"] === $selectedCategory ? print_r("checked") : print_r(""); ?> type="checkbox" name="categories" value="<?php echo $category["id"]; ?>" id="search-category-<?php echo $category["id"]; ?>"  @change="narrowList" v-model="categories">
                </li>
                <?php endforeach; ?>
            </ul>
        </section>
        <section class="searchPanel searchPanel--brand">
            <h2 class="ttl--searchPanel icn_brand">ブランド</h2>
            <ul class="searchPanel__items">
                <?php foreach($blands as $key => $bland): ?>
                <li>
                    <label class="checkbox" for="search-bland-<?php echo $key; ?>"><?php echo $bland->name; ?></label>
                    <input class="jsc-categories" <?php $bland->name_en === $selectedBland ? print_r("checked") : print_r(""); ?> type="checkbox" name="blands" value="<?php echo $key; ?>" id="search-bland-<?php echo $key; ?>" @change="narrowList" v-model="blands">
                </li>
                <?php endforeach; ?>
            </ul>
        </section>
<!--        <section class="searchPanel">-->
<!--            <h2 class="ttl--searchPanel icn_type">タイプ</h2>-->
<!--            <ul class="searchPanel__items">-->
<!--                <li>-->
<!--                    <label class="checkbox" for="komoti">1人掛け</label>-->
<!--                    <input type="checkbox" name="komoti" value="hoge" id="komoti">-->
<!--                    <ul class="middleChild">-->
<!--                        <li>-->
<!--                            <label class="checkbox">-->
<!--                                <input type="checkbox" name="ko" value="hoge">アーム-->
<!--                            </label>-->
<!--                            <ul class="smallChild">-->
<!--                                <li>-->
<!--                                    <label class="checkbox">-->
<!--                                        <input type="checkbox" name="mago" value="hoge">木アーム-->
<!--                                    </label>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <label class="checkbox">-->
<!--                                        <input type="checkbox" name="mago" value="hoge">木以外のアーム-->
<!--                                    </label>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <label class="checkbox">-->
<!--                                        <input type="checkbox" name="mago" value="hoge">アームなし-->
<!--                                    </label>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <label class="checkbox" for="xxx">2人掛け</label>-->
<!--                    <input type="checkbox" name="hugahuga" value="hoge" id="xxx">-->
<!--                </li>-->
<!--                <li>-->
<!--                    <label class="checkbox" for="xxx">3人掛け</label>-->
<!--                    <input type="checkbox" name="hugahuga" value="hoge" id="xxx">-->
<!--                </li>-->
<!--                <li>-->
<!--                    <label class="checkbox" for="xxx">カウチソファ</label>-->
<!--                    <input type="checkbox" name="hugahuga" value="hoge" id="xxx">-->
<!--                </li>-->
<!--                <li>-->
<!--                    <label class="checkbox" for="xxx">LDソファ</label>-->
<!--                    <input type="checkbox" name="hugahuga" value="hoge" id="xxx">-->
<!--                </li>-->
<!--            </ul>-->
<!--        </section>-->
<!--        <section class="searchPanel">-->
<!--            <h2 class="ttl--searchPanel icn_color">色</h2>-->
<!--            <ul class="searchPanel__items searchPanel__items--color">-->
<!--                --><?php //foreach($colors as $key => $color_code): ?>
<!--                --><?php //if(! $color_code) { continue; } ?>
<!--                <li>-->
<!--                    <div class="cm_checkbox--color">-->
<!--                    <input id="color1" class="checkboxInput checkboxInput--color jsc_trigger_color" data-colorcode="--><?php //echo $color_code; ?><!--" type="checkbox">-->
<!--                        <label for="color1" class="checkboxLabel checkboxLabel--color" data-searchColor="red"></label>-->
<!--                    </div>-->
<!--                </li>-->
<!--                --><?php //endforeach; ?>
<!--            </ul>-->
<!--        </section>-->
        <a href="#" class="jsc_searchItemModal_closeArea is_sp">
          <span class="searchItemModal_closeBtn btn cta_lv1 jsc_searchItemModal_closeTrigger">設定した条件でさがす</span>
        </a>
    </section>
      <section class="col_left left--search">
          <div class="result__main">
              <ul class="resultList cf jsc_changeview_target">
                  <template v-for="item in items">
                  <!-- label_ new,sale,few,soon -->
                  <li>
                      <a href="<?php echo Uri::base(); ?>detail/{{ item.id }}">
                          <figure>
                              <img src="<?php echo Uri::base(); ?>assets/img/items/{{ item.id }}/thumbnail.jpg" :alt="item.name">
                          </figure>
                      </a>
                      <?php if(Session::get('user.hush')): ?>
                      <div class="add_favorite">
                          <input id="favorite_{{item.id}}" type="checkbox" value="{{ item.id }}"  @change="changeFavorite(item.id)" v-model='favorites' :disabled="isFavoriteUpdate" v-bind:checked="favorites.indexOf(1) >= 0">
                          <label for="favorite_{{item.id}}" class="favoriteLabel"></label>
                      </div>
                      <?php endif; ?>
                      <dl class="resultList__info">
                      <dt><a href="<?php echo Uri::base(); ?>detail/{{ item.id }}">{{ item.name }}</a></dt>
                          <dd class="info__price">税込<span>{{ (item.multina_price * 1.08) | currency '' 0 }}</span>円</dd>
                      </dl>
                  </li>
                  </template>
              </ul>
              <ol class="pager">
                  <li v-for="(key, pageNumber) in pageNumbers">
                      <a href="" @click="showThisPage(key + 1, $event)" class="{{ activePage == key + 1 ? 'is_current' : '' }}">{{ key + 1 }}</a>
                  </li>
                  <li><a href="#" @click="showThisPage(pageNumbers, $event)">最後</a></li>
              </ol>
          </div>
      </section>
</main>
