(function () {
	'use strict';
	var UACHECK = require('./modules/cm_uacheck');
	var NAVACCORDION = require('./modules/pc_navaccordion');
	var DOKIDOKI = require('./modules/pc_dokidoki');
	var SLIDEMENU = require('./modules/sp_slideMenu');
	var SELECTALLITEM = require('./modules/pc_selectallitem');
	var FLOWCAUTION = require('./modules/pc_flowcaution');
	var ADDCHECKED = require('./modules/pc_addChecked');

})();
