(function () {
	'use strict';

	var UACHECK = require('./modules/cm_uacheck');
	var NAVACCORDION = require('./modules/pc_navaccordion');
	var DOKIDOKI = require('./modules/pc_dokidoki');
	var SLIDEMENU = require('./modules/sp_slideMenu');
	var THUMBCHANGE = require('./modules/pc_thumbchange');
	var SEARCHSMOOTHSCROLL = require('./modules/pc_searchsmoothscroll');
	var FIXEDORDER = require('./modules/pc_fixedorder');
	var MOREREAD = require('./modules/sp_moreread');
	var THUMBSLIDE = require('./modules/sp_thumbslide');

})();
