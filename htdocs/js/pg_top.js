(function () {
	'use strict';

	var UACHECK = require('./modules/cm_uacheck');
	var NAVACCORDION = require('./modules/pc_navaccordion');
	var DOKIDOKI = require('./modules/pc_dokidoki');
	var SMOOTHSCROLL = require('./modules/pc_smoothscroll');
	var SLIDEMENU = require('./modules/sp_slideMenu');
	var CHANGEKV = require('./modules/pc_changeKV');
	var CHANGECATEGORY = require('./modules/pc_changeCategory');
	var SLIDER = require('./modules/pc_slider');
	var CHANGEBRANDINFO = require('./modules/pc_changebrandinfo');
	var TXTFUWAT = require('./modules/pc_txtfuwat');

})();
