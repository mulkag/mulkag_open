var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.PRERELEASE = function(){
  this.init();
}
MULKAG.FUNCTION.PRERELEASE.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
		this.$countDownBlock = $('.jsc-countdown');
		this.$year = this.$countDownBlock.find('.year');
		this.$month = this.$countDownBlock.find('.month');
		this.$day = this.$countDownBlock.find('.day');

    this.$img = $('.jsc-changeItems');
    this.imgSrc = this.$img.attr('src');

  },
  bindEvent: function(){
    var self = this;

		this.countdown();

  },
	countdown: function(){

		var start = new Date();
		var end = new Date("December 24,2016 00:00:00");
		var goal = end - start;
 		var a_day = 24 * 60 * 60 * 1000;

		var d = Math.floor(goal / a_day)
		this.$countDownBlock.html(d);
	}
};
$(function(){
  new MULKAG.FUNCTION.PRERELEASE();
});
