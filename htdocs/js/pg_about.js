(function () {
	'use strict';

	var UACHECK = require('./modules/cm_uacheck');
	var NAVACCORDION = require('./modules/pc_navaccordion');
	var DOKIDOKI = require('./modules/pc_dokidoki');
	var SLIDEMENU = require('./modules/sp_slideMenu');
	var ISFADE = require('./modules/pc_isFade');

})();