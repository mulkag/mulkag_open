(function () {
	'use strict';

	var UACHECK = require('./modules/cm_uacheck');
	var NAVACCORDION = require('./modules/pc_navaccordion');
	var DOKIDOKI = require('./modules/pc_dokidoki');
	var SMOOTHSCROLL = require('./modules/pc_smoothscroll');
	var SLIDEMENU = require('./modules/sp_slideMenu');
	var TXTFUWAT = require('./modules/pc_txtfuwat');

})();
