(function () {
	'use strict';

	var UACHECK = require('./modules/cm_uacheck');
	var NAVACCORDION = require('./modules/pc_navaccordion');
	var DOKIDOKI = require('./modules/pc_dokidoki');
	var SLIDEMENU = require('./modules/sp_slideMenu');
	// var YANASEARCH = require('./modules/search');
	// var VIEW = require('./modules/vue');
	var SEARCHBOXACCORDION = require('./modules/pc_searchboxaccordion');
	var SELECTCOLORS = require('./modules/pc_selectcolors');
	var CHANGEVIEW = require('./modules/pc_changeviews');
	var SEARCHSMOOTHSCROLL = require('./modules/pc_searchsmoothscroll');
	var ACCORDIONMIDDLECATEGORY = require('./modules/pc_accordionmiddlecategory');
	var ADDCHECKED = require('./modules/pc_addChecked');
	var SEARCHITEMMODAL = require('./modules/sp_searchItemModal');
	var TOGGLEITEMCONTROLLER = require('./modules/sp_toggleItemController');

})();
