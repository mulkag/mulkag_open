(function () {
	'use strict';

	var NAVACCORDION = require('./modules/pc_navaccordion');
	var DOKIDOKI = require('./modules/pc_dokidoki');
	var SLIDEMENU = require('./modules/sp_slideMenu');
	var SHOWCONTENT = require('./modules/pc_showcontent');

})();
