var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.SEARCHITEMMODAL = function(){
  this.init();
}
MULKAG.FUNCTION.SEARCHITEMMODAL.prototype = {
  STATE: {},
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$triggerSort = $('.jsc_sortItemController_openTrigger');
    this.$targetSort = $('.sort');

  },
  bindEvent: function(){
    var self = this;

    this.$triggerSort.on('touchend click', function(e){
      e.preventDefault();
      self.$targetSort.toggle();
    });

  }
};
$(function(){
  if($("body").is('.ua_sp')){
    new MULKAG.FUNCTION.SEARCHITEMMODAL();
  }
});
