var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.CHANGEKV = function(){
  this.init();
}
MULKAG.FUNCTION.CHANGEKV.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$kv = $('.jsc_changeKV');
 },
 bindEvent: function(){
   var self = this;
   var num = Math.ceil( Math.random()*2 );
   this.bgSrc = "url(/assets/img/pg_top/kv/kv" + num + ".jpg)";
   this.$kv.css({
     "background-image": self.bgSrc
   });

 }
};

$(function(){
  new MULKAG.FUNCTION.CHANGEKV();
});
