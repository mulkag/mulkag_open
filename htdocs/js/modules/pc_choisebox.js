var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.CHOISEBOX = function($choiseBox){
  this.$choiseBox = $choiseBox;
  this.init();
}
MULKAG.FUNCTION.CHOISEBOX.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$raidobox = this.$choiseBox.find('input');
    this.$detailContents = this.$choiseBox.find('.choiseBox_detail_contents');

  },
  bindEvent: function(){
    var self = this;

    this.fadeContents();

    this.$choiseBox.on('change', function(){
      self.fadeContents();
    });

  },
  fadeContents: function(){
    var self = this;
    var detailType = self.$choiseBox.find('input:checked').data('detailtype');

    self.$detailContents.hide();
    self.$choiseBox.find('.' + detailType).fadeIn(500);

  }
};

$(function(){
  $('.jsc-choiseBox').each(function(){
    new MULKAG.FUNCTION.CHOISEBOX($(this));
  });
});
