var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.SEARCHBOXACCORDION = function(){
  this.init();
}
MULKAG.FUNCTION.SEARCHBOXACCORDION.prototype = {
  SLIDESPEED: 200,
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$triggerWrapper = $('.jsc_accordion_trigger');
  },
  bindEvent: function(){
    var self = this;

    this.$triggerWrapper.on('click', function(e){
      e.preventDefault();
      self.slideWrapper(this);
    });
  },
  slideWrapper: function(trigger){
    var self = this,
      $target = $($(trigger).attr('href'));

    if($target.is(':animated')){
      return;
    }

    if($(trigger).hasClass('is_active')){
      $(trigger).removeClass('is_active');
      $target.slideUp(self.SLIDESPEED);
    } else {
      $(trigger).addClass('is_active');
      $target.slideDown(self.SLIDESPEED);
    }
  }
};
$(function(){
new MULKAG.FUNCTION.SEARCHBOXACCORDION(this);
});