var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.ISFADE = function(){
  this.init();
}
MULKAG.FUNCTION.ISFADE.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$target = $('.kv_inner');
  },
  bindEvent: function(){

    this.$target.addClass('is_fade');


  }
};

$(function(){
new MULKAG.FUNCTION.ISFADE();
});