var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.CHANGEVIEW = function(){
  this.init();
}
MULKAG.FUNCTION.CHANGEVIEW.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$triggers = $('.jsc_changeview').find('input');
    this.$target = $('.jsc_changeview_target');
  },
  bindEvent: function(){
    var self = this;

    this.$triggers.on('change', function(){
      self.changeViewMode(this);
    });
  },
  changeViewMode: function(input){
    var self = this;

    if($(input).attr('id') == "viewTypeB"){
      self.$target.addClass('is_many');
    }else {
      self.$target.removeClass('is_many');
    }
  }
};

$(function(){
new MULKAG.FUNCTION.CHANGEVIEW();
});