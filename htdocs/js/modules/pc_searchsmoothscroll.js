var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.SEARCHSMOOTHSCROLL = function(){
  this.init();
}
MULKAG.FUNCTION.SEARCHSMOOTHSCROLL.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$body = $("html,body");
    this.$trigger = $('.jsc_smoothscroll');
    this.headerHeight = 70;
  },
  bindEvent: function(){
    var self = this;

    this.$trigger.on('click', function(e){
      self.scrollLinks(e, this);
    });

  },
  scrollLinks: function(e, trigger){
    var self = this,
      href = $(trigger).attr('href'),
      targetPosition = $(href).offset().top;

    e.preventDefault();

    self.$body.animate({
      scrollTop : targetPosition - self.headerHeight
    });
  }
};

$(function(){
new MULKAG.FUNCTION.SEARCHSMOOTHSCROLL();
});