var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.SEARCHITEMMODAL = function(){
  this.init();
}
MULKAG.FUNCTION.SEARCHITEMMODAL.prototype = {
  STATE: {
    BACKGROUND:'<div class="jsc_slideMenu_bg"></div>',
    SLIDESPEED: 100
  },
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$triggerOpenModal = $('.jsc_searchItemModal_openTrigger');
    this.$triggerCloseModal = $('.jsc_searchItemModal_closeTrigger');
    this.$closeArea = $('.jsc_searchItemModal_closeArea');
    this.$searchModal = $('.sp_searchModal');
    this.$searchModal.after(this.STATE.BACKGROUND);
    this.$slideBg = this.$searchModal.next('.jsc_slideMenu_bg');

    this.$accordionOpenTrigger = $('.ttl--searchPanel');
    this.$accordionList = $('.searchPanel__items');
    this.$hamburgerMenu = $('.jsc_hamburgerMenu');

  },
  bindEvent: function(){
    var self = this;

    this.$triggerOpenModal.on('touchend click', function(e){
      e.preventDefault();
      self.actionSlide('open');
    });

    this.$triggerCloseModal.on('touchend click', function(e){
      e.preventDefault();
      self.actionSlide('close');
    });

    this.$accordionOpenTrigger.on('click', function(e){
      e.preventDefault();
      $(this).next('.searchPanel__items').slideToggle(self.STATE.SLIDESPEED);
      $(this).toggleClass('is_open');
    });

    this.$slideBg.on('touchend click', function(e){
      e.preventDefault();
      self.actionSlide('close');
    });

  },
  actionSlide: function(action){
    var self = this;

    if(action === "open"){
      this.$searchModal.addClass('is_open');
      this.$closeArea.addClass('is_open');
      this.$slideBg.show(function(){
        self.$slideBg.addClass('is_show');
        self.$hamburgerMenu.addClass('disable');
      });
    }else {
      this.$searchModal.removeClass('is_open');
      this.$closeArea.removeClass('is_open');
      this.$slideBg.removeClass('is_show');
      this.$hamburgerMenu.removeClass('disable');
      this.$slideBg.fadeOut();
    }


  }
};
$(function(){
  if($("body").is('.ua_sp')){
    new MULKAG.FUNCTION.SEARCHITEMMODAL();
  }
});
