var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.BRANDSLIDER = function(){
  this.init();
}
MULKAG.FUNCTION.BRANDSLIDER.prototype = {
  FADEINSPEED: 500,
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
   this.$trigger = $('.jsc_changeBrandInfo').find('a');
   this.$target = $('.jsc_target_changeKv');
 },
 bindEvent: function(){
   var self = this;

   this.$trigger.on('mouseover', function(){
     self.changeBrandInfo(this);
   });

 },
 changeBrandInfo: function(trigger){
   var href = $(trigger).attr('href'),
   activeItem = $('.is_show');

   if($(href).is(':animated')){
     return;
   }

   activeItem.hide();
   activeItem.removeClass('is_show');
   $(href).fadeIn(self.FADEINSPEED);
   $(href).addClass('is_show');
 }
};
$(function(){
  new MULKAG.FUNCTION.BRANDSLIDER();
});