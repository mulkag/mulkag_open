var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.CHANGEFORMACTION = function(){
  this.init();
}
MULKAG.FUNCTION.CHANGEFORMACTION.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$inquiryList = $('.jsc_inquiryList');
    this.$form = $('.jsc_form--contact');
    this.$inquiryInput = this.$inquiryList.find('input');
  },
  bindEvent: function(){
    var self = this;

    this.FormItemSet("formPatternA");

    this.$inquiryInput.on('change', function(){

      var checkedVal = self.$inquiryList.find('input:checked').val();
 
      self.resetFormClass();
      self.FormItemSet(checkedVal);

    });

  },
  FormItemSet: function(pattern){
    var self = this;

    if(pattern == "formPatternA"){
      self.$form.addClass('form--contact--normal');
    } else if(pattern == "formPatternB"){
      self.$form.addClass('form--contact--item');
    } else if(pattern == "formPatternC"){
      self.$form.addClass('form--contact--cancel');
    }

  },
  resetFormClass: function(pattern){
    var self = this;

      self.$form.removeClass('form--contact--normal');
      self.$form.removeClass('form--contact--item');
      self.$form.removeClass('form--contact--cancel');

  }
};

$(function(){
new MULKAG.FUNCTION.CHANGEFORMACTION();
});