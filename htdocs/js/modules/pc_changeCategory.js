var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.CHANGECATEGORY = function(){
  this.init();
}
MULKAG.FUNCTION.CHANGECATEGORY.prototype = {
  STATE: {
    FIRST_ACTIVE_SLIDE: '#category__sofa'
  },
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
   this.$categories = $('.jsc_trigger_changeCategory').find('a');
   this.$slides = $('.category');
  },
   bindEvent: function(){
     var self = this;

     this.setDefaultStatus();

     this.$categories.on('mouseover', function(){
        self.changeNav(this);
        self.changeKV(this);
     });
   },
   setDefaultStatus: function(){
    var self = this;

      $(self.STATE.FIRST_ACTIVE_SLIDE).addClass('is_active');

   },
   changeNav: function(nav){

      $('.jsc_trigger_changeCategory').find('.is_current').removeClass('is_current');
      $(nav).addClass('is_current');

   },
   changeKV: function(nav){
      var href = $(nav).data('href');

      $('.category.is_active').toggleClass('is_active', $(this).hide());

      $(href).toggleClass('is_active');

      this.lasyLoadCheck(href);

   },
   lasyLoadCheck: function(href){
      var self = this,
          activeId = $(href).attr('id'),
          activeSlide = $("#" + activeId),
          activeSlideItems = activeSlide.find('img'),
          lasyFlag;

      activeSlideItems.each(function(){
        if(!$(this).attr('src')){
          self.lasyLoad(activeSlideItems);
        }
      });

   },
   lasyLoad: function(activeSlideItems){
      var self = this;

      activeSlideItems.each(function(){
        if(!$(this).attr('src')){
          var imgSrc = $(this).data('imgsrc');
          $(this).attr('src', imgSrc);
        }
      });
   }
};

$(function(){
  new MULKAG.FUNCTION.CHANGECATEGORY();
});
