var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.SHOWCONTENT = function(){
  this.init();
}
MULKAG.FUNCTION.SHOWCONTENT.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
    this.defaultSet();
  },
  setParameters: function(){
    this.$body = $("html,body");
    this.$trigger = $('.jsc_trigger_showcontent');
    this.$target = $('.inputCustomerInfo');

    this.$otherInputs = this.$target.find('input');

  },
  bindEvent: function(){
    var self = this;

    this.$trigger.on('click', function(e){
      e.preventDefault();
      $(this).hide();
      self.showContent();
    });

  },
  defaultSet: function(){
    var self = this;
    
    this.$otherInputs.each(function(){
      if($(this).val()){
        self.$trigger.hide();
        self.showContent();
        return false;
      }
    });

  },
  showContent: function(){
    var self = this;

    self.$target.show();

    self.$body.animate({
      scrollTop : $('.customerInfo').offset().top　- 100
    });
  }
};

$(window).on('load',function(){
  new MULKAG.FUNCTION.SHOWCONTENT();
});