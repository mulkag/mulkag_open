var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.THUMBCHANGE = function(){
  this.init();
}
MULKAG.FUNCTION.THUMBCHANGE.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$thumbMain = $('.jsc_thumbSlide_main').find('li');
    this.$thumbSub = $('.jsc_thumbSlide_sub').find('li');
  },
  bindEvent: function(){
    var self = this;
    this.$thumbSub.hover(function(){

      $('.is_active').removeClass('is_active');

      var index = self.$thumbSub.index(this);
      $('.is_active').removeClass('is_active');
      self.$thumbMain.eq(index).addClass('is_active');

    });

  }
};

$(function(){
new MULKAG.FUNCTION.THUMBCHANGE();
});