var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.SLIDEMENU = function(){
  this.init();
}
MULKAG.FUNCTION.SLIDEMENU.prototype = {
  STATE: {
    BACKGROUND:'<div class="jsc_slideMenu_bg"></div>',
    CONTROLL_CSSANIMATION:'oTransitionEnd mozTransitionEnd webkitTransitionEnd transitionend'
  },
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$slideTrigger = $(".jsc_hamburgerMenu");
    this.$slideBody = $(".jsc_slideMenu_body");
    this.$slideBody.after(this.STATE.BACKGROUND);
    this.$slideBg = $('.jsc_slideMenu_bg');
    this.$anchor = this.$slideBody.find('a');

    this.$accordionTrigger = $('.jsc_sp_navAccordion');
    this.$accordionTarget = this.$accordionTrigger.find('.jsc_navAccordion_target');

 },
 bindEvent: function(){
   var self = this;

  this.$slideTrigger.on('touchstart click', function(e){
    e.preventDefault();
    self.actionSlide();
  });

  this.$slideBg.on('click', function(){
    self.actionSlide();
  });

  this.$accordionTrigger.on('touchstart click', function(e){
    e.preventDefault();
    self.accordion($(this));
  });

  this.$anchor.on('touchstart', function(){
    $(this).trigger('click');

  });

 },
  accordion: function($trigger){

    if(this.$accordionTarget.is(':hidden')){
      $trigger.addClass('is_open');
    }else {
      $trigger.removeClass('is_open');
    }
    this.$accordionTarget.slideToggle();

  },
  actionSlide: function(){
    var self = this;

    if(this.$slideTrigger.hasClass('active')){
      this.$slideTrigger.removeClass('active');
      this.$slideBody.removeClass('is_show');
      this.$slideBg.removeClass('is_show');
      this.$slideBg.fadeOut();
    }else {
      this.$slideTrigger.addClass('active');
      this.$slideBody.addClass('is_show');
      this.$slideBg.show(function(){
        self.$slideBg.addClass('is_show');
      });
    }

  }
};
$(function(){
  if($("body").is('.ua_sp')){
    new MULKAG.FUNCTION.SLIDEMENU();
  }
});
