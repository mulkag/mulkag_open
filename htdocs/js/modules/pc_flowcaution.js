var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.FLOWCAUTION = function(){
  this.init();
}
MULKAG.FUNCTION.FLOWCAUTION.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$trigger = $('.jsc_toggleshow_trigger');

  },
  bindEvent: function(){
    var self = this;

    this.$trigger.on('change', function(){
      var id = $(this).attr('id');
      $('.is_active').removeClass('is_active');
      $('.' + id).addClass('is_active');
    });
  }
};

$(function(){
new MULKAG.FUNCTION.FLOWCAUTION();
});