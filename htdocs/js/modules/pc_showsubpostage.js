var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.SHOWSUBPOSTAGE = function(){
  this.init();
}
MULKAG.FUNCTION.SHOWSUBPOSTAGE.prototype = {
  SCROLLSPEED : 500,
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$body = $("html,body");
    this.headerHeight = $('.header').height();
    this.$trigger = $('.jsc_showsubpostage_trigger');
    this.$subPostageArea = $('#subPostageArea');
  },
  bindEvent: function(){
    var self = this;

    this.$trigger.on('click', function(e){
      self.showPostage(this,e);
    });
  },
  showPostage: function(trigger, e){
    var self = this,
      id = $(trigger).attr('href');

    $(trigger).hide();
    $(id).show();

    var scrollPosition = $(id).offset().top - (self.headerHeight);

    e.preventDefault();

    self.$body.animate({
      scrollTop : scrollPosition
    }, self.SCROLLSPEED);

  }
};

$(function(){
new MULKAG.FUNCTION.SHOWSUBPOSTAGE();
});
