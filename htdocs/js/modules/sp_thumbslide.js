var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.THUMBSLIDE = function(){
  this.init();
}
MULKAG.FUNCTION.THUMBSLIDE.prototype = {
  STATE: {},
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$thumbslide = $('.jsc-thumbSlide');
    this.$slideItems = this.$thumbslide.find('li');
    this.slideLength = this.$slideItems.length;
  },
  bindEvent: function(){
    var self = this;

    this.$thumbslide.outerWidth(this.slideLength + "00%");
    this.$slideItems.outerWidth(100 / this.slideLength + "%");

    if(this.slideLength > 1){
      $('.scrollArrow').show();
    }
    this.$thumbslide.on('touchmove', function(){
      $('.scrollArrow').fadeOut();
    });

  }
};
$(function(){
  if($("body").is('.ua_sp')){
    new MULKAG.FUNCTION.THUMBSLIDE();
  }
});
