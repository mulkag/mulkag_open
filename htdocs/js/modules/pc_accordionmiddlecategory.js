var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.ACCORDIONMIDDLECATEGORY = function(selector){
  this.$bigCategory = $(selector).children('label').children('input');
  this.init();
}
MULKAG.FUNCTION.ACCORDIONMIDDLECATEGORY.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$middleCategory = $('.middleChild').children('li').children('label').children('input');
    this.$smallCategory = $('.smallChild').children('li').children('label').children('input');
  },
  bindEvent: function(){
    var self = this;

    this.$bigCategory.on('change', function(){
      self.showMiddleCategory(this);
    });

    this.$middleCategory.on('click', function(){
      self.showSmallCategory(this);

    });

  },
  showMiddleCategory: function(bigcategory){
    var $middleChild = $(bigcategory).parents('li').find('.middleChild');

    if($(bigcategory).prop('checked') && $middleChild.length){
      $middleChild.addClass('is_show');
    }else {
      $middleChild.find('input').prop('checked', false);
      $middleChild.find('label').removeClass('is_checked');
      $middleChild.removeClass('is_show');
    }
  },
  showSmallCategory: function(middlecategory){
    var $smallChild = $(middlecategory).parents('li').find('.smallChild');

    if($(middlecategory).prop('checked') && $smallChild.length){
      $smallChild.addClass('is_show');
    }else {
      $smallChild.find('input').prop('checked', false);
      $smallChild.find('label').removeClass('is_checked');
      $smallChild.removeClass('is_show');
    }
  }
};

$(function(){
  $('.searchPanel__items').children('li').each(function(){
    new MULKAG.FUNCTION.ACCORDIONMIDDLECATEGORY(this);
  });
});