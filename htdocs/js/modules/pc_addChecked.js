var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.ADDCHECKED = function(){
  this.init();
}
MULKAG.FUNCTION.ADDCHECKED.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$checkbox = $('.checkbox');
    this.$checkInputs = $('.jsc-categories');
    this.$radiobox = $('.radiobox');
  },
  bindEvent: function(){
    var self = this;

    this.setDefaultStatus();


    this.$checkbox.each(function(){
      self.actionCheckbox(this);
    });

    this.$radiobox.each(function(){
      self.actionRadiobox(this);
    });

  },
  actionCheckbox: function(label){
    var self = this,
        $label = $(label),
        $input = $label.next('input');

    if ($input.is(':checked')) {
      $label.addClass('is_checked');
    } else {
      $label.removeClass('is_checked');
    }

    $input.on('change', function(){
      if ($(this).is(':checked')) {
        $label.addClass('is_checked');
      } else {
        $label.removeClass('is_checked');
      }
    });
  },
  actionRadiobox: function(label){
    var self = this,
        $label = $(label),
        $input = $label.next('input');

    $input.on('change', function(){
      var _self = this,
          name = $(this).attr('name');

      $('input[name="' + name + '"]').each(function(){

        if($(this).is(':checked')){
          $(this).prev('label').addClass('is_checked');
        }else {
          $(this).prev('label').removeClass('is_checked');
        }

      });



    });

  },
  setDefaultStatus: function(){
    $('.is_checked').find('input').prop('checked', true);
  }
};

$(function(){
new MULKAG.FUNCTION.ADDCHECKED();
});
