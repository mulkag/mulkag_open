var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.CHANGE_ITEMSCORE = function($selector){
  this.$base = $selector;
  this.init();
}
MULKAG.FUNCTION.CHANGE_ITEMSCORE.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$allCheckTrigger = this.$base.find('.jsc_selectAllItem');
    this.$targetCheckbox = this.$base.find('.jsc_selectAllItem_checkbox');

    this.$totalCellNumber = this.$base.find('.jsc-cart-total-number');
    this.$totalCellSendprice = this.$base.find('.jsc-cart-total-send-price');
    this.$totalCellPrice = this.$base.find('.jsc-cart-total-price');

  },
  bindEvent: function(){
    var self = this;

    this.$allCheckTrigger.on('change', function(){
      self.change_controller();
    });

    this.$targetCheckbox.on('change', function(){
      self.change_controller();
    });

  },
  change_controller: function(){
    var self = this;
    var $checkedCell = self.$targetCheckbox.filter(':checked').parents('tr');
    self.change_number();
    self.change_postage($checkedCell);
    self.change_price($checkedCell);
  },
  change_number: function(){
    var itemLength = this.$targetCheckbox.filter(':checked').length;
    this.$totalCellNumber.html(itemLength);
  },
  change_price: function($checkedCell){
    var self = this,
        $feeCell = $checkedCell.find('.jsc-cart-smallprice'),
        totalScrorePrice = 0;
    $feeCell.each(function(){
      totalScrorePrice += parseInt($(this).html().replace(/,/g,''));
    });
    self.$totalCellPrice.html(totalScrorePrice);
  },
  change_postage: function($checkedCell){
    var self = this,
        $postageCell = $checkedCell.find('.jsc-cart-postage'),
        totalScrorePostage = 0;
    $postageCell.each(function(){
      totalScrorePostage += parseInt($(this).html().replace(/,/g,''));
    });
    self.$totalCellSendprice.html(totalScrorePostage);
  }
};

$(function(){
  $(".jsc-changeItemScore").each(function(){
    new MULKAG.FUNCTION.CHANGE_ITEMSCORE($(this));
  });
});
