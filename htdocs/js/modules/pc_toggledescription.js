var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.TOGGLEDESCRIPTION = function(){
  this.init();
}
MULKAG.FUNCTION.TOGGLEDESCRIPTION.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$trigger = $('.jsc_toggleshow_trigger');

  },
  bindEvent: function(){
    var self = this;

    this.$trigger.each(function(){
      $(this).on('change', function(){
        var id = $(this).attr('id');

        if($(this).prop('checked')) {
          $('.' + id).addClass('is_active');
          console.log("checkされました");
        }
        else {
          $('.' + id).removeClass('is_active');
          console.log("チェックされていません。");
        }
      });
    });

  }
};

$(function(){
  new MULKAG.FUNCTION.TOGGLEDESCRIPTION();
});