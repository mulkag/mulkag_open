var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.AUTOKANA = function(){
  this.init();
}
MULKAG.FUNCTION.AUTOKANA.prototype = {
  init: function(){
    this.bindEvent();
  },
  bindEvent: function(){

    $.fn.autoKana('input[name="name"]', 'input[name="kana"]', {
      katakana : false  //true：カタカナ、false：ひらがな（デフォルト）
    });

  }
};

$(function(){
new MULKAG.FUNCTION.AUTOKANA();
});
