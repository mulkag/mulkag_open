var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.TXTFUWAT = function(){
  this.init();
}
MULKAG.FUNCTION.TXTFUWAT.prototype = {
  STATE: {
    BEFOREDISTANCE: 200
  },
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$window = $(window);
    this.$targetTxt = $('.fuwat');
  },
  bindEvent: function(){
    var self = this;

    this.$targetTxt.css('visibility','hidden');
    this.fuwatAnime(this.$window);

    this.$window.scroll(function(){
      self.fuwatAnime($(this));
    });

  },
  fuwatAnime: function($window){
    var self = this;
    var windowHeight = $window.height(),
    topWindow = $window.scrollTop();

    self.$targetTxt.each(function(){
      var objectPosition = $(this).offset().top;
      if(topWindow > objectPosition - windowHeight){
        $(this).addClass("fuwatAnime");
      }
    });

  }

};

$(function(){
  new MULKAG.FUNCTION.TXTFUWAT();
});
