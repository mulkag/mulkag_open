var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.UACHECK = function(){
  this.init();
}
MULKAG.FUNCTION.UACHECK.prototype = {
  STATE: {
    BREAKPOINT: 985
  },
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$window = $(window);
    this.$body = $('body');

    this.windowsize = this.$window.outerWidth();

  },
  bindEvent: function(){
    var self = this;
    var timer = false;

    this.setUaClass();

    $(window).resize(function() {
      var _self = self;
      if (timer !== false) {
          clearTimeout(timer);
      }
      timer = setTimeout(function() {
          _self.setUaClass();
      }, 200);
    });

  },
  setUaClass: function(timer){
    var self  = this;

    this.windowsize = this.$window.outerWidth();

    if(this.windowsize <= this.STATE.BREAKPOINT){
      this.$body.removeClass('ua_pc');
      this.$body.addClass('ua_sp');
    }else {
      this.$body.removeClass('ua_sp');
      this.$body.addClass('ua_pc');      
    }

  }
};
$(function(){
  new MULKAG.FUNCTION.UACHECK();
});
