var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.MOREREAD = function(){
  this.init();
}
MULKAG.FUNCTION.MOREREAD.prototype = {
  STATE: {},
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$target = $('.detailSection');
  },
  bindEvent: function(){
    var self = this;

    this.$target.each(function(){
      var _self = this,
          $trigger = $(this).find('.detailSection_overTrigger');

      $trigger.on('click', function(e){
        e.preventDefault();
        $(_self).addClass('is_open');
      });

    });
  }
};
$(function(){
  if($("body").is('.ua_sp')){
    new MULKAG.FUNCTION.MOREREAD();
  }
});
