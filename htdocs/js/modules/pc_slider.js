var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.SLIDER = function($base){
  this.$base = $base;
  this.$lists = $base.find('li');
  this.listsLength = this.$lists.length;

  this.activeIndex = 1;
  this.init();
};
MULKAG.FUNCTION.SLIDER.prototype = {
  STATE: {
    SLIDEINTERVAL : 3000,
    SLIDESPEED : 500
  },
  init: function(){
    this.setParameters();
    this.moveTimeout();
  },
  setParameters: function(){
    this.$base.append(this.$lists.clone());
    this.$lists = this.$base.find('li');
  },
  moveTimeout: function(){
   var self = this;

   setInterval(function(){
     self.moveContent();
   }, self.STATE.SLIDEINTERVAL);

 },
 moveContent: function(){

   if(this.$base.is(":hidden")){
     return;
   }

   var baseLeft = parseInt(this.$base.css('left'));
   var moveValue = 0 - this.$lists.eq(this.activeIndex).width();

   if(baseLeft){
     moveValue = baseLeft  - this.$lists.eq(this.activeIndex).width();
   }

   var self = this;

  this.$base.animate({
    left: moveValue
  }, self.STATE.SLIDESPEED, function(){

      self.activeIndex++;

      if(self.activeIndex > self.listsLength){
        self.activeIndex = 0;
        self.$base.css("left", 0);
      }

    });
  }
};

$(function(){
  $('.jsc_slider').each(function(){
    new MULKAG.FUNCTION.SLIDER($(this));
  });
});