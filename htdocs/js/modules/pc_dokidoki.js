var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.DOKIDOKI = function(){
  this.init();
}
MULKAG.FUNCTION.DOKIDOKI.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$trigger = $('.glonav > li > a');
  },
  bindEvent: function(){
    var self = this;
    this.$trigger.hover(function(){
      $(this).addClass('is_dokidoki');
      },function(){
      $(this).removeClass('is_dokidoki');
      }
    );
  }
};
$(function(){
  new MULKAG.FUNCTION.DOKIDOKI();
});