var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.SELECTALLITEM = function(selector){
  this.init();
}
MULKAG.FUNCTION.SELECTALLITEM.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$wrapper = $('.cart');
    this.$trigger = this.$wrapper.find('.jsc_selectAllItem');
    this.$triggerLabel = this.$trigger.prev('.checkbox');
    this.$target = this.$wrapper.find('.jsc_selectAllItem_checkbox');
    this.$targetLabel = this.$target.prev('.checkbox');
  },
  bindEvent: function(){
    var self = this;

    this.$trigger.on('change', function(){
      if($(this).is(':checked')){
        self.$target.prop('checked', true);
        self.$targetLabel.addClass('is_checked');
      } else {
        self.$target.prop('checked', false);
        self.$targetLabel.removeClass('is_checked');
      }
    });

    this.$target.on('change', function(){
      var targetLength = self.$target.length;
      var checkedTargetLength = self.$target.filter(':checked').length;

      if(targetLength > checkedTargetLength){
        self.$trigger.prop('checked', false);
        self.$triggerLabel.removeClass('is_checked');
      }

    });

  }
};

$(function(){
  new MULKAG.FUNCTION.SELECTALLITEM();
});
