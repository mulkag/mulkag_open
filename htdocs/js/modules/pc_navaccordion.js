var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.NAVACCORDION = function(nav){
  this.$nav = $(nav);
  this.init();
}
MULKAG.FUNCTION.NAVACCORDION.prototype = {
  flag: false,
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$trigger = this.$nav.find('.jsc_navAccordion_trigger');
    this.$target = this.$nav.find('.jsc_navAccordion_target');
  },
  bindEvent: function(){
    var self = this;

    this.$trigger.hover(function(){
      self.flagAction(true);
    },
    function(){
      self.flagAction(false);
    });

    this.$target.hover(function(){
      self.flagAction(true);
    },
    function(){
      self.flagAction(false);
    });

  },
  flagAction: function(flag){
    var self = this;

    if(flag){
      self.$target.show();
    }else {
      self.$target.hide();
    }

  }
};

$(function(){
  if($("body").is('.ua_pc')){
    $('.jsc_navAccordion').each(function(){
      new MULKAG.FUNCTION.NAVACCORDION($(this));
    });
  }
});
