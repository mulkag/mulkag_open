var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.GUIDESLIDE = function(){
  this.init();
}
MULKAG.FUNCTION.GUIDESLIDE.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$trigger = $('.jsc_guideSlide').find('a');

  },
  bindEvent: function(){
    var self = this;

    this.$trigger.mouseover(function(e){
      e.preventDefault();
      self.changeSlide(this);
    });
    this.$trigger.on('click', function(e){
      e.preventDefault();
      self.changeSlide(this);
    });

  },
  changeSlide: function(trigger){
    var href = $(trigger).attr('href');

    $(trigger).toggleClass('is_active');
    $('.is_active').removeClass('is_active');
    $(href).toggleClass('is_active');
  }
};

$(function(){
new MULKAG.FUNCTION.GUIDESLIDE();
});