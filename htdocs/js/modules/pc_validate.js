var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.VALIDATE = function(){
  this.init();
}
MULKAG.FUNCTION.VALIDATE.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.$form = $('.formRegist');
  },
  bindEvent: function(){
    var self = this;

    this.$form.validate({options});
    var options = {
      rules : {
        comment: {
          required: true,
          minlength: 5
        }
      }}
    }
};

$(function(){
  new MULKAG.FUNCTION.VALIDATE();
});