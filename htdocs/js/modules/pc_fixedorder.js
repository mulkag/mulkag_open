var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.FIXEDORDER = function(){
  this.init();
}
MULKAG.FUNCTION.FIXEDORDER.prototype = {
  init: function(){
    this.setParameters();
    this.bindEvent();
  },
  setParameters: function(){
    this.headerHeight = $('.header').height();
    this.headerBottomHeight = $('.header_bottom').height();
    this.$rightCol = $('.col_right');
    this.rightColTop = this.$rightCol.offset().top;
    this.rightColHeight = this.$rightCol.height();
    this.rightColBottom = this.rightColHeight + this.$rightCol.offset().top;
    this.$nav = $(".order");
    this.navTop = this.$nav.offset().top;

  },
  bindEvent: function(){
    var self = this;

    $(window).scroll(function() {
      var scrollValue = $(this).scrollTop(),
          scrollWindowBottom = $(this).scrollTop() + $(window).height();

      if(scrollValue >= self.rightColTop){
        self.$nav.addClass('orderFixed');
      }else {
        self.$nav.removeClass('orderFixed');
      }

      if(scrollWindowBottom >= self.rightColBottom){
        self.$nav.removeClass('orderFixed');
        self.$nav.addClass('orderBottom');
      }else {
        self.$nav.removeClass('orderBottom');
      }
    });
  }
};

$(window).load(function() {
new MULKAG.FUNCTION.FIXEDORDER();
});
