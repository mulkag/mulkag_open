window.onload = function () {
    Vue.filter('arrayShift', function(array) {
        var firstKey = '';
        for(var key in array) {
            firstKey = key;
        }
        return array[firstKey].photo;
    });
    var search = new Vue({
        el: '.jsc-search',
        data: function() {
            return {
                limit: 50,
                offset: 1,
                items: [],
                number: 0,
                pageNumbers: 0,
                activePage: 1,
                order: 1,
                categories: [],
                blands: [],
                materials: [],
            };
        },
        created: function() {
            this.buildList();
            this.deferred = new $.Deferred();
        },
        methods: {
            buildList: function(isDeferred){
                var that = this;
                $.ajax({
                    type: 'GET',
                    url: '/api/search.json',
                    data: {
                        'limit': this.limit,
                        'offset': this.offset,
                        'order': this.order,
                        'categories': this.categories,
                        'blands': this.blands,
                        'materials': this.materials,
                    },
                    success: function(result){
                        that.number = result.number;
                        that.items = result.items;
                        that.buildPagination();

                        if(isDeferred) {
                            that.deferred.resolve();
                        }
                    }
                });
            },
            buildPagination: function(){
                this.pageNumbers = Math.ceil(this.number / this.limit);
            },
            showThisPage: function(pageNumber, event) {
                event.preventDefault();
                if(pageNumber === this.activePage) {
                    return;
                }
                this.offset = pageNumber;
                this.activePage = pageNumber;
                this.buildList(true);
                this.deferred.promise().then(function() {
                    $("html,body").animate({scrollTop:0},"300");
                });
            },
            sortList: function() {
                this.buildList();
            },
            narrowList: function() {
                this.buildList();
            },
        }
    });
};
