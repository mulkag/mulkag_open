var MULKAG = MULKAG || {};
MULKAG.FUNCTION = MULKAG.FUNCTION || {};

MULKAG.FUNCTION.STOPTRANSITION = function(){
  this.init();
}
MULKAG.FUNCTION.STOPTRANSITION.prototype = {
  init: function(){
    this.bindEvent();
  },
  bindEvent: function(){

    var isChanged = false;

    $(window).on("beforeunload", function() {
        if (isChanged) {
            return "ご注文手続きを入力途中です。\nこのページを離れると、入力したデータが削除されます。";
        }
    });

    $("input, select, textarea").on("keyup change",function() {
        var text_num = 0;
        for (var i=0; i< $("input[type='text']").length; i++) {
            text_num +=  $("input[type='text']").eq(i).val().length;
        }
        for (var i=0; i< $("input[type='email']").length; i++) {
            text_num += $("input[type='email']").eq(i).val().length;
        }
        for (var i=0; i< $("input[type='number']").length; i++) {
            text_num += $("input[type='number']").eq(i).val().length;
        }
        for (var i=0; i< $("input[type='tel']").length; i++) {
            text_num += $("input[type='tel']").eq(i).val().length;
        }
        for (var i=0; i< $("input[type='checkbox']:checked").length; i++) {
            text_num += $("input[type='checkbox']:checked").eq(i).val().length;
        }
        for (var i=0; i< $("textarea").length; i++) {
            text_num += $("textarea").eq(i).val().length;
        }

        if (text_num > 0 ) {
            isChanged = true;
        } else {
            isChanged = false;
        }
    });

    $("[type=submit]").on("click",function() {
        isChanged = false;
    });

  }
};

$(function(){
new MULKAG.FUNCTION.STOPTRANSITION();
});