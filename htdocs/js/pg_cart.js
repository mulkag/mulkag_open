(function () {
	'use strict';

	var UACHECK = require('./modules/cm_uacheck');
	var NAVACCORDION = require('./modules/pc_navaccordion');
	var DOKIDOKI = require('./modules/pc_dokidoki');
	var SLIDEMENU = require('./modules/sp_slideMenu');
	var SELECTALLITEM = require('./modules/pc_selectallitem');
	var CHANGEITEMSCORE = require('./modules/pc_changeItemScore');
	var ADDCHECKED = require('./modules/pc_addChecked');
	var SHOWSUBPOSTAGE = require('./modules/pc_showsubpostage');
	var STOPTRANSITION = require('./modules/pc_stoptransition');
	var CHOISEBOX = require('./modules/pc_choisebox');
	var SMOOTHSCROLL = require('./modules/pc_smoothscroll');
	var AUTOKANAPLUG = require('./modules/plugin_autoKana');
	var AUTOPOSTPLUG = require('./modules/plugin_autoPost');
	var AUTOKANA = require('./modules/pc_autokana');
	var AUTOEMAIL = require('./modules/pc_autoemail');

})();
