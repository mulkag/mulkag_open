var gulp = require('gulp');
// var sass = require('gulp-sass');
var cssnext = require('gulp-cssnext');
var webpack = require('gulp-webpack');
var uglify = require("gulp-uglify");
var cssmin = require('gulp-cssmin');
var imagemin = require('gulp-imagemin');
var pngmin = require('imagemin-pngquant');
var svgmin = require('imagemin-svgo');
var jpgmin = require('imagemin-jpegtran');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
var jshint = require('gulp-jshint');
var csslint = require('gulp-csslint');

var htdocs = './htdocs/';
var build = './public/assets';
var paths = {
	'scss': './public/sass/',
	'css': './public/assets/css/'
}

// gulp.task('scss', function() {
// 	return gulp.src(paths.scss = '*.scss')
// 	  .pipe(sass())
// 	  .on('error', function(err) {
// 		  console.log(err.message);
// 	  })
// 	  .pipe(cssnext())
// 	  .pipe(gulp.dest(paths.css))
// });


gulp.task('default', [
  'min',
  'css_top',
  'css_sitemap',
	'css_search',
  'css_detail',
  'css_sct',
  'css_rule',
  'css_regist',
  'css_privacy',
  'css_news',
  'css_member',
  'css_login',
  'css_guide',
  'css_contact',
	'css_cart',
  'css_cart_thanks',
  'css_collection',
	'css_brand',
  'css_brand_detail',
  'css_favorite',
	'css_error',
  'css_about'
]);

gulp.task('min', function() {

  // gulp.src(build + '/img/**/*.+(jpg|jpeg|png|gif|svg)')
  //   .pipe(imagemin({
  //       optimizationLevel: 7,
  //       progressive: true,
  //       use: [
  //       pngmin({
  //           quality: '65-80', speed: 1
  //       }),
  //       svgmin({
  //           plugins: [{
  //               removeDoctype: false
  //           }, {
  //               removeComments: false
  //           }, {
  //               cleanupNumericValues: {
  //                   floatPrecision: 2
  //               }
  //           }, {
  //               convertColors: {
  //                   names2hex: false,
  //                   rgb2hex: false
  //               }
  //           }]
  //       }),
  //       jpgmin()]
  //   }))
  //   .pipe(gulp.dest(build + '/img/'));

    gulp.src(build + '/css/pc/*.min.css')
        .pipe(cssmin())
        .pipe(gulp.dest(build + '/css/'));

    gulp.src(htdocs + '/js/*.js')
        .pipe(webpack({
            entry: {
                top: htdocs + '/js/pg_top.js',
                search: htdocs + '/js/pg_search.js',
                search_detail: htdocs + '/js/pg_search_detail.js',
                sitemap: htdocs + '/js/pg_common.js',
                sct: htdocs + '/js/pg_common.js',
                rule: htdocs + '/js/pg_common.js',
                regist_input: htdocs + '/js/pg_regist_input.js',
                regist_confirm: htdocs + '/js/pg_common.js',
                regist_thanks: htdocs + '/js/pg_common.js',
                privacy: htdocs + '/js/pg_common.js',
                news: htdocs + '/js/pg_common.js',
                member: htdocs + '/js/pg_common.js',
                member_info: htdocs + '/js/pg_member_info.js',
                login: htdocs + '/js/pg_common.js',
                guide: htdocs + '/js/pg_guide.js',
                contact: htdocs + '/js/pg_contact.js',
                collection: htdocs + '/js/pg_common.js',
                cart: htdocs + '/js/pg_cart.js',
                cart_sub: htdocs + '/js/pg_common.js',
                about: htdocs + '/js/pg_about.js',
                favorite: htdocs + '/js/pg_common.js',
								brand: htdocs + '/js/pg_brand.js',
								brand_detail: htdocs + '/js/pg_brand_detail.js',
                error: htdocs + '/js/pg_error.js',
                prerelease: htdocs + '/js/pg_prerelease.js'
            },
            output: {
                filename: 'pc_[name].js'
            }
        }))
        .pipe(uglify({
          options: {
            mangle: false,
            compress: false
            }
          }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(build + '/js/'));

});

gulp.task("css_top", function () {
  return gulp.src([
      htdocs + '/css/common/reset.css',
      htdocs + '/css/common/common.css',
      htdocs + '/css/pg_top.css'
      ])
      .pipe(concat("pc_top.min.css"))
      .pipe(gulp.dest(build + '/css/'));
});

gulp.task("css_sitemap", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_sitemap.css'
        ])
      .pipe(concat("pc_sitemap.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_search", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_search.css'
        ])
      .pipe(concat("pc_search.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_detail", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_search_detail.css'
        ])
      .pipe(concat("pc_search_detail.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_sct", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_sct.css'
        ])
      .pipe(concat("pc_sct.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_collection", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_collection.css'
        ])
      .pipe(concat("pc_sct.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_rule", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_rule.css'
        ])
      .pipe(concat("pc_rule.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_regist", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_regist.css'
        ])
      .pipe(concat("pc_regist.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_privacy", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_privacy.css'
        ])
      .pipe(concat("pc_privacy.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_news", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_news.css'
        ])
      .pipe(concat("pc_news.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_member", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_member.css'
        ])
      .pipe(concat("pc_member.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_login", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_login.css'
        ])
      .pipe(concat("pc_login.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_guide", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_guide.css'
        ])
      .pipe(concat("pc_guide.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_contact", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_contact.css'
        ])
      .pipe(concat("pc_contact.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_collection", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_collection.css'
        ])
      .pipe(concat("pc_collection.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_cart", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_cart.css'
        ])
      .pipe(concat("pc_cart.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_cart_thanks", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_cart_thanks.css'
        ])
      .pipe(concat("pc_cart_thanks.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_brand", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_brand.css'
        ])
      .pipe(concat("pc_brand.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_brand_detail", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_brand_detail.css'
        ])
      .pipe(concat("pc_brand_detail.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_error", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_error.css'
        ])
      .pipe(concat("pc_error.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_favorite", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_favorite.css'
        ])
      .pipe(concat("pc_favorite.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});
gulp.task("css_about", function () {

  return gulp.src([
        htdocs + '/css/common/reset.css',
        htdocs + '/css/common/common.css',
        htdocs + '/css/pg_about.css'
        ])
      .pipe(concat("pc_about.min.css"))
      .pipe(gulp.dest(build + '/css/'));

});

gulp.task('lint', function() {
    gulp.src('./htdocs/css/pc/*.css')
        .pipe(csslint())
        .pipe(csslint.reporter());
    return gulp.src('./htdocs/js/modules/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});
